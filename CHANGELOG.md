# Change Log:

# 3.5.0 10-Jan-2025
 - switch to qt 6.8

# 3.4.0 21-May-2024
 - switch to dtk-widgets 3.4.0
 
# 3.3.0 11-Apr-2024
 - plot2D: add method to set selected fields
 - fix: do not try to create plot if we have not enough data

# 3.2.2 25-Mar-2024
 - plot2D: avoid calling too many times sortItems when adding a lot of fieldY

# 3.2.1 06-Dev-2023
 - use field name as axis label for X Axis in 2DPlots

# 3.2.0 27-Nov-2023
 - switch to dtk-widgets 3.2.0
 - remove c++20 warnings
 
# 3.1.0 09-Nov-2023
 - switch to c++20
 - update cmakelists
 - fix default plugin path with conda

# 3.0.0 27-Sep-2023
 - switch to Qt6

# 2.30.0 23-Mai-2022
 - switch to c++17
 - add accessor to plot2D menu and don't add it automatically to the menubar

# 2.29.0 15-Sep-2021
 - fix memory leak in decorator
 - add setInitialDate and setTimeStep methods in dtkVisualizationWidgetsVideoControls

# 2.28.0 30-Mar-2021
 - add setBackground method in canvas API 
 - save seed type and show status for stream tracer 
 - bugfix: when re-enabling scalar bar, set it to On if needed
 
# 2.27.0 18-Mar-2021
 - switch to VTK 9

# 2.26.1 1-Feb-2021
 - use dtk-widgets >= 2.25.0

# 2.26.0 21-Jan-2021
 - remove ambiguous signature of addFieldY method

# 2.25.0 04-Dec-2020
 - add setTitle in dtkVisualizationCanvas API
 
# 2.24.0 31-Jul-2020
 - fix: use previous setting of each slice when show is called
 - vector glyph auto scale can be turned off
 - expose random sampling for vector glyph decorator

# 2.23.1 21-Jul-2020
 - fix struct initialization for visual studio
# 2.23.0 21-Jul-2020
 - plot2d: add source color control; add remove source
 - fix: don't add scalar bar several times
 - raise maximum scale factor from 1000 to 100000

# 2.22.2 7-May-2020
 - makes reset virtual in canvas and implement it in plot2D
# 2.22.1 6-May-2020
 - use current theme to set background brush of plot2D
# 2.22.0 28-Apr-2020
 - decoratorClipper can hide all decorator of a collection
# 2.21.0 08-Apr-2020
 - new GUI behavior for DecoratorWithClut (auto/manual mode)
 - enhance scalar bar positioning (addScalarBar takes ScalarBarWidget)
 - check data in clipper decorator

# 2.20.2 18-Feb-2020
 - support for reload different data in DecoratorVertexEdge

# 2.20.1 13-Feb-2020
 - add reset and automatic button for scalar bar

# 2.20.0 13-Feb-2020
 - added support for vtkImageData in DecoratorClipper
 - bugfix in DecoratorClipper
 - fix crash in decorator clipper when loading incompatible data
 - add DecoratorClipper : for clipping out vtkPolyData
 - add DecoratorVertexEdge : renders spheres and tubes to show vertex and edges

# 2.19.0 04-Feb-2020
 - raise maximum values for spin boxes
 - optionally do not include class name in scalar bar title

# 2.18.2 28-Jan-2020
 - Fix set visibility for Streamlines decorator when source sphere widget doesn't have an interactor

# 2.18.1 28-Jan-2020
 - Fix scalar bar position

# 2.18.0 16-Jan-2020
 - update decorator API: add setInputConnection/outputPort

# 2.17.1 15-Jan-2020
 - Make veriants allow for different vtk types within a hierarchy of types.

# 2.17.0 14-Jan-2020
 - use QVTKOpenGLNativeWidget (requires VTK 8.2.0 and up)
 - fix display of menubar on macos in dtkVisualizationViewer
 - stream tracer widget refactoring
 - major update to decorators:
  - fix draw/touch usage
  - uniform coloring is available for all vector decorators
  - management of color map can be enhanced especially to include opacity
  - dtkVisualizationDecoratorScalarIsolines -> dtkVisualizationDecoratorIsocontours
  - dtkVisualizationDecoratorScalarScalarColormap -> dtkVisualizationDecoratorSurfaceColor
 - add cmake option to build with VTK git (```DTKVISUALIZATION_USE_UPCOMING_VTK```)

# 2.16.1 20-Dec-2019
 - use vtkGlyph3DMapper in glyph decorators

# 2.16.0 20-Dec-2019
 - add vector stream line decorator
 - add vector glyph decorator (wip)
 - fix clear of plot2D

# 2.15.1 05-Dec-2019
 - bugfix plot2d dont clear old_fields on field_x->clear
 - fix axes touch method
 - bugfix plot2d block signals on field_y before clearing it

# 2.15.0 05-Dec-2019
 - add setTransform in decorator API
 - rewrite 2d plot class
 - fix Slice decorator
 - add clut from sdm topography

# 2.14.3 29-Nov-2019
 - fix restore when using slice decorator fixed range

# 2.14.2 28-Nov-2019
 - add field method in dtkVisualizationDecoratorWithClut
 - fix restoring settings of decorators
 - fix Slice decorator
 - display frame info in dtkVisualizationWidgetsVideoControls;

# 2.14.1 17-Oct-2019
 - Non lazy instanciation scheme for specific situations in ScalarGlyph decorator

# 2.14.0 15-Oct-2019
 - really fix viewVideoRaster widgets
 - must increase minor number because of API change in 2.13.2

# 2.13.2 10-Oct-2019
 - fix viewVideoRaster widgets
 - give access to Glyph Decorator internals
 - glyph generate points ids

# 2.13.1 01-Oct-2019
 - use vtk components in cmake
 - fix default install prefix and libdir when running in conda env

# 2.13.0 09-Sep-2019
 - add slice, axes and volume decorators

# 2.12.0 23-Aug-2019
 - add decorators persistence

# 2.11.0 27-May-2019
 - glyph size ok for big domains
 - refactoring of decorator methods name (touch, draw, render)
 - fix decoratorIsolines
 - videoControls has 2 bars
 - add decorator snapshot
 - add decoratorCollection

# 2.10.0 21-May-2019
 - cmake refactoring
 - add geometry decorators (points and Delaunay 2D)
 - add streamline decorator
 - factorize scalar bar in dtkVisualizationDecoratorWithClut

# 2.9.2 03-Apr-2019
 - Fix scalar bar size, position and default show/hide

# 2.9.1 30-Apr-2019
 - Fix update for glyph and color map.

# 2.9.0 29-Apr-2019
 - add dtk-themes

# 2.8.3 02-Apr-2019
 - fix clut editor
 - fix resize behaviour and initial background of VideoRaster class
 - do not reset camera in render (range settings)

# 2.8.2 29-Mar-2019
 - add hudItem to toggle overlay in Canvas
 - add checkbox for scalar bars
 -   can set ranges on clutEditor

# 2.8.1 25-Mar-2019
 - fix build on windows
 - don't use vtk 8.2.0 for now

# 2.8.0 25-Mar-2019
 - add dtkVisualizationViewVideoPlayer based on QtMultimedia
 - major rearchitectore of VTK based classes:
    - add dtkVisualizationCanvas
    - add dtkVisualizationDecorator and child classes

# 2.7.2 20-Feb-2019
 - move toggle button of 2D plots to left using new overlaypane
 - add tool tip to toggle button

# 2.7.1 13-Feb-2019
 - add method to set range in plot2d

# 2.7.0 12-Feb-2019
 - add method enable/disable grid in plot2d
 - remove deprecated method addFIeld

# 2.6.2 09-Feb-2019
 - better internal handling of ViewHybrid
 - provides API to hide 2D and 3D buttons

# 2.6.1 07-Feb-2019
 - fix bottom left and right widget insertion in ViewHybrid

# 2.6.0 07-Feb-2019
 - add bottomLeftWidget and bottomRightWidget + protected acces to some widgets in ViewHybrid
 - add setVisible in dtkVisualizationViewOverlay
 - immediate rendering on update in Plot2D

# 2.5.0 03-Feb-2019
 - add dtkVisualizationViewOverlay class
 - add dtkVisualizationViewHybrid class (WIP)

# 2.4.1 10-Dec-2018
 - Fix color space when drawing VDO shaders
 - Add dependency on dtkLog because of dtkWidgets

## 2.4.0 6-Dec-2018
 - cmake fix for windows (link to opengl)
 - add ratioBuffer and ratioWidget in ViewVideoGL API

## 2.3.5 18-Oct-2018
 - cmake fix for windows

## 2.3.0 17-Oct-2018
 - Add a GL video view (GLSL 1.2)
 - Add a Raster video view
 - Implement corresponding examples

## 2.2.2 9-Oct-2018
 - Specify the vtk opengl format directly in the objects

## 2.2.1 20-Sep-2018
 - addField is now deprecated, use addFieldX and addFieldY instead
 - more accessors are available for overload

## 2.2.0 19-Sep-2018
 - add methods for Plot 2D to add/remove fields

## 2.1.1 14-Sep-2018
- fix install rules (missing layer wide header)

## 2.1.0 12-Sep-2018
- fix install on windows
- add actor base concept

## 2.0.0 02-Jul-2018
- sdm-modeler release
- add dtkVisualizationWidgets
- add new VTK stubs for View 2D/3D and Plot 2D/3D

## 0.1.0 14-May-2018
- Initial revision

# dtk-visualization

[![Anaconda-Server Badge](https://anaconda.org/dtk-forge/dtk-visualization/badges/version.svg)](https://anaconda.org/dtk-forge/dtk-visualization)
[![Anaconda-Server Badge](https://anaconda.org/dtk-forge/dtk-visualization/badges/latest_release_date.svg)](https://anaconda.org/dtk-forge/dtk-visualization)
[![Anaconda-Server Badge](https://anaconda.org/dtk-forge/dtk-visualization/badges/platforms.svg)](https://anaconda.org/dtk-forge/dtk-visualization)

`dtk-visualization` is a bridge between Qt and VTK and requires no additional dependency. Assuming VTK datasets as an input it provides visualization option in a most intuitive way as possible. Therefore, few filters are used.

Its purpose is double:

- As a standalone viewer through its `dtkVisualizationViewer` application, it is a quick and lightweight alternative to ParaView
- As a set of layer, it is intended to be injected in many application to have a maintained, efficient and robust visualization system


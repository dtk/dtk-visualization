// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationMetaTypeTest.h"

#include <dtkVisualizationTest>

#include <dtkVisualization/dtkVisualizationTypeTraits>
#include <dtkVisualization/dtkVisualizationMetaType>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationMetaTypeTestCasePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationMetaTypeTestCasePrivate
{
public:
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVisualizationMetaTypeTestCase::dtkVisualizationMetaTypeTestCase(void) : d(new dtkVisualizationMetaTypeTestCasePrivate)
{
}

dtkVisualizationMetaTypeTestCase::~dtkVisualizationMetaTypeTestCase(void)
{
    delete d;
}

void dtkVisualizationMetaTypeTestCase::initTestCase(void)
{

}

void dtkVisualizationMetaTypeTestCase::init(void)
{
}

void dtkVisualizationMetaTypeTestCase::testIsVtkDataSet(void)
{
    QVERIFY(dtk::is_vtkdataset<vtkDataSet>::value);
    QVERIFY(dtk::is_vtkdataset<vtkDataSet&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkDataSet&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkDataSet&&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkDataSet *>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkDataSet *>::value);
    QVERIFY(dtk::is_vtkdataset<vtkDataSet *&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkDataSet *&>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkDataSet>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkDataSet>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkDataSet *>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkDataSet *>::value);

    QVERIFY(dtk::is_vtkdataset<vtkImageData>::value);
    QVERIFY(dtk::is_vtkdataset<vtkImageData&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkImageData&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkImageData&&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkImageData *>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkImageData *>::value);
    QVERIFY(dtk::is_vtkdataset<vtkImageData *&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkImageData *&>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkImageData>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkImageData>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkImageData *>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkImageData *>::value);

    QVERIFY(dtk::is_vtkdataset<vtkPolyData>::value);
    QVERIFY(dtk::is_vtkdataset<vtkPolyData&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkPolyData&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkPolyData&&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkPolyData *>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkPolyData *>::value);
    QVERIFY(dtk::is_vtkdataset<vtkPolyData *&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkPolyData *&>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkPolyData>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkPolyData>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkPolyData *>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkPolyData *>::value);

    QVERIFY(dtk::is_vtkdataset<vtkRectilinearGrid>::value);
    QVERIFY(dtk::is_vtkdataset<vtkRectilinearGrid&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkRectilinearGrid&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkRectilinearGrid&&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkRectilinearGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkRectilinearGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<vtkRectilinearGrid *&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkRectilinearGrid *&>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkRectilinearGrid>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkRectilinearGrid>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkRectilinearGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkRectilinearGrid *>::value);

    QVERIFY(dtk::is_vtkdataset<vtkStructuredGrid>::value);
    QVERIFY(dtk::is_vtkdataset<vtkStructuredGrid&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkStructuredGrid&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkStructuredGrid&&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkStructuredGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkStructuredGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<vtkStructuredGrid *&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkStructuredGrid *&>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkStructuredGrid>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkStructuredGrid>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkStructuredGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkStructuredGrid *>::value);

    QVERIFY(dtk::is_vtkdataset<vtkUnstructuredGrid>::value);
    QVERIFY(dtk::is_vtkdataset<vtkUnstructuredGrid&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkUnstructuredGrid&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkUnstructuredGrid&&>::value);
    QVERIFY(dtk::is_vtkdataset<vtkUnstructuredGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkUnstructuredGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<vtkUnstructuredGrid *&>::value);
    QVERIFY(dtk::is_vtkdataset<const vtkUnstructuredGrid *&>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkUnstructuredGrid>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkUnstructuredGrid>::value);
    QVERIFY(dtk::is_vtkdataset<volatile vtkUnstructuredGrid *>::value);
    QVERIFY(dtk::is_vtkdataset<const volatile vtkUnstructuredGrid *>::value);
}

void dtkVisualizationMetaTypeTestCase::testCanConvert(void)
{
    // First one needs to register vtk types
    dtk::visualization::registerVTKToMetaType();

    QList<int> list_of_types_to_convert = {
        QMetaType::fromName("vtkDataSet*").id(),
        QMetaType::fromName("vtkImageData*").id(),
        QMetaType::fromName("vtkPolyData*").id(),
        QMetaType::fromName("vtkRectilinearGrid*").id(),
        QMetaType::fromName("vtkStructuredGrid*").id(),
        QMetaType::fromName("vtkUnstructuredGrid*").id()
    };
    QVERIFY(!dtk::canConvert<vtkDataSet *>(list_of_types_to_convert));
}

void dtkVisualizationMetaTypeTestCase::testVariantFromValue(void)
{
    QVariant v;
    {
        vtkDataSet *data = vtkImageData::New();
        v = dtk::variantFromValue(data);
        QVERIFY(v.userType() == QMetaType::fromName("vtkImageData*").id());
        auto dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        auto ee = v.value<vtkImageData *>();
        QVERIFY(ee);
        v = dtk::variantFromValue(ee);
        dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        data->Delete();
    }
    {
        vtkDataSet *data = vtkPolyData::New();
        v = dtk::variantFromValue(data);
        QVERIFY(v.userType() == QMetaType::fromName("vtkPolyData*").id());
        auto dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        auto ee = v.value<vtkPolyData *>();
        QVERIFY(ee);
        v = dtk::variantFromValue(ee);
        dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        data->Delete();
    }
    {
        vtkDataSet *data = vtkRectilinearGrid::New();
        v = dtk::variantFromValue(data);
        QVERIFY(v.userType() == QMetaType::fromName("vtkRectilinearGrid*").id());
        auto dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        auto ee = v.value<vtkRectilinearGrid *>();
        QVERIFY(ee);
        v = dtk::variantFromValue(ee);
        dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        data->Delete();
    }
    {
        vtkDataSet *data = vtkStructuredGrid::New();
        v = dtk::variantFromValue(data);
        QVERIFY(v.userType() == QMetaType::fromName("vtkStructuredGrid*").id());
        auto dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        auto ee = v.value<vtkStructuredGrid *>();
        QVERIFY(ee);
        v = dtk::variantFromValue(ee);
        dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        data->Delete();
    }
    {
        vtkDataSet *data = vtkUnstructuredGrid::New();
        v = dtk::variantFromValue(data);
        QVERIFY(v.userType() == QMetaType::fromName("vtkUnstructuredGrid*").id());
        auto dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        auto ee = v.value<vtkUnstructuredGrid *>();
        QVERIFY(ee);
        v = dtk::variantFromValue(ee);
        dd = v.value<vtkDataSet *>();
        QVERIFY(dd);
        data->Delete();
    }
}

void dtkVisualizationMetaTypeTestCase::cleanupTestCase(void)
{

}

void dtkVisualizationMetaTypeTestCase::cleanup(void)
{
}

DTKVISUALIZATIONTEST_MAIN_NOGUI(dtkVisualizationMetaTypeTest, dtkVisualizationMetaTypeTestCase);

//
// dtkVisualizationMetaTypeTest.cpp ends here

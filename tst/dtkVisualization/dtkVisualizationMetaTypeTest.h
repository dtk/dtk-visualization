// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkVisualizationMetaTypeTestCase : public QObject
{
    Q_OBJECT

public:
     dtkVisualizationMetaTypeTestCase(void);
    ~dtkVisualizationMetaTypeTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testIsVtkDataSet(void);
    void testCanConvert(void);
    void testVariantFromValue(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkVisualizationMetaTypeTestCasePrivate *d;
};

//
// dtkVisualizationMetaTypeTest.h ends here

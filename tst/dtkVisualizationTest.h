// ///////////////////////////////////////////////////////////////////

#pragma once

#include <QtTest>

#define DTKVISUALIZATIONTEST_MAIN_NOGUI(TestMain, TestObject) \
    int TestMain(int argc, char *argv[]) {                        \
        QCoreApplication app(argc, argv);                     \
        TestObject tc;                                        \
        return QTest::qExec(&tc, argc, argv);                 \
    }

#define DTKVISUALIZATIONTEST_MAIN_GUI(TestMain, TestObject)   \
    int TestMain(int argc, char *argv[]) {                        \
        QApplication app(argc, argv);                         \
        TestObject tc;                                        \
        return QTest::qExec(&tc, argc, argv);                 \
    }

//
// dtkVisualizationTest.h ends here

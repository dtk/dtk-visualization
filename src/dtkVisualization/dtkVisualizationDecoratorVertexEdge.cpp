#include "dtkVisualizationDecoratorVertexEdge.h"
#include "dtkVisualizationWidgetsVertexEdgesControls.h"

#include <dtkLog>
#include <dtkVisualization>
#include <dtkThemesEngine>

#include <QtGui>
#include <QtWidgets>

#include <vtkActor.h>
#include <vtkLookupTable.h>
#include <vtkCellData.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkRendererCollection.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkExtractEdges.h>
#include <vtkTubeFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkGlyph3D.h>
#include <vtkSphereSource.h>
#include <vtkLight.h>
#include <vtkLightActor.h>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVertexEdge declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorVertexEdgePrivate
{
public:
    dtkVisualizationView3D *view{nullptr};

public:
    QList<QWidget *> inspectors;

public:
    vtkSmartPointer<vtkDataSet> data;

public:
    vtkSmartPointer<vtkExtractEdges> edges_extractor;
    vtkSmartPointer<vtkTubeFilter> edges_filter;
    vtkSmartPointer<vtkPolyDataMapper> edges_mapper;
    vtkSmartPointer<vtkActor> edges_actor;
    bool is_edges_initialized{false};

public:
    vtkSmartPointer<vtkSphereSource> vertex_glyph;
    vtkSmartPointer<vtkGlyph3D> vertex_glyph3d;
    vtkSmartPointer<vtkPolyDataMapper> vertex_mapper;
    vtkSmartPointer<vtkActor> vertex_actor;
    bool is_vertex_initialized{false};

public:
    dtkVisualizationWidgetsVertexEdgesControls *edge_indicator{nullptr};
    dtkVisualizationWidgetsVertexEdgesControls *vertex_indicator{nullptr};
    vtkSmartPointer<vtkLightActor> lightActor;

public:
    void initializeVertex(void);

    void initializeEdges(void);
};

void dtkVisualizationDecoratorVertexEdgePrivate::initializeVertex(void)
{
    vertex_glyph3d->SetInputData(data);
    vertex_glyph3d->Update();
    vertex_mapper->Modified();
    is_vertex_initialized = true;
}

void dtkVisualizationDecoratorVertexEdgePrivate::initializeEdges(void)
{
    edges_extractor->SetInputData(data);
    edges_extractor->Update();
    edges_filter->SetInputData(edges_extractor->GetOutput());
    edges_filter->Update();
    is_edges_initialized = true;
}

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorPoints implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorVertexEdge::dtkVisualizationDecoratorVertexEdge(void) : d(
        new dtkVisualizationDecoratorVertexEdgePrivate())
{
    d->edges_extractor = vtkSmartPointer<vtkExtractEdges>::New();

    d->edges_filter = vtkSmartPointer<vtkTubeFilter>::New();

    d->edges_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    d->edges_mapper->SetInputData(d->edges_filter->GetOutput());

    d->edges_actor = vtkSmartPointer<vtkActor>::New();
    d->edges_actor->SetMapper(d->edges_mapper);
    d->edges_actor->SetVisibility(false);

    d->vertex_glyph = vtkSmartPointer<vtkSphereSource>::New();
    d->vertex_glyph->SetPhiResolution(8);
    d->vertex_glyph->SetThetaResolution(8);
    d->vertex_glyph->Update();

    d->vertex_glyph3d = vtkSmartPointer<vtkGlyph3D>::New();
    d->vertex_glyph3d->SetSourceData(d->vertex_glyph->GetOutput());

    d->vertex_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    d->vertex_mapper->SetInputData(d->vertex_glyph3d->GetOutput());

    d->vertex_actor = vtkSmartPointer<vtkActor>::New();
    d->vertex_actor->SetMapper(d->vertex_mapper);
    d->vertex_actor->SetVisibility(false);

    this->setObjectName("Vertex & Edges");

    d->edge_indicator = new dtkVisualizationWidgetsVertexEdgesControls;
    d->edge_indicator->setObjectName("Edges");


    d->vertex_indicator = new dtkVisualizationWidgetsVertexEdgesControls;
    d->vertex_indicator->setObjectName("Vertex");

    d->inspectors
            << d->edge_indicator
            << d->vertex_indicator;

    connect(d->edge_indicator, &dtkVisualizationWidgetsVertexEdgesControls::toggled,
            [=,this](bool checked)
            {
                if (checked && !d->is_edges_initialized) {
                    d->initializeEdges();
                }
                d->edges_actor->SetVisibility(checked);
                this->saveSettings("edges_visibility", checked);
                this->draw();
            });

    connect(d->edge_indicator, &dtkVisualizationWidgetsVertexEdgesControls::color_changed,
            [=,this](const QColor& color) {
                d->edges_actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
                this->saveSettings("edges_color", color);
                this->draw();
            });

    connect(d->edge_indicator, &dtkVisualizationWidgetsVertexEdgesControls::opacity_changed,
            [=,this](double opacity) {
                d->edges_actor->GetProperty()->SetOpacity(opacity);
                this->saveSettings("edges_opacity", opacity);
                this->draw();
            });

    connect(d->edge_indicator, &dtkVisualizationWidgetsVertexEdgesControls::radius_changed,
            [=,this](double radius) {
                d->edges_filter->SetRadius(radius);
                if (d->is_edges_initialized) {
                    d->edges_filter->Update();
                }
                this->saveSettings("edges_radius", radius);
                this->draw();
            });

    connect(d->vertex_indicator, &dtkVisualizationWidgetsVertexEdgesControls::toggled,
            [=,this](bool checked) {
                if (checked && !d->is_vertex_initialized) {
                    d->initializeVertex();
                }
                d->vertex_actor->SetVisibility(checked);
                this->saveSettings("vertex_visibility", checked);
                this->draw();
            });

    connect(d->vertex_indicator, &dtkVisualizationWidgetsVertexEdgesControls::color_changed,
            [=,this](const QColor& color) {
                d->vertex_actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
                this->saveSettings("vertex_color", color);
                this->draw();
            });

    connect(d->vertex_indicator, &dtkVisualizationWidgetsVertexEdgesControls::opacity_changed,
            [=,this](double value) {
                d->vertex_actor->GetProperty()->SetOpacity(value);
                this->saveSettings("vertex_opacity", value);
                this->draw();
            });

    connect(d->vertex_indicator, &dtkVisualizationWidgetsVertexEdgesControls::radius_changed,
            [=,this](double value) {
                d->vertex_glyph3d->SetScaleFactor(value);
                d->vertex_glyph3d->Update();
                this->saveSettings("vertex_radius", value);
                this->draw();
            });


    vtkSmartPointer<vtkLight> light = vtkSmartPointer<vtkLight>::New();
    light->SetPositional(true);

    d->lightActor = vtkSmartPointer<vtkLightActor>::New();
    d->lightActor->SetLight(light);
}

dtkVisualizationDecoratorVertexEdge::~dtkVisualizationDecoratorVertexEdge(void)
{
    d->view = nullptr;
    delete d;
    d = nullptr;
}

void dtkVisualizationDecoratorVertexEdge::restoreSettings(void)
{
    QString name = this->objectName();
    if (name.isEmpty()) {
        return;
    }

    QSettings settings;

    settings.beginGroup("canvas");

    bool edges_visibility = settings.value(name + "_edges_visibility", false).toBool();
    const QString& edges_color_string = settings.value(name + "_edges_color", "#659e51").toString();
    const QColor& edges_color = QColor(edges_color_string);
    double edges_opacity = settings.value(name + "_edges_opacity", 1.0).toDouble();
    double edges_radius = settings.value(name + "_edges_radius", 0.0).toDouble();

    bool vertex_visibility = settings.value(name + "_vertex_visibility", false).toBool();
    const QString& vertex_color_string = settings.value(name + "_vertex_color", "#a35468").toString();
    const QColor& vertex_color = QColor(vertex_color_string);
    double vertex_opacity = settings.value(name + "_vertex_opacity", 1.0).toDouble();
    double vertex_radius = settings.value(name + "_vertex_radius", 0.0).toDouble();

    settings.endGroup();

    d->edge_indicator->setActive(edges_visibility);
    d->edge_indicator->setColor(edges_color);
    d->edge_indicator->setOpacity(edges_opacity);
    if (edges_radius > 0) {
        d->edge_indicator->setRadius(edges_radius);
    }

    d->vertex_indicator->setActive(vertex_visibility);
    d->vertex_indicator->setColor(vertex_color);
    d->vertex_indicator->setOpacity(vertex_opacity);
    if (vertex_radius > 0) {
        d->vertex_indicator->setRadius(vertex_radius);
    }
}


void dtkVisualizationDecoratorVertexEdge::touch(void)
{
    if (!this->canvas()) {
        dtkWarn() << Q_FUNC_INFO << "No canvas was set, call setCanvas to call draw on a canvas.";
        return;
    }

    Q_ASSERT(this->canvas()->renderer());

    if (d->is_vertex_initialized) {
        d->vertex_glyph3d->Update();
        d->vertex_mapper->Modified();
    }

    if (d->is_edges_initialized) {
        d->edges_extractor->Update();
        d->edges_filter->Update();
        d->edges_mapper->Modified();
    }

    if (this->canvas()->interactor()) {
        this->canvas()->interactor()->Render();
    }
}

bool dtkVisualizationDecoratorVertexEdge::isDecorating(void)
{
    return d->data;
}

void dtkVisualizationDecoratorVertexEdge::setData(const QVariant& data)
{
    if (data.value<vtkStructuredGrid *>() || (data.value<vtkUnstructuredGrid *>() || data.value<vtkPolyData *>())) {
        d->data = data.value<vtkDataSet *>();
    }

    if (!d->data) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet is expected. Input data is not stored.";
        return;
    }

    d->edges_filter->SetInputData(d->data);
    d->vertex_glyph3d->SetInputData(d->data);

    this->restoreSettings();

    const double *bounds = d->data->GetBounds();

    const double max_dimension = std::max(bounds[1] - bounds[0],
                                          std::max(bounds[3] - bounds[2], bounds[5] - bounds[4]));

    if (d->edge_indicator->radius() <= 0) {
        d->edge_indicator->setRadius(0.002 * max_dimension);
    }
    if (d->vertex_indicator->radius() <= 0) {
        d->vertex_indicator->setRadius(0.03 * max_dimension);
    }

    this->draw();
}

void dtkVisualizationDecoratorVertexEdge::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d->view = dynamic_cast<dtkVisualizationView3D *>(canvas);

    if (!d->view) {
        qWarning() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    d->view->renderer()->AddActor(d->edges_actor);
    d->view->renderer()->AddActor(d->vertex_actor);

    if (d->view->interactor()) {
        d->view->interactor()->Render();
    }
}

void dtkVisualizationDecoratorVertexEdge::unsetCanvas(void)
{
    if (d->view) {
        d->view->renderer()->RemoveActor(d->edges_actor);
        d->view->renderer()->RemoveActor(d->vertex_actor);
    }

    d->view = nullptr;
}

void dtkVisualizationDecoratorVertexEdge::setVisibility(bool visible)
{
    dtkVisualizationDecorator::setVisibility(visible);
    if (visible && !d->is_edges_initialized) {
        d->initializeEdges();
    }
    d->edges_actor->SetVisibility(visible && d->edge_indicator->isActive());

    if (visible && !d->is_vertex_initialized) {
        d->initializeVertex();
    }
    d->vertex_actor->SetVisibility(visible && d->vertex_indicator->isActive());
}

QVariant dtkVisualizationDecoratorVertexEdge::data(void) const
{
    return d->data ? dtk::variantFromValue(d->data.Get()) : QVariant();
}

dtkVisualizationCanvas *dtkVisualizationDecoratorVertexEdge::canvas(void) const
{
    return d->view;
}

QList<QWidget *> dtkVisualizationDecoratorVertexEdge::inspectors(void) const
{
    return d->inspectors;
}

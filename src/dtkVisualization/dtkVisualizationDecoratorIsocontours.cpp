// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationDecoratorIsocontours.h"

#include "dtkVisualizationDecoratorWithClut_p.h"
#include "dtkVisualizationCanvas.h"
#include "dtkVisualizationMetaType.h"
#include "dtkVisualizationView2D.h"

#include <dtkVisualizationWidgets/dtkVisualizationWidgetsCategory>

#include <dtkLog>

#include <QtGui>
#include <QtWidgets>

#include <vtkActor.h>
#include <vtkCellDataToPointData.h>
#include <vtkCellData.h>
#include <vtkColorTransferFunction.h>
#include <vtkContourFilter.h>
#include <vtkDataSet.h>
#include <vtkLookupTable.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorIsocontoursPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorIsocontoursPrivate
{
public:
    vtkSmartPointer<vtkActor> actor;
    vtkSmartPointer<vtkPolyDataMapper> mapper;

public:
    vtkSmartPointer<vtkContourFilter> isolines;
    vtkSmartPointer<vtkCellDataToPointData> c2p_filter;

public:
    int default_isolines_count = 10;

public:
    QSpinBox *isolines_counts_sb = nullptr;
    QDoubleSpinBox *isolines_width_sb = nullptr;

    QSlider *opacity_slider = nullptr;
    QDoubleSpinBox *opacity_val_sp = nullptr;

public:
    QHash<QString, std::size_t> isolines_counts;

public:
    bool dirty = true;
    bool dirty_range = true;
};

// ///////////////////////////////////////////////////////////////////

void dtkVisualizationDecoratorIsocontours::updateContours(void)
{
    auto field_name = d_func()->current_field_name;

    if (field_name.isEmpty() || !d_func()->dataset) {
        return;
    }

    auto&& isoline_range = d_func()->ranges[field_name];

    auto count = d->isolines_counts[field_name];

    d->isolines_counts_sb->blockSignals(true);
    d->isolines_counts_sb->setValue(count);
    d->isolines_counts_sb->blockSignals(false);

    d->isolines->GenerateValues(count, isoline_range[0], isoline_range[1]);

    d->isolines->Modified();
    d->mapper->Modified();
    d->actor->Modified();
}

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorIsocontours implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorIsocontours::dtkVisualizationDecoratorIsocontours(void): dtkVisualizationDecoratorWithClut(), d(new dtkVisualizationDecoratorIsocontoursPrivate())
{
    d->isolines = vtkSmartPointer<vtkContourFilter>::New();

    d->c2p_filter = vtkSmartPointer<vtkCellDataToPointData>::New();

    d->mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    d->mapper->SetInputConnection(d->isolines->GetOutputPort());
    d->mapper->SetColorModeToMapScalars();
    d->mapper->SetScalarVisibility(true);

    d->actor = vtkSmartPointer<vtkActor>::New();
    d->actor->SetMapper(d->mapper);

    //////////
    // Inspectors creation and setup

    d->isolines_counts_sb = new QSpinBox;
    d->isolines_counts_sb->setObjectName("Count");
    d->isolines_counts_sb->setValue(10);
    d->isolines_counts_sb->setMaximum(10000);
    d->isolines_counts_sb->setKeyboardTracking(false);

    d->isolines_width_sb = new QDoubleSpinBox;
    d->isolines_width_sb->setObjectName("Contour Width");
    d->isolines_width_sb->setValue(1);
    d->isolines_width_sb->setMinimum(0.1);
    d->isolines_width_sb->setMaximum(10);
    d->isolines_width_sb->setDecimals(1);
    d->isolines_width_sb->setSingleStep(0.1);
    d->isolines_width_sb->setKeyboardTracking(false);

    //////////
    // Inspectors connections

    connect(d_func()->show_actor_cb, &QCheckBox::stateChanged, [=,this] (int state)
    {
        this->saveSettings("visibility", state == Qt::Checked);
        this->setVisibility(state == Qt::Checked);
        this->draw();
    });

    connect(d->isolines_counts_sb, QOverload<int>::of(&QSpinBox::valueChanged), [=,this] (int value)
    {
        this->saveSettings("isolines_count", value);
        this->setCurrentIsolinesCount(value);
        this->touch();
        this->draw();
    });

    connect(d->isolines_width_sb, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this] (double value)
    {
        this->saveSettings("isolines_line_width", value);
        d->actor->GetProperty()->SetLineWidth(value);
        d->actor->Modified();
        this->draw();
    });

    this->setObjectName("Isocontours");

    QWidget *param_box = new dtkVisualizationWidgetsCategory("Isocontours", {
            d->isolines_counts_sb
        });

    QWidget *styling_box = new dtkVisualizationWidgetsCategory("Styling", {
            d->isolines_width_sb,
            d_func()->opacity_widget
        });

    d_func()->inspectors << param_box << styling_box;
}

dtkVisualizationDecoratorIsocontours::~dtkVisualizationDecoratorIsocontours(void)
{
    dtkVisualizationDecoratorIsocontours::unsetCanvas();

    delete d;
    d = nullptr;
}

void dtkVisualizationDecoratorIsocontours::restoreSettings(void)
{
    QString name = this->objectName();
    if (name.isEmpty())
        return;
    dtkVisualizationDecoratorWithClut::restoreSettings();

    QSettings settings;
    settings.beginGroup("canvas");
    d->default_isolines_count = settings.value(name+"_isolines_count", 10).toInt();
    settings.endGroup();

    this->setCurrentIsolinesCount(d->default_isolines_count);

    d->dirty_range = !d_func()->original_range;
}

void dtkVisualizationDecoratorIsocontours::touchRange(void)
{
    // We first need to update the range of the current field.
    dtkVisualizationDecoratorWithClut::touchRange();

    // Then we update the contours when necessary
    if (d->dirty  || (d->dirty_range != d_func()->original_range)) {
        this->updateContours();
        d->dirty_range = d_func()->original_range;
        d->dirty = false;
    }
}

void dtkVisualizationDecoratorIsocontours::setData(const QVariant& data)
{
    vtkDataSet *dataset = data.value<vtkDataSet *>();

    if (!dataset) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet is expected. Input data is not stored.";
        return;
    }

    d_func()->clear();
    d_func()->retrieveScalarPoints(dataset);
    d_func()->retrieveScalarCells(dataset);
    d_func()->retrieveVectorPoints(dataset);
    d_func()->retrieveVectorCells(dataset);

    if (!this->isDecorating()) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet has no field to decorate. Nothing is done.";
        d_func()->clear();
        return;
    }

    d_func()->dataset = dataset;

    this->restoreSettings();

    d->c2p_filter->SetInputData(dataset);

    d_func()->sortEligibleFields();
    d->isolines_counts.clear();
    for (auto field_name : d_func()->eligible_field_names) {
        d->isolines_counts[field_name] = d->default_isolines_count;
    }
    this->setCurrentFieldName(d_func()->current_field_name);

    d->isolines->Modified();
    d->mapper->Modified();

    if (this->canvas()) {
        this->canvas()->renderer()->AddActor(d->actor);
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorIsocontours::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d_func()->view = dynamic_cast<dtkVisualizationView2D *>(canvas);
    if (!d_func()->view) {
        qWarning() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    if (d->isolines->GetInput()) {
        d_func()->view->renderer()->AddActor(d->actor);
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorIsocontours::unsetCanvas(void)
{
    if (d_func()->view) {
        d_func()->view->renderer()->RemoveActor(d->actor);
    }

    d_func()->disableScalarBar();

    d_func()->view = nullptr;
}

void dtkVisualizationDecoratorIsocontours::setVisibility(bool b)
{
    d->isolines_counts_sb->setEnabled(b);
    dtkVisualizationDecoratorWithClut::setVisibility(b);
    d->actor->SetVisibility(b);
}

void dtkVisualizationDecoratorIsocontours::setOpacity(double opacity)
{
    d->actor->GetProperty()->SetOpacity(opacity);
    d->actor->Modified();
}

void dtkVisualizationDecoratorIsocontours::setCurrentRange(double min, double max)
{
    dtkVisualizationDecoratorWithClut::setCurrentRange(min, max);

    d->dirty = true;
}

void dtkVisualizationDecoratorIsocontours::setCurrentIsolinesCount(std::size_t count)
{
    QString& field_name = d_func()->current_field_name;
    if (field_name.isEmpty()) {
        if (d_func()->default_field_name.isEmpty())
            return;
        field_name = d_func()->default_field_name;
    }
    d->isolines_counts[field_name] = count;

    d->dirty = true;
}

bool dtkVisualizationDecoratorIsocontours::setCurrentFieldName(const QString& field_name)
{
    if (field_name.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "Field name is empty, nothing is done.";
        return false;
    }

    if (!d_func()->dataset) {
        dtkWarn() << Q_FUNC_INFO << "Before calling setCurrentFieldName, setDataSet must be called.";
        return false;
    }

    if(!d_func()->eligible_field_names.contains(field_name)) {
        dtkWarn() << Q_FUNC_INFO << "The field name :" << field_name << "that was specified doesn't match any of the eligible scalar field names";

        return false;
    }

    d->dirty = true;

    using Support = dtkVisualizationDecoratorWithClut::Support;
    int support = d_func()->supports[field_name];
    if(support == Support::Point) {
        d->isolines->SetInputData(d_func()->dataset);
        d->isolines->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, qPrintable(field_name));
    } else if(support == Support::Cell) {
        d->c2p_filter->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, qPrintable(field_name));
        d->isolines->SetInputConnection(d->c2p_filter->GetOutputPort());
        d->isolines->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, qPrintable(field_name));
        d->c2p_filter->Modified();
    }
    d->isolines->Modified();

    return dtkVisualizationDecoratorWithClut::setCurrentFieldName(field_name);
}

void dtkVisualizationDecoratorIsocontours::setColorMap(const QMap<double, QColor>& new_colormap)
{
    dtkVisualizationDecoratorWithClut::setColorMap(new_colormap);

    d->mapper->SetLookupTable(d_func()->color_function);
    d->mapper->SelectColorArray(qPrintable(d_func()->current_field_name));
    auto&& range = d_func()->ranges[d_func()->current_field_name];
    d->mapper->SetScalarRange(range[0], range[1]);
    d->mapper->Modified();
}

//
// dtkVisualizationDecoratorIsocontours.cpp ends here

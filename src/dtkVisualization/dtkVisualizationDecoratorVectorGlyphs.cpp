// dtkVisualizationDecoratorVectorGlyphs.cpp
//

#include "dtkVisualizationDecoratorVectorGlyphs.h"
#include "dtkVisualizationDecoratorWithClut_p.h"
#include "dtkVisualizationMetaType.h"
#include "dtkVisualizationView2D.h"

#include <dtkVisualizationWidgets/dtkVisualizationWidgetsCategory>

#include <dtkLog>

#include <QtGui>
#include <QtWidgets>

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkCellCenters.h>
#include <vtkColorTransferFunction.h>
#include <vtkDataArray.h>
#include <vtkGlyph3DMapper.h>
#include <vtkLookupTable.h>
#include <vtkMaskPoints.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorGlyphsPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorVectorGlyphsPrivate
{
public:
    bool adjustGlyphs = true;
    vtkSmartPointer<vtkActor> actor;

public:
    vtkSmartPointer<vtkArrowSource> source_arrow;
    vtkSmartPointer<vtkCellCenters> cell_centers;
    vtkSmartPointer<vtkMaskPoints> mask_points;
    vtkSmartPointer<vtkGlyph3DMapper> glyphs_mapper;

public:
    dtkVisualizationWidgetsCategory *styling_box = nullptr;
    QComboBox *glyphs_scale_mode_cb = nullptr;
    QComboBox *mask_mode_combo_box = nullptr;
    QSpinBox *glyphs_number_spin_box = nullptr;
    QSpinBox *glyphs_stride_sb = nullptr;
    QSpinBox *glyphs_offset_spin_box = nullptr;
    QDoubleSpinBox *glyphs_scale_factor_sb = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorGlyphs implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorVectorGlyphs::dtkVisualizationDecoratorVectorGlyphs(void): dtkVisualizationDecoratorWithClut(), d(new dtkVisualizationDecoratorVectorGlyphsPrivate())
{
    d->source_arrow = vtkSmartPointer<vtkArrowSource>::New();

    d->cell_centers = vtkSmartPointer<vtkCellCenters>::New();

    d->mask_points = vtkSmartPointer<vtkMaskPoints>::New();
    d->mask_points->RandomModeOff();
    d->mask_points->SetOnRatio(2); // 1 => all points are taken then no masking

    d->glyphs_mapper = vtkSmartPointer<vtkGlyph3DMapper>::New();
    d->glyphs_mapper->SetInputConnection(d->mask_points->GetOutputPort());
    d->glyphs_mapper->SetSourceConnection(d->source_arrow->GetOutputPort());
    d->glyphs_mapper->SetColorModeToMapScalars();
    d->glyphs_mapper->SetOrientationModeToDirection();
    d->glyphs_mapper->SetScalarModeToUsePointFieldData();
    d->glyphs_mapper->SetScaleMode(vtkGlyph3DMapper::ScaleModes::NO_DATA_SCALING);
    d->glyphs_mapper->OrientOn();
    d->glyphs_mapper->ScalingOn();
    d->glyphs_mapper->SetScaleFactor(0.1);

    d->actor = vtkSmartPointer<vtkActor>::New();
    d->actor->SetMapper(d->glyphs_mapper);

    //////////
    // Inspectors creation

    d->glyphs_scale_mode_cb = new QComboBox;
    d->mask_mode_combo_box = new QComboBox;
    d->glyphs_number_spin_box = new QSpinBox;
    d->glyphs_stride_sb = new QSpinBox;
    d->glyphs_offset_spin_box = new QSpinBox;
    d->glyphs_scale_factor_sb = new QDoubleSpinBox;

    d->glyphs_scale_mode_cb->setObjectName("Scaling Mode");
    d->mask_mode_combo_box->setObjectName("Samping Mode");
    d->glyphs_number_spin_box->setObjectName("Max number");
    d->glyphs_stride_sb->setObjectName("Stride");
    d->glyphs_offset_spin_box->setObjectName("Offset");
    d->glyphs_scale_factor_sb->setObjectName("Scale Factor");


    d->styling_box = new dtkVisualizationWidgetsCategory("Styling", {
            d->glyphs_scale_mode_cb,
            d->mask_mode_combo_box,
            d->glyphs_number_spin_box,
            d->glyphs_stride_sb,
            d->glyphs_offset_spin_box,
            d->glyphs_scale_factor_sb,
            d_func()->opacity_widget
    });

    d_func()->inspectors << d->styling_box;

    //////////
    // Inspectors setup

    d->glyphs_scale_mode_cb->addItem("No Scaling");
    d->glyphs_scale_mode_cb->addItem("Magnitude");
    d->glyphs_scale_mode_cb->addItem("Component X");
    d->glyphs_scale_mode_cb->addItem("Component Y");
    d->glyphs_scale_mode_cb->addItem("Component Z");
    d->glyphs_scale_mode_cb->setCurrentIndex(0);

    d->mask_mode_combo_box->addItem("No randomness");
    d->mask_mode_combo_box->addItem("Randomized strides");
    d->mask_mode_combo_box->addItem("Random sample");
    d->mask_mode_combo_box->addItem("Spatially stratified");
    d->mask_mode_combo_box->setCurrentIndex(0);

    d->glyphs_stride_sb->setValue(2);
    d->glyphs_stride_sb->setMaximum(9999999);
    d->glyphs_stride_sb->setKeyboardTracking(false);

    d->glyphs_scale_factor_sb->setValue(0.1);
    d->glyphs_scale_factor_sb->setDecimals(5);
    d->glyphs_scale_factor_sb->setMaximum(100000);
    d->glyphs_scale_factor_sb->setKeyboardTracking(false);

    //////////
    // Inspectors connections

    connect(d->glyphs_scale_mode_cb, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int component_id) {
        this->saveSettings("scaling_mode", component_id);
        this->touch();
        this->draw();
    });

    connect(d->mask_mode_combo_box, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int mode) {
        this->saveSettings("mask_mode", mode);
        d->mask_points->SetRandomMode(mode != 0);
        if (mode > 0) {
            d->mask_points->SetRandomModeType(mode - 1);
        }
        d->styling_box->setVisible(d->glyphs_stride_sb, mode < 2);
        d->styling_box->setVisible(d->glyphs_offset_spin_box, mode < 2);

        this->draw();
    });

    connect(d->glyphs_number_spin_box, QOverload<int>::of(&QSpinBox::valueChanged), [this](int value) {
        this->saveSettings("mask_num_points", value);
        d->mask_points->SetMaximumNumberOfPoints(value);
        this->draw();
    });

    connect(d->glyphs_offset_spin_box, QOverload<int>::of(&QSpinBox::valueChanged), [this](int value) {
        this->saveSettings("mask_offset", value);
        d->mask_points->SetOffset(value);
        this->draw();
    });

    connect(d->glyphs_stride_sb, QOverload<int>::of(&QSpinBox::valueChanged), [this](int value) {
        this->saveSettings("mask_ratio", value);
        d->mask_points->SetOnRatio(value);
        this->draw();
    });

    connect(d->glyphs_scale_factor_sb, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
        this->saveSettings("scale_factor", value);
        d->glyphs_mapper->SetScaleFactor(value);
        this->draw();
    });

    this->setObjectName("Vector Glyphs");
}

dtkVisualizationDecoratorVectorGlyphs::~dtkVisualizationDecoratorVectorGlyphs(void)
{
    dtkVisualizationDecoratorVectorGlyphs::unsetCanvas();

    delete d->mask_mode_combo_box;
    delete d;
    d = nullptr;
}

void dtkVisualizationDecoratorVectorGlyphs::restoreSettings(void)
{
    QString name = this->objectName();
    if (name.isEmpty()) {
        return;
    }

    dtkVisualizationDecoratorWithClut::restoreSettings();

    QSettings settings;
    settings.beginGroup("canvas");

    int scaling_mode = settings.value(name + "_scaling_mode").toInt();
    d->glyphs_scale_mode_cb->blockSignals(true);
    d->glyphs_scale_mode_cb->setCurrentIndex(scaling_mode);
    d->glyphs_mapper->SetScaleMode(scaling_mode < 2 ? scaling_mode : scaling_mode - 2);
    if (scaling_mode > 1) {
        d->glyphs_mapper->SetArrayComponent(scaling_mode - 2);
    }
    d->glyphs_scale_mode_cb->blockSignals(false);

    const int mask_mode = settings.value(name + "_mask_mode").toInt();
    d->mask_mode_combo_box->blockSignals(true);
    d->mask_mode_combo_box->setCurrentIndex(mask_mode);
    d->mask_points->SetRandomMode(mask_mode != 0);
    if (mask_mode > 0) {
        d->mask_points->SetRandomModeType(mask_mode - 1);
    }
    d->styling_box->setVisible(d->glyphs_stride_sb, mask_mode < 2);
    d->styling_box->setVisible(d->glyphs_offset_spin_box, mask_mode < 2);
    d->mask_mode_combo_box->blockSignals(false);

    const int mask_num_points = settings.value(name + "_mask_num_points").toInt();
    d->glyphs_number_spin_box->blockSignals(true);
    d->glyphs_number_spin_box->setValue(mask_num_points);
    d->mask_points->SetMaximumNumberOfPoints(mask_num_points);
    d->glyphs_number_spin_box->blockSignals(false);

    const int mask_offset = settings.value(name + "_mask_offset").toInt();
    d->glyphs_offset_spin_box->blockSignals(true);
    d->glyphs_offset_spin_box->setValue(mask_offset);
    d->mask_points->SetOffset(mask_offset);
    d->glyphs_offset_spin_box->blockSignals(false);

    const int mask_ratio = settings.value(name + "_mask_ratio").toInt();
    d->glyphs_stride_sb->blockSignals(true);
    d->glyphs_stride_sb->setValue(mask_ratio);
    d->mask_points->SetOnRatio(mask_ratio);
    d->glyphs_stride_sb->blockSignals(false);

    const double scale_factor = settings.value(name + "_scale_factor").toInt();
    d->glyphs_scale_factor_sb->blockSignals(true);
    d->glyphs_scale_factor_sb->setValue(scale_factor);
    d->glyphs_mapper->SetScaleFactor(scale_factor);
    d->glyphs_scale_factor_sb->blockSignals(false);

    settings.endGroup();
}

void dtkVisualizationDecoratorVectorGlyphs::setData(const QVariant& data)
{
    vtkDataSet *dataset = data.value<vtkDataSet *>();

    if (!dataset) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet is expected. Input data is not stored.";
        return;
    }

    d_func()->clear();
    d_func()->retrieveVectorPoints(dataset);
    d_func()->retrieveVectorCells(dataset);

    if (!this->isDecorating()) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet has no field to decorate. Nothing is done.";
        d_func()->clear();
        return;
    }

    d_func()->dataset = dataset;
    this->restoreSettings();

    // Try to set an acceptable value for the scaling factor.
    if (d->adjustGlyphs) {
        adjustGlyphSizeToDataSet();
    }

    d->glyphs_number_spin_box->setMaximum(dataset->GetNumberOfCells());

    d->cell_centers->SetInputData(dataset);

    d_func()->sortEligibleFields();
    this->setCurrentFieldName(d_func()->current_field_name);

    if (this->canvas()) {
        this->canvas()->renderer()->AddActor(d->actor);
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorVectorGlyphs::setAutoAdjustGlyphSize(bool value)
{
    d->adjustGlyphs = value;
    if (d->adjustGlyphs) {
        adjustGlyphSizeToDataSet();
    }
}

void dtkVisualizationDecoratorVectorGlyphs::adjustGlyphSizeToDataSet()
{
    if (!d_func() || !d_func()->dataset) {
        return;
    }
    double bounds[6];
    d_func()->dataset->GetBounds(bounds);
    double max_dim = 1.;
    double exponent = 1;
    if (std::abs(bounds[1] - bounds[0]) > 1.e-10) {
        max_dim = std::abs(bounds[1] - bounds[0]);
    }
    if (std::abs(bounds[3] - bounds[2]) > 1.e-10) {
        max_dim *= std::abs(bounds[3] - bounds[2]);
        exponent += 1;
    }
    if (std::abs(bounds[5] - bounds[4]) > 1.e-10) {
        max_dim *= std::abs(bounds[5] - bounds[4]);
        exponent += 1;
    }

    double measure = std::pow(max_dim / d_func()->dataset->GetNumberOfCells(), 1. / exponent);

    d->glyphs_scale_factor_sb->blockSignals(true);
    d->glyphs_scale_factor_sb->setValue(measure);
    d->glyphs_mapper->SetScaleFactor(measure);
    d->glyphs_scale_factor_sb->blockSignals(false);
}

void dtkVisualizationDecoratorVectorGlyphs::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d_func()->view = dynamic_cast<dtkVisualizationView2D *>(canvas);
    if (!d_func()->view) {
        qWarning() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    if (d->glyphs_mapper->GetInput()) {
        d_func()->view->renderer()->AddActor(d->actor);
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorVectorGlyphs::unsetCanvas(void)
{
    if (d_func()->view) {
        d_func()->view->renderer()->RemoveActor(d->actor);
    }

    d_func()->disableScalarBar();

    d_func()->view = nullptr;
}

void dtkVisualizationDecoratorVectorGlyphs::touch(void)
{
    switch(d->glyphs_scale_mode_cb->currentIndex()) {
    case 0: // No Scaling
        d->glyphs_mapper->SetScaleMode(vtkGlyph3DMapper::ScaleModes::NO_DATA_SCALING);
        break;
    case 1: // Magnitude
        d->glyphs_mapper->SetScaleMode(vtkGlyph3DMapper::ScaleModes::SCALE_BY_MAGNITUDE);
        break;
    case 2: // Component X
        d->glyphs_mapper->SetScaleMode(vtkGlyph3DMapper::ScaleModes::SCALE_BY_COMPONENTS);
        d->glyphs_mapper->SetArrayComponent(0);
        break;
    case 3: // Component Y
        d->glyphs_mapper->SetScaleMode(vtkGlyph3DMapper::ScaleModes::SCALE_BY_COMPONENTS);
        d->glyphs_mapper->SetArrayComponent(1);
        break;
    case 4: // Component Y
        d->glyphs_mapper->SetScaleMode(vtkGlyph3DMapper::ScaleModes::SCALE_BY_COMPONENTS);
        d->glyphs_mapper->SetArrayComponent(2);
        break;
    default:
        break;
    }

    dtkVisualizationDecoratorWithClut::touch();
}

void dtkVisualizationDecoratorVectorGlyphs::setVisibility(bool visible)
{
    d->actor->SetVisibility(visible);
    dtkVisualizationDecoratorWithClut::setVisibility(visible);
}

bool dtkVisualizationDecoratorVectorGlyphs::setCurrentFieldName(const QString& field_name)
{
    if (field_name.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "Field name is empty, nothing is done.";
        return false;
    }

    if (!d_func()->dataset) {
        dtkWarn() << Q_FUNC_INFO << "Before calling setCurrentFieldName, setDataSet must be called.";
        return false;
    }

    if(!d_func()->eligible_field_names.contains(field_name)) {
        dtkWarn() << Q_FUNC_INFO << "The field name :" << field_name << "that was specified doesn't match any of the eligible scalar field names";
        return false;
    }

    d_func()->current_field_name = field_name;

    using Support = dtkVisualizationDecoratorWithClut::Support;
    int support = d_func()->supports[field_name];
    if(support == Support::Point) {
        d->mask_points->SetInputData(d_func()->dataset);
    } else if(support == Support::Cell) {
        d->cell_centers->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, qPrintable(field_name));
        d->mask_points->SetInputConnection(d->cell_centers->GetOutputPort());
    }
    d->mask_points->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, qPrintable(field_name));
    d->mask_points->Modified();

    d->glyphs_mapper->SetOrientationArray(qPrintable(field_name));
    d->glyphs_mapper->SetScaleArray(qPrintable(field_name));
    d->glyphs_mapper->Modified();

    return dtkVisualizationDecoratorWithClut::setCurrentFieldName(field_name);
}

void dtkVisualizationDecoratorVectorGlyphs::setColorMap(const QMap<double, QColor>& new_colormap)
{
    dtkVisualizationDecoratorWithClut::setColorMap(new_colormap);

    d->glyphs_mapper->SetLookupTable(d_func()->color_function);
    d->glyphs_mapper->SelectColorArray(qPrintable(d_func()->current_field_name));
    auto&& range = d_func()->ranges[d_func()->current_field_name];
    d->glyphs_mapper->SetScalarRange(range[0], range[1]);
    d->glyphs_mapper->Modified();
}

void dtkVisualizationDecoratorVectorGlyphs::setOpacity(double opacity)
{
    d->actor->GetProperty()->SetOpacity(opacity);
    d->actor->Modified();
}

//
// dtkVisualizationDecoratorVectorGlyphs.cpp ends here

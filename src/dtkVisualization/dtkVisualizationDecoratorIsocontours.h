// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVisualizationExport>

#include "dtkVisualizationDecoratorWithClut.h"

class DTKVISUALIZATION_EXPORT dtkVisualizationDecoratorIsocontours : public dtkVisualizationDecoratorWithClut
{
    Q_OBJECT
public:
     dtkVisualizationDecoratorIsocontours(void);
    ~dtkVisualizationDecoratorIsocontours(void);

protected:
    void touchRange(void) override;

public:
    void setData(const QVariant&) override;
    void setCanvas(dtkVisualizationCanvas *) override;
    void unsetCanvas(void) override;

protected:
    void restoreSettings(void) override;
    void setCurrentRange(double, double) override;
    bool setCurrentFieldName(const QString&) override;
    void setColorMap(const QMap<double, QColor>&) override;
    void setOpacity(double) override;

public:
    void setVisibility(bool) override;

public:
    void setCurrentIsolinesCount(std::size_t);

protected:
    void updateContours(void);

protected:
    class dtkVisualizationDecoratorIsocontoursPrivate *d = nullptr;
};

//
// dtkVisualizationDecoratorIsocontours.h ends here

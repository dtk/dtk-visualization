// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationDecoratorVectorStreamlines.h"

#include "dtkVisualizationDecoratorWithClut_p.h"

#include "dtkVisualizationMetaType.h"

#include "dtkVisualizationCanvas.h"
#include "dtkVisualizationDecoratorClutEditorBase.h"
#include "dtkVisualizationView2D.h"

#include <dtkVisualizationWidgets/dtkVisualizationWidgetsCategory>

#include <dtkWidgets>
#include <dtkLog>

#include <QtGui/QColor>

#include <vtkActor.h>
#include <vtkBoundingBox.h>
#include <vtkCellData.h>
#include <vtkColorTransferFunction.h>
#include <vtkDataSet.h>
#include <vtkEvenlySpacedStreamlines2D.h>
#include <vtkLookupTable.h>
#include <vtkMatrix4x4.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSphereRepresentation.h>
#include <vtkSphereWidget2.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkTubeFilter.h>

// /////////////////////////////////////////////////////////////////
// dtkStreamTracerObserver
// /////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorStreamlinesSphereObserver : public vtkCommand
{
private:
    dtkVisualizationDecoratorStreamlinesSphereObserver(void) = default;

public:
    static dtkVisualizationDecoratorStreamlinesSphereObserver *New(void)
    {
        return new dtkVisualizationDecoratorStreamlinesSphereObserver;
    };

    void Execute(vtkObject *caller, unsigned long event, void *) override
    {
        if (event == vtkCommand::InteractionEvent) {
            double *center = source_sphere->GetCenter();
            //streamlines->SetStartPosition(center[0], center[1], 0.);
        }
    };

public:
    vtkSmartPointer<vtkSphereRepresentation> source_sphere;
    vtkSmartPointer<vtkEvenlySpacedStreamlines2D> streamlines;
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorStreamlinesPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorVectorStreamlinesPrivate
{
public:
    vtkSmartPointer<dtkVisualizationDecoratorStreamlinesSphereObserver> source_sphere_observer;
    vtkSmartPointer<vtkSphereWidget2> source_sphere_widget;
    vtkSmartPointer<vtkSphereRepresentation> source_sphere;

public:
    vtkSmartPointer<vtkTransform> transform;
    vtkSmartPointer<vtkTransformFilter> transform_filter;
    vtkSmartPointer<vtkEvenlySpacedStreamlines2D> streamlines;
    vtkSmartPointer<vtkTubeFilter> tube_filter;
    vtkSmartPointer<vtkPolyDataMapper> mapper;
    vtkSmartPointer<vtkActor> actor;

public:
    QCheckBox *show_source_actor_cb = nullptr;
    QDoubleSpinBox *sp_radius = nullptr;
    QDoubleSpinBox *sp_start_pos_x = nullptr;
    QDoubleSpinBox *sp_start_pos_y = nullptr;

public:
    double bounds[6]{};
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorStreamlines implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorVectorStreamlines::dtkVisualizationDecoratorVectorStreamlines(void)
        : dtkVisualizationDecoratorWithClut(), d(new dtkVisualizationDecoratorVectorStreamlinesPrivate())
{
    vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();
    matrix->Identity();
    matrix->SetElement(2, 2, 0.);

    d->transform = vtkSmartPointer<vtkTransform>::New();
    d->transform->SetMatrix(matrix);

    d->transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
    d->transform_filter->SetTransform(d->transform);

    d->streamlines = vtkSmartPointer<vtkEvenlySpacedStreamlines2D>::New();
    d->streamlines->SetInputConnection(d->transform_filter->GetOutputPort());
    d->streamlines->SetIntegrationStepUnit(2); // <=> vtkStreamTracer::Units::CELL_LENGTH_UNIT
    d->streamlines->SetIntegratorTypeToRungeKutta4();
    d->streamlines->SetInitialIntegrationStep(0.2);
    d->streamlines->SetMaximumNumberOfSteps(2000);
    d->streamlines->SetSeparatingDistance(2);
    d->streamlines->SetSeparatingDistanceRatio(0.4);
    d->streamlines->SetClosedLoopMaximumDistance(0.2);
    d->streamlines->SetTerminalSpeed(1.e-12);

    d->tube_filter = vtkSmartPointer<vtkTubeFilter>::New();
    d->tube_filter->SetInputConnection(d->streamlines->GetOutputPort());
    d->tube_filter->SetNumberOfSides(8);
    d->tube_filter->SetRadius(.02);
    d->tube_filter->SetVaryRadius(0);

    d->mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    d->mapper->SetInputConnection(d->tube_filter->GetOutputPort());
    d->mapper->SetScalarModeToUsePointFieldData();

    d->actor = vtkSmartPointer<vtkActor>::New();
    d->actor->SetMapper(d->mapper);

    d->source_sphere = vtkSmartPointer<vtkSphereRepresentation>::New();
    d->source_sphere->SetPhiResolution(7);
    d->source_sphere->SetThetaResolution(7);
    d->source_sphere->SetRadius(1.0);
    d->source_sphere->SetRepresentationToWireframe();
    d->source_sphere->RadialLineOff();

    d->source_sphere_widget = vtkSmartPointer<vtkSphereWidget2>::New();
    d->source_sphere_widget->SetEnabled(false);
    d->source_sphere_widget->SetRepresentation(d->source_sphere);

    d->source_sphere_observer = vtkSmartPointer<dtkVisualizationDecoratorStreamlinesSphereObserver>::New();
    d->source_sphere_observer->source_sphere = d->source_sphere;
    d->source_sphere_observer->streamlines = d->streamlines;

    d->source_sphere_widget->AddObserver(vtkCommand::InteractionEvent, d->source_sphere_observer);

    //////////
    // Inspectors creation and setup

    d->show_source_actor_cb = new QCheckBox;
    d->show_source_actor_cb->setChecked(true);

    d->sp_radius = new QDoubleSpinBox;
    d->sp_radius->setObjectName("Tube Radius");
    d->sp_radius->setValue(1.0);
    d->sp_radius->setSingleStep(0.1);
    d->sp_radius->setDecimals(4);
    d->sp_radius->setKeyboardTracking(false);

    d->sp_start_pos_x = new QDoubleSpinBox;
    d->sp_start_pos_x->setSingleStep(0.1);
    d->sp_start_pos_x->setDecimals(6);
    d->sp_start_pos_x->setKeyboardTracking(false);
    d->sp_start_pos_y = new QDoubleSpinBox;
    d->sp_start_pos_y->setSingleStep(0.1);
    d->sp_start_pos_y->setDecimals(6);
    d->sp_start_pos_y->setKeyboardTracking(false);

    auto h_layout_start_pos = new QHBoxLayout;
    h_layout_start_pos->addWidget(d->sp_start_pos_x);
    h_layout_start_pos->addWidget(d->sp_start_pos_y);
    auto start_pos_widget = new QWidget;
    start_pos_widget->setObjectName("Start Position");
    start_pos_widget->setLayout(h_layout_start_pos);

    //////////
    // Inspectors connections

    connect(d->show_source_actor_cb, &QCheckBox::stateChanged, [=,this](int state) {
        d->source_sphere_widget->SetEnabled(state == Qt::Checked);
        this->draw();
    });

    connect(d->sp_radius, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this](double val) {
        d->tube_filter->SetRadius(val);
        this->draw();
    });

    connect(d->sp_start_pos_x, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this](double val) {
        double *pos = d->streamlines->GetStartPosition();
        pos[0] = val;

        d->streamlines->SetStartPosition(pos);
        d->source_sphere->SetCenter(pos);
        d->source_sphere->Modified();
        //d->streamlines->Modified();
        qDebug() << Q_FUNC_INFO
                 << "Changing start position is not available since VTK crash. Have to wait for next version of VTK.";

        this->draw();
    });

    connect(d->sp_start_pos_y, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this](double val) {
        double *pos = d->streamlines->GetStartPosition();
        pos[1] = val;

        d->streamlines->SetStartPosition(pos);
        d->source_sphere->SetCenter(pos);
        d->source_sphere->Modified();
        //d->streamlines->Modified();
        qDebug() << Q_FUNC_INFO
                 << "Changing start position is not available since VTK crash. Have to wait for next version of VTK.";
        this->draw();
    });

    this->setObjectName("Vector Streamlines 2D");


    QWidget *styling_box = new dtkVisualizationWidgetsCategory("Styling", {
            start_pos_widget,
            d->sp_radius,
            d_func()->opacity_widget
    });

    d_func()->inspectors << styling_box;
}

dtkVisualizationDecoratorVectorStreamlines::~dtkVisualizationDecoratorVectorStreamlines(void)
{
    dtkVisualizationDecoratorVectorStreamlines::unsetCanvas();

    delete d;

    d = nullptr;
}

void dtkVisualizationDecoratorVectorStreamlines::restoreSettings(void)
{
    QString name = this->objectName();
    if (name.isEmpty()) {
        return;
    }
    dtkVisualizationDecoratorWithClut::restoreSettings();

    d->actor->SetVisibility(d_func()->default_visibility);
}

bool dtkVisualizationDecoratorVectorStreamlines::isCurrentFieldUniform(void)
{
    auto r = d_func()->ranges[d_func()->current_field_name];
    return std::abs(r[0] - r[1]) < 1.e-12;
}

void dtkVisualizationDecoratorVectorStreamlines::setData(const QVariant& data)
{
    auto *dataset = data.value<vtkDataSet *>();

    if (!dataset) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet is expected. Input data is not stored.";
        return;
    }

    dataset->GetBounds(d->bounds);
    vtkBoundingBox box;
    box.SetBounds(d->bounds);

    if (d->bounds[4] != d->bounds[5]) {
        dtkWarn() << Q_FUNC_INFO << "Vector stream lines decorator only works with 2D datasets";
        d_func()->clear();
        return;
    }

    d_func()->clear();
    d_func()->retrieveVectorPoints(dataset);
    d_func()->retrieveVectorCells(dataset);

    if (!this->isDecorating()) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet has no field to decorate. Nothing is done.";
        d_func()->clear();
        return;
    }

    d_func()->dataset = dataset;
    this->restoreSettings();

    double center[3];
    box.GetCenter(center);

    d->sp_radius->blockSignals(true);
    d->sp_radius->setValue(box.GetDiagonalLength() / 500.0);
    d->tube_filter->SetRadius(box.GetDiagonalLength() / 500.0);
    d->sp_radius->blockSignals(false);

    d->transform_filter->SetInputData(dataset);

    d->streamlines->SetStartPosition(center[0], center[1], 0.);
    d->source_sphere->SetCenter(center[0], center[1], 0.);

    d->sp_start_pos_x->blockSignals(true);
    d->sp_start_pos_x->setMinimum(d->bounds[0]);
    d->sp_start_pos_x->setMaximum(d->bounds[1]);
    d->sp_start_pos_x->setSingleStep(std::abs(d->bounds[1] - d->bounds[0]) / 100);
    d->sp_start_pos_x->setValue(center[0]);
    d->sp_start_pos_x->blockSignals(false);
    d->sp_start_pos_y->blockSignals(true);
    d->sp_start_pos_y->setValue(center[1]);
    d->sp_start_pos_y->setMinimum(d->bounds[2]);
    d->sp_start_pos_y->setMaximum(d->bounds[3]);
    d->sp_start_pos_y->setSingleStep(std::abs(d->bounds[3] - d->bounds[2]) / 100);
    d->sp_start_pos_y->blockSignals(false);

    d->streamlines->Modified();
    d->mapper->Modified();

    d_func()->sortEligibleFields();
    this->setCurrentFieldName(d_func()->current_field_name);

    if (this->canvas()) {
        this->canvas()->renderer()->AddActor(d->actor);
        d->source_sphere_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());
        d->source_sphere_widget->On();
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorVectorStreamlines::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d_func()->view = dynamic_cast<dtkVisualizationView2D *>(canvas);
    if (!d_func()->view) {
        qWarning() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    if (this->isDecorating()) {
        d_func()->view->renderer()->AddActor(d->actor);
        d->source_sphere_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());
        if (!d_func()->default_visibility) {
            d_func()->default_visibility = true;
        } else {
            d->source_sphere_widget->On();
        }
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorVectorStreamlines::unsetCanvas(void)
{
    if (d_func()->view) {
        d->source_sphere_widget->Off();
        d->source_sphere_widget->SetInteractor(nullptr);
        d_func()->view->renderer()->RemoveActor(d->actor);
    }

    d_func()->disableScalarBar();

    d_func()->view = nullptr;
}

void dtkVisualizationDecoratorVectorStreamlines::touch(void)
{
    dtkVisualizationDecoratorWithClut::touch();
}

void dtkVisualizationDecoratorVectorStreamlines::setVisibility(bool visible)
{
    dtkVisualizationDecoratorWithClut::setVisibility(visible);
    d->actor->SetVisibility(visible);
    if (d->source_sphere_widget->GetInteractor()) {
        d->source_sphere_widget->SetEnabled(visible);
    }
}

bool dtkVisualizationDecoratorVectorStreamlines::setCurrentFieldName(const QString& field_name)
{
    if (field_name.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "Field name is empty, nothing is done.";
        return false;
    }

    if (!d_func()->dataset) {
        dtkWarn() << Q_FUNC_INFO << "Before calling setCurrentFieldName, setDataSet must be called.";
        return false;
    }

    if (!d_func()->eligible_field_names.contains(field_name)) {
        dtkWarn() << Q_FUNC_INFO << "The field name :" << field_name
                  << "that was specified doesn't match any of the eligible scalar field names";
        return false;
    }

    d_func()->current_field_name = field_name;

    using Support = dtkVisualizationDecoratorWithClut::Support;
    int support = d_func()->supports[field_name];
    if (support == Support::Point) {
        d->streamlines->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, qPrintable(field_name));
    } else if (support == Support::Cell) {
        d->streamlines->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, qPrintable(field_name));
    }
    d->streamlines->Modified();

    return dtkVisualizationDecoratorWithClut::setCurrentFieldName(field_name);
}

void dtkVisualizationDecoratorVectorStreamlines::setColorMap(const QMap<double, QColor>& new_colormap)
{
    dtkVisualizationDecoratorWithClut::setColorMap(new_colormap);

    d->mapper->SetLookupTable(d_func()->color_function);
    d->mapper->SelectColorArray(qPrintable(d_func()->current_field_name));
    auto&& range = d_func()->ranges[d_func()->current_field_name];
    d->mapper->SetScalarRange(range[0], range[1]);
    d->mapper->Modified();
}

void dtkVisualizationDecoratorVectorStreamlines::setOpacity(double opacity)
{
    d->actor->GetProperty()->SetOpacity(opacity);
    d->actor->Modified();
}

//
// dtkVisualizationDecoratorVectorStreamlines.cpp ends here

// dtkVisualizationDecoratorVectorCurvedGlyphs.cpp
//

#include "dtkVisualizationDecoratorVectorCurvedGlyphs.h"

#include "dtkVisualizationDecoratorWithClut_p.h"
#include "dtkVisualizationMetaType.h"
#include "dtkVisualizationView2D.h"

#include <dtkVisualizationWidgets/dtkVisualizationWidgetsCategory>

#include <dtkLog>

#include <QtGui>
#include <QtWidgets>

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkBoundingBox.h>
#include <vtkCellData.h>
#include <vtkClipPolyData.h>
#include <vtkColorTransferFunction.h>
#include <vtkContourFilter.h>
#include <vtkGlyph3DMapper.h>
#include <vtkLineRepresentation.h>
#include <vtkLineSource.h>
#include <vtkLineWidget2.h>
#include <vtkLookupTable.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPointData.h>
#include <vtkPointSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereRepresentation.h>
#include <vtkSphereWidget2.h>
#include <vtkStreamTracer.h>
#include <vtkTubeFilter.h>

// /////////////////////////////////////////////////////////////////
// dtkCurvedGlyphsObserver
// /////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorCurvedGlyphsSphereObserver : public vtkCommand
{
private:
    dtkVisualizationDecoratorCurvedGlyphsSphereObserver(void) {
    };

public:
    static dtkVisualizationDecoratorCurvedGlyphsSphereObserver *New(void)
    {
        return new dtkVisualizationDecoratorCurvedGlyphsSphereObserver;
    };

    void Execute(vtkObject *caller, unsigned long event, void *) override
    {
        if (event == vtkCommand::InteractionEvent) {
            source_sphere->GetPolyData(source_data);
            stream_tracer->SetSourceData(source_data);
            stream_tracer->Modified();
        }
    };

public:
    vtkSmartPointer<vtkPolyData> source_data;
    vtkSmartPointer<vtkSphereRepresentation> source_sphere;
    vtkSmartPointer<vtkStreamTracer> stream_tracer;
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorCurvedGlyphsPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorVectorCurvedGlyphsPrivate
{
public:
    vtkSmartPointer<dtkVisualizationDecoratorCurvedGlyphsSphereObserver> source_sphere_observer;
    vtkSmartPointer<vtkSphereWidget2> source_sphere_widget;
    vtkSmartPointer<vtkPolyData> source_data;
    vtkSmartPointer<vtkSphereRepresentation> source_sphere;

public:
    vtkSmartPointer<vtkStreamTracer> streamtracer;
    vtkSmartPointer<vtkContourFilter> contours;
    vtkSmartPointer<vtkArrowSource> source_arrows;
    vtkSmartPointer<vtkGlyph3DMapper> glyphs_mapper;
    vtkSmartPointer<vtkActor> arrows_actor;
    vtkSmartPointer<vtkStreamTracer> curves_stream;
    vtkSmartPointer<vtkTubeFilter> curves_tube;
    vtkSmartPointer<vtkClipPolyData> curves_clip;
    vtkSmartPointer<vtkPolyDataMapper> curves_mapper;
    vtkSmartPointer<vtkActor> curves_actor;

public:
    QCheckBox *show_source_actor_cb = nullptr;

public:
    QComboBox *cb_integrator_type = nullptr;
    QSpinBox *sp_integrator_max_steps = nullptr;
    QSpinBox *sp_resolution = nullptr;
    QDoubleSpinBox *sp_integrator_max_lengths = nullptr;

public:
    QDoubleSpinBox *sp_radius = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorCurvedGlyphs implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorVectorCurvedGlyphs::dtkVisualizationDecoratorVectorCurvedGlyphs(void): dtkVisualizationDecoratorWithClut(), d(new dtkVisualizationDecoratorVectorCurvedGlyphsPrivate())
{

    d->source_data = vtkSmartPointer<vtkPolyData> ::New();

    d->source_sphere = vtkSmartPointer<vtkSphereRepresentation>::New();
    d->source_sphere->SetPhiResolution(7);
    d->source_sphere->SetThetaResolution(7);
    d->source_sphere->SetRadius(1.0);
    d->source_sphere->GetPolyData(d->source_data);
    d->source_sphere->SetRepresentationToWireframe();
    d->source_sphere->RadialLineOff();

    d->source_sphere_widget = vtkSmartPointer<vtkSphereWidget2>::New();
    d->source_sphere_widget->SetEnabled(false);
    d->source_sphere_widget->SetRepresentation(d->source_sphere);

    d->streamtracer = vtkSmartPointer<vtkStreamTracer>::New();
    d->streamtracer->SetSourceData(d->source_data);

    d->streamtracer->SetIntegrationDirectionToBoth();
    d->streamtracer->SetIntegratorTypeToRungeKutta45();
    d->streamtracer->SetIntegrationStepUnit(vtkStreamTracer::Units::LENGTH_UNIT);
    d->streamtracer->SetInitialIntegrationStep(0.2);
    d->streamtracer->SetMinimumIntegrationStep(0.01);
    d->streamtracer->SetMaximumIntegrationStep(0.5);

    d->streamtracer->SetMaximumNumberOfSteps(2000);
    d->streamtracer->SetMaximumPropagation(20); // To be set as the maximum size of the dataset
    d->streamtracer->SetTerminalSpeed(1.e-12);
    d->streamtracer->SetMaximumError(1.e-6);
    d->streamtracer->SetComputeVorticity(false);

    d->contours = vtkSmartPointer<vtkContourFilter>::New();
    d->contours->SetInputConnection(d->streamtracer->GetOutputPort());
    d->contours->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "IntegrationTime");
    d->contours->ComputeNormalsOff();
    d->contours->ComputeGradientsOff();
    d->contours->ComputeScalarsOn();

    d->source_arrows = vtkSmartPointer<vtkArrowSource>::New();
    d->source_arrows->SetTipLength(1);
    d->source_arrows->SetTipRadius(0.2);

    d->glyphs_mapper = vtkSmartPointer<vtkGlyph3DMapper>::New();
    d->glyphs_mapper->SetSourceConnection(d->source_arrows->GetOutputPort());
    d->glyphs_mapper->SetInputConnection(d->contours->GetOutputPort());
    d->glyphs_mapper->SetOrientationModeToDirection();
    d->glyphs_mapper->SetScalarModeToUsePointFieldData();
    d->glyphs_mapper->SetScaleMode(vtkGlyph3DMapper::ScaleModes::NO_DATA_SCALING);
    d->glyphs_mapper->OrientOn();
    d->glyphs_mapper->ScalingOn();
    d->glyphs_mapper->SetScaleFactor(1.);

    d->arrows_actor = vtkSmartPointer<vtkActor>::New();
    d->arrows_actor->SetMapper(d->glyphs_mapper);

    d->curves_stream = vtkSmartPointer<vtkStreamTracer>::New();
    d->curves_stream->SetSourceConnection(d->contours->GetOutputPort());

    d->curves_stream->SetIntegrationDirectionToBackward();
    d->curves_stream->SetIntegratorTypeToRungeKutta45();
    d->curves_stream->SetIntegrationStepUnit(vtkStreamTracer::Units::LENGTH_UNIT);
    d->curves_stream->SetInitialIntegrationStep(0.2);
    d->curves_stream->SetMinimumIntegrationStep(0.01);
    d->curves_stream->SetMaximumIntegrationStep(0.5);

    d->curves_stream->SetMaximumNumberOfSteps(200);
    d->curves_stream->SetMaximumPropagation(2); // To be set as the maximum size of the dataset
    d->curves_stream->SetTerminalSpeed(1.e-12);
    d->curves_stream->SetMaximumError(1.e-6);
    d->curves_stream->SetComputeVorticity(false);

    d->curves_tube = vtkSmartPointer<vtkTubeFilter>::New();
    d->curves_tube->SetInputConnection(d->curves_stream->GetOutputPort());
    d->curves_tube->SetNumberOfSides(8);
    d->curves_tube->SetRadius(.02);
    d->curves_tube->SetVaryRadius(0);

    d->curves_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    d->curves_mapper->SetInputConnection(d->curves_tube->GetOutputPort());
    d->curves_mapper->SetScalarModeToUsePointFieldData();

    d->curves_actor = vtkSmartPointer<vtkActor>::New();
    d->curves_actor->SetMapper(d->curves_mapper);

    d->source_sphere_observer = vtkSmartPointer<dtkVisualizationDecoratorCurvedGlyphsSphereObserver>::New();
    d->source_sphere_observer->source_data = d->source_data;
    d->source_sphere_observer->source_sphere = d->source_sphere;
    d->source_sphere_observer->stream_tracer = d->streamtracer;

    d->source_sphere_widget->AddObserver(vtkCommand::InteractionEvent, d->source_sphere_observer);

    //////////
    // Inspectors creation and setup

    d->show_source_actor_cb = new QCheckBox;

    d->cb_integrator_type = new QComboBox;
    d->cb_integrator_type->addItem(QStringLiteral("RK2"));
    d->cb_integrator_type->addItem(QStringLiteral("RK4"));
    d->cb_integrator_type->addItem(QStringLiteral("RK45"));
    d->cb_integrator_type->setCurrentIndex(0);

    d->sp_resolution = new QSpinBox;
    d->sp_resolution->setMaximum(1000);
    d->sp_resolution->setValue(50);
    d->sp_resolution->setKeyboardTracking(false);

    d->sp_integrator_max_steps = new QSpinBox;
    d->sp_integrator_max_steps->setMaximum(999999);
    d->sp_integrator_max_steps->setValue(2000);
    d->sp_integrator_max_steps->setKeyboardTracking(false);

    d->sp_integrator_max_lengths = new QDoubleSpinBox;
    d->sp_integrator_max_lengths->setMaximum(9999999);
    d->sp_integrator_max_lengths->setValue(20);
    d->sp_integrator_max_lengths->setKeyboardTracking(false);

    d->sp_radius = new QDoubleSpinBox;

    d->show_source_actor_cb->setChecked(true);
    d->sp_radius->setValue(1.0);
    d->sp_radius->setSingleStep(0.1);
    d->sp_radius->setDecimals(4);
    d->sp_radius->setKeyboardTracking(false);

    //////////
    // Inspectors connections

    connect(d->show_source_actor_cb, &QCheckBox::stateChanged, [=,this] (int state)
    {
        d->source_sphere_widget->SetEnabled(state == Qt::Checked);
        this->draw();
    });

    connect(d->cb_integrator_type, QOverload<int>::of(&QComboBox::currentIndexChanged), [=,this] (int id)
    {
        d->streamtracer->SetIntegratorType(id);
        this->draw();
    });

    connect(d->sp_resolution, QOverload<int>::of(&QSpinBox::valueChanged), [=,this] (int val)
    {
        int sqrt_val = std::round(std::sqrt(val));
        d->source_sphere->SetPhiResolution(sqrt_val);
        d->source_sphere->SetThetaResolution(sqrt_val);
        this->touch();
        this->draw();
    });

    connect(d->sp_integrator_max_steps, QOverload<int>::of(&QSpinBox::valueChanged), [=,this] (int val)
    {
        d->streamtracer->SetMaximumNumberOfSteps(val);
        this->draw();
    });

    connect(d->sp_integrator_max_lengths, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this] (double val)
    {
        d->streamtracer->SetMaximumPropagation(val);
        this->draw();
    });

    connect(d->sp_radius, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this] (double val)
    {
        d->curves_tube->SetRadius(val);
        this->draw();
    });

    this->setObjectName("Vector Curved Glyphs");

    d->show_source_actor_cb->setObjectName("Display Source");
    d->cb_integrator_type->setObjectName("Integrator Type");
    d->sp_integrator_max_steps->setObjectName("Maximum Steps");
    d->sp_integrator_max_lengths->setObjectName("Maximum Streamline Length");
    d->sp_radius->setObjectName("Tube Radius");
    d->sp_resolution->setObjectName("Resolution");

    d_func()->inspectors << d->show_source_actor_cb
                         << d->cb_integrator_type << d->sp_integrator_max_steps
                         << d->sp_integrator_max_lengths << d->sp_radius << d->sp_resolution;

}

dtkVisualizationDecoratorVectorCurvedGlyphs::~dtkVisualizationDecoratorVectorCurvedGlyphs(void)
{
    dtkVisualizationDecoratorVectorCurvedGlyphs::unsetCanvas();

    delete d;

    d = nullptr;
}

void dtkVisualizationDecoratorVectorCurvedGlyphs::restoreSettings(void)
{
    dtkVisualizationDecoratorWithClut::restoreSettings();
}

void dtkVisualizationDecoratorVectorCurvedGlyphs::setData(const QVariant& data)
{
    vtkDataSet *dataset = data.value<vtkDataSet *>();

    if (!dataset) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet is expected. Input data is not stored.";
        return;
    }

    d_func()->clear();
    d_func()->retrieveVectorPoints(dataset);
    d_func()->retrieveVectorCells(dataset);

    if (!this->isDecorating()) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet has no field to decorate. Nothing is done.";
        d_func()->clear();
        return;
    }

    d_func()->dataset = dataset;
    this->restoreSettings();

    double bounds[6];
    double center[3];
    dataset->GetBounds(bounds);
    dataset->GetCenter(center);
    double pos1[3]; pos1[0] = bounds[0]; pos1[1] = bounds[1]; pos1[2] = bounds[2];
    double pos2[3]; pos2[0] = bounds[3]; pos2[1] = bounds[4]; pos2[2] = bounds[5];
    vtkBoundingBox box;
    box.SetBounds(bounds);
    d->sp_integrator_max_lengths->setValue(box.GetDiagonalLength());
    d->sp_radius->setValue(box.GetDiagonalLength()/500.0);

    d->source_sphere->SetCenter(center);
    d->source_sphere->SetRadius(box.GetDiagonalLength()/20.0);
    d->source_sphere->Modified();

    d->streamtracer->SetInputData(dataset);
    d->streamtracer->Modified();

    d->curves_stream->SetInputData(dataset);
    d->curves_stream->Modified();

    d_func()->sortEligibleFields();
    this->setCurrentFieldName(d_func()->current_field_name);

    if (this->canvas()) {
        this->canvas()->renderer()->AddActor(d->arrows_actor);
        this->canvas()->renderer()->AddActor(d->curves_actor);
        d->source_sphere_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());
        d->source_sphere_widget->On();
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorVectorCurvedGlyphs::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d_func()->view = dynamic_cast<dtkVisualizationView2D *>(canvas);
    if (!d_func()->view) {
        dtkWarn() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    if (this->isDecorating()) {
        d_func()->view->renderer()->AddActor(d->arrows_actor);
        d_func()->view->renderer()->AddActor(d->curves_actor);
        d->source_sphere_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());
        if (!d_func()->default_visibility) {
            d_func()->default_visibility = true;
        } else {
            d->source_sphere_widget->On();
        }
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorVectorCurvedGlyphs::unsetCanvas(void)
{
    if (d_func()->view) {
        d->source_sphere_widget->Off();
        d->source_sphere_widget->SetInteractor(nullptr);
        d_func()->view->renderer()->RemoveActor(d->arrows_actor);
        d_func()->view->renderer()->RemoveActor(d->curves_actor);
    }

    d_func()->disableScalarBar();

    d_func()->view = nullptr;
}

void dtkVisualizationDecoratorVectorCurvedGlyphs::touch(void)
{
    dtkVisualizationDecoratorWithClut::touch();

    d->source_sphere->GetPolyData(d->source_data);
    d->streamtracer->SetSourceData(d->source_data);
    d->streamtracer->Modified();

    auto&& range = d_func()->ranges[d_func()->current_field_name];

    d->contours->GenerateValues(23, range[0], range[1]);
    d->contours->Modified();
    d->curves_stream->Modified();

}

void dtkVisualizationDecoratorVectorCurvedGlyphs::setVisibility(bool visible)
{
    dtkVisualizationDecoratorWithClut::setVisibility(visible);
    d->arrows_actor->SetVisibility(visible);
    d->curves_actor->SetVisibility(visible);
    if (visible && this->canvas()) {
        d->source_sphere_widget->SetEnabled(d->show_source_actor_cb->isChecked());
    } else {
        d->show_source_actor_cb->setChecked(false);
    }
}

bool dtkVisualizationDecoratorVectorCurvedGlyphs::setCurrentFieldName(const QString& field_name)
{
    if (field_name.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "Field name is empty, nothing is done.";
        return false;
    }

    if (!d_func()->dataset) {
        dtkWarn() << Q_FUNC_INFO << "Before calling setCurrentFieldName, setDataSet must be called.";
        return false;
    }

    if(!d_func()->eligible_field_names.contains(field_name)) {
        dtkWarn() << Q_FUNC_INFO << "The field name :" << field_name << "that was specified doesn't match any of the eligible scalar field names";
        return false;
    }

    d_func()->current_field_name = field_name;

    using Support = dtkVisualizationDecoratorWithClut::Support;
    int support = d_func()->supports[field_name];
    if(support == Support::Point) {
        d->streamtracer->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, qPrintable(field_name));
        d->curves_stream->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, qPrintable(field_name));

    } else if(support == Support::Cell) {
        d->streamtracer->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, qPrintable(field_name));
        d->curves_stream->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, qPrintable(field_name));
    }
    d->streamtracer->Modified();
    d->curves_stream->Modified();

    d->glyphs_mapper->SetOrientationArray(qPrintable(field_name));
    d->glyphs_mapper->SetScaleArray(qPrintable(field_name));
    d->glyphs_mapper->Modified();

    return dtkVisualizationDecoratorWithClut::setCurrentFieldName(field_name);
}

void dtkVisualizationDecoratorVectorCurvedGlyphs::setColorMap(const QMap<double, QColor>& new_colormap)
{
    dtkVisualizationDecoratorWithClut::setColorMap(new_colormap);

    d->glyphs_mapper->SetLookupTable(d_func()->color_function);
    d->curves_mapper->SetLookupTable(d_func()->color_function);
    d->glyphs_mapper->SelectColorArray(qPrintable(d_func()->current_field_name));
    d->curves_mapper->SelectColorArray(qPrintable(d_func()->current_field_name));
    auto&& range = d_func()->ranges[d_func()->current_field_name];
    d->glyphs_mapper->SetScalarRange(range[0], range[1]);
    d->glyphs_mapper->Modified();
    d->curves_mapper->SetScalarRange(range[0], range[1]);
    d->curves_mapper->Modified();
}

//
// dtkVisualizationDecoratorVectorCurvedGlyphs.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVisualizationExport>

#include <dtkWidgets/dtkWidgetsWidget>

class vtkRenderer;
class vtkRenderWindowInteractor;
class vtkScalarBarWidget;

class dtkWidgetsHUD;

class QWidget;

class DTKVISUALIZATION_EXPORT dtkVisualizationCanvas : public dtkWidgetsWidget
{
    Q_OBJECT

public:
     dtkVisualizationCanvas(QWidget *parent = nullptr);
    ~dtkVisualizationCanvas(void);

public:
    QWidget *widget(void) override;

public slots:
    void link(dtkVisualizationCanvas *other);
    void unlink(void);

public:
    dtkWidgetsHUD *hud(void);

public:
    virtual void reset(void);
    void draw(void);
    void scale(double x, double y, double z);

public:
    vtkRenderer *renderer(void);
    vtkRenderWindowInteractor *interactor(void);

public:
    void addScalarBar(vtkScalarBarWidget *, bool scalar_bar_is_seek_bar = false);
    void removeScalarBar(vtkScalarBarWidget *);

public:
    virtual void setTitle(const QString &);
    void setBackground(QColor color);

private:
    class dtkVisualizationCanvasPrivate *d;
};

//
// dtkVisualizationCanvas.h ends here

#pragma once

#include <dtkVisualizationExport.h>

#include <dtkVisualization>

class DTKVISUALIZATION_EXPORT dtkVisualizationDecoratorVertexEdge : public dtkVisualizationDecorator
{
    Q_OBJECT

public:
    dtkVisualizationDecoratorVertexEdge(void);
    ~dtkVisualizationDecoratorVertexEdge(void) override;

public:
    void touch(void) override;

protected:
    void restoreSettings(void) override;

public:
    void setVisibility(bool) override;
    void setData(const QVariant&) override;
    void setCanvas(dtkVisualizationCanvas *) override;

 public:
    bool isDecorating(void) override;
    void unsetCanvas(void) override;

 public:
    QVariant data(void) const override;
    QList<QWidget *> inspectors(void) const override;
    dtkVisualizationCanvas *canvas(void) const override;

signals:
    void updated(void);

protected:
    class dtkVisualizationDecoratorVertexEdgePrivate *d;
};

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVisualizationExport>

#include <QtCore>

#include <vtkSmartPointer.h>
#include <vtkPiecewiseFunction.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarWidget.h>
#include <vtkTransform.h>

#include <array>

class dtkVisualizationView2D;
class dtkVisualizationWidgetsColorDialog;
class dtkVisualizationWidgetsColorMapEditor;

class QCheckBox;
class QComboBox;
class QDoubleSpinBox;
class QLineEdit;
class QPushButton;
class QSlider;
class QLabel;

class vtkColorTransferFunction;
class vtkDataSet;
class vtkDataSetAttributes;
class vtkLookupTable;

class DTKVISUALIZATION_EXPORT dtkVisualizationDecoratorWithClutPrivate
{
public:
    class dtkVisualizationDecoratorWithClut *q = nullptr;

public:
    enum Support {
        Unknown = 0,
        Point = 1,
        Cell = 2
    };

    enum Kind {
        Scalar = 0x001,
        Vector = 0x003,
        Tensor = 0x009
    };

public:
     dtkVisualizationDecoratorWithClutPrivate(void);
    ~dtkVisualizationDecoratorWithClutPrivate(void) = default;

public:
    vtkDataSet *dataset = nullptr;
    dtkVisualizationView2D *view = nullptr;

public:
    QList<QWidget *> inspectors;

    QComboBox *fields_box  = nullptr;
    QComboBox *field_components = nullptr;

    QWidget *range_widget = nullptr;
    QLineEdit *min_range   = nullptr;
    QLineEdit *max_range   = nullptr;
    QWidget *range_widget2 = nullptr;
    QCheckBox *dynamic_range_button = nullptr;
    QPushButton *reset_range = nullptr;
    bool original_range = true;

    QCheckBox *show_scalar_bar = nullptr;
    QCheckBox *show_actor_cb = nullptr;

    QWidget *opacity_widget = nullptr;
    QSlider *opacity_slider = nullptr;
    QDoubleSpinBox *opacity_val_sp = nullptr;

    dtkVisualizationWidgetsColorDialog *col_dialog = nullptr;
    dtkVisualizationWidgetsColorMapEditor *colormap_editor = nullptr;

public:
    QStringList eligible_field_names;
    QString current_field_name;
    int current_field_component;
    QString default_field_name;
    int default_field_component;
    QString default_color_map;
    bool default_visibility;
    bool default_show_scalar_bar;
    bool scalar_bar_label_includes_decorator_name;
    bool scalar_bar_is_seek_bar; //* This is used to know where to position scalar bars on the bottom edge (if there's a seek bar it must be a bit higher.)

public:
    vtkSmartPointer<vtkColorTransferFunction> color_function;
    vtkSmartPointer<vtkPiecewiseFunction> opacity;
    vtkSmartPointer<vtkTransform> transform;

    vtkSmartPointer<vtkScalarBarActor> scalar_bar;
    vtkSmartPointer<vtkScalarBarWidget> scalar_bar_widget;

    QHash<QString, QString> color_transfer_functions; // <field_name, colormap_name>
    QHash<QString, vtkSmartPointer<vtkPiecewiseFunction>> opacity_functions;
    QHash<QString, std::array<double,2>> ranges;
    QHash<QString, Support> supports;
    QHash<QString, Kind> kinds;

public:
    void clear(void);
    void enableScalarBar(void);
    void disableScalarBar(void);
    void retrieveScalarPoints(vtkDataSet *);
    void retrieveScalarCells(vtkDataSet *);
    void retrieveVectorPoints(vtkDataSet *);
    void retrieveVectorCells(vtkDataSet *);
    void sortEligibleFields(void);
    void updateRange(void);
    void updateColorTransferFunction(void);

public:
    QWidget *inspectorItem(const QString&) const;

public:
    void updateDisabledWidgets();

private:
    void retrieveScalarData(vtkDataSetAttributes *, Support support);
    void retrieveVectorData(vtkDataSetAttributes *, Support support);
};

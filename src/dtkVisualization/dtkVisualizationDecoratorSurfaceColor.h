// dtkVisualizationDecoratorSurfaceColor.h
//

#pragma once

#include <dtkVisualizationExport>

#include "dtkVisualizationDecoratorWithClut.h"

class DTKVISUALIZATION_EXPORT dtkVisualizationDecoratorSurfaceColor : public dtkVisualizationDecoratorWithClut
{
    Q_OBJECT

public:
     dtkVisualizationDecoratorSurfaceColor(void);
    ~dtkVisualizationDecoratorSurfaceColor(void);

public:
    void setData(const QVariant&) override;
    void setCanvas(dtkVisualizationCanvas *) override;
    void setTransform(vtkTransform *) override;
    void unsetCanvas(void) override;

public:
    void setVisibility(bool) override;

public:
    void setInputConnection(Connection *) override;
    Connection *outputPort(void) override;

protected:
    void restoreSettings(void) override;
    bool setCurrentFieldName(const QString&) override;
    void setColorMap(const QMap<double, QColor>&) override;
    void setOpacity(double) override;

protected:
    class dtkVisualizationDecoratorSurfaceColorPrivate *d;
};

//
// dtkVisualizationDecoratorSurfaceColor.h ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVisualizationExport>

#include "dtkVisualizationCanvas.h"

#include <vtkChartXY.h>
#include <vtkPlotPoints.h>

template <typename T> class vtkSmartPointer;
class vtkAbstractArray;
class dtkWidgetsMenu;

class DTKVISUALIZATION_EXPORT dtkVisualizationPlot2D : public dtkVisualizationCanvas
{
    Q_OBJECT

public:
     dtkVisualizationPlot2D(QWidget *parent = nullptr);
    ~dtkVisualizationPlot2D(void);

public:
    void setGridVisible(bool visible);
    QStringList fields(void);

public:
    dtkWidgetsMenu *menu(void) const;

public:
    int numSources();
    void addSource(QColor color = Qt::transparent);
    void setSourceColor(int index, QColor color);
    void setSelectedFields(QStringList);
    void removeSource(int index);
    void addFieldX(vtkSmartPointer<vtkAbstractArray> field, int source = 0);
    void addFieldY(vtkSmartPointer<vtkAbstractArray>,
                   bool checked = false,
                   int type = vtkChart::POINTS,
                   int marker_style = vtkPlotPoints::PLUS,
                   QColor color = Qt::transparent,
                   int width = 3,
                   int source = 0);
    void clearFields(void);
    void removeField(vtkSmartPointer<vtkAbstractArray>);
    void removeField(const QString&);

 public:
    void reset(void) override;

public:
    QStringList selectedFields(void);
    void setAddPlotSuffix(const QString& s);
    void setShowLegend(bool);

private slots:
    void exportAsCSV(const QString &);
    void exportAsPNG(const QString &);

protected:
    void selectX(const QString&);
    QString selectedX(void);
    void render(void);
    //void keyPressEvent(QKeyEvent *e);

private:
    class dtkVisualizationPlot2DPrivate *d;

};

//
// dtkVisualizationPlot2D.h ends here

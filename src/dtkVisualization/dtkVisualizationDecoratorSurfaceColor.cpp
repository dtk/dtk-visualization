// dtkVisualizationDecoratorSurfaceColor.cpp
//

#include "dtkVisualizationDecoratorSurfaceColor.h"

#include "dtkVisualizationDecoratorWithClut_p.h"
#include "dtkVisualizationMetaType.h"
#include "dtkVisualizationView2D.h"

#include <dtkVisualizationWidgetsCategory.h>

#include <dtkLog>

#include <QtGui>
#include <QtWidgets>

#include <vtkActor.h>
#include <vtkAlgorithmOutput.h>
#include <vtkColorTransferFunction.h>
#include <vtkCellData.h>
#include <vtkDataSet.h>
#include <vtkDataSetMapper.h>
#include <vtkLookupTable.h>
#include <vtkPointData.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorSurfaceColorPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorSurfaceColorPrivate
{
public:
    vtkSmartPointer<vtkActor> actor;
    vtkSmartPointer<vtkDataSetMapper> mapper;
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorSurfaceColor implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorSurfaceColor::dtkVisualizationDecoratorSurfaceColor(void): dtkVisualizationDecoratorWithClut(), d(new dtkVisualizationDecoratorSurfaceColorPrivate())
{
    d->mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    d->mapper->SetColorModeToMapScalars();
    d->mapper->SetScalarVisibility(true);

    d->actor = vtkSmartPointer<vtkActor>::New();
    d->actor->SetMapper(d->mapper);

    //////////
    // Inspectors connections

    connect(d_func()->show_actor_cb, &QCheckBox::stateChanged, [=,this] (int state)
    {
        this->saveSettings("visibility",state == Qt::Checked);
        this->setVisibility(state == Qt::Checked);
        this->draw();
    });

    this->setObjectName("Surface Color");

    QWidget *styling_box = new dtkVisualizationWidgetsCategory("Styling", {
            d_func()->opacity_widget
        });

    d_func()->inspectors << styling_box;
}

dtkVisualizationDecoratorSurfaceColor::~dtkVisualizationDecoratorSurfaceColor(void)
{
    this->unsetCanvas();

    delete d;
    d = nullptr;
}

void dtkVisualizationDecoratorSurfaceColor::setData(const QVariant& data)
{
    vtkDataSet *dataset = data.value<vtkDataSet *>();

    if (!dataset) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet is expected. Input data is not stored.";
        return;
    }

    d_func()->clear();
    d_func()->retrieveScalarPoints(dataset);
    d_func()->retrieveScalarCells(dataset);
    d_func()->retrieveVectorPoints(dataset);
    d_func()->retrieveVectorCells(dataset);

    if (!this->isDecorating()) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet has no field to decorate. Nothing is done.";
        d_func()->clear();
        return;
    }
    d_func()->dataset = dataset;
    this->restoreSettings();
    d->mapper->SetInputData(d_func()->dataset);

    d_func()->sortEligibleFields();
    this->setCurrentFieldName(d_func()->current_field_name);

    if (this->canvas()) {
        this->canvas()->renderer()->AddActor(d->actor);
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorSurfaceColor::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d_func()->view = dynamic_cast<dtkVisualizationView2D *>(canvas);
    if (!d_func()->view) {
        qWarning() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    if (d->mapper->GetInput()) {
        d_func()->view->renderer()->AddActor(d->actor);
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorSurfaceColor::unsetCanvas(void)
{
    if (d_func()->view) {
        d_func()->view->renderer()->RemoveActor(d->actor);
    }

    d_func()->disableScalarBar();

    d_func()->view = nullptr;
}

void dtkVisualizationDecoratorSurfaceColor::setVisibility(bool visible)
{
    d->actor->SetVisibility(visible);
    dtkVisualizationDecoratorWithClut::setVisibility(visible);
}

void dtkVisualizationDecoratorSurfaceColor::setInputConnection(Connection *connection)
{
    vtkDataSet *data = vtkDataSet::SafeDownCast(connection->GetProducer()->GetOutputDataObject(connection->GetIndex()));
    if (!data)
        return;
    this->setData(dtk::variantFromValue(data));

    d->mapper->SetInputConnection(connection);
}

dtkVisualizationDecorator::Connection *dtkVisualizationDecoratorSurfaceColor::outputPort(void)
{
    return d->mapper->GetOutputPort();
}

void dtkVisualizationDecoratorSurfaceColor::setOpacity(double opacity)
{
    d->actor->GetProperty()->SetOpacity(opacity);
    d->actor->Modified();
}

void dtkVisualizationDecoratorSurfaceColor::restoreSettings(void)
{
    dtkVisualizationDecoratorWithClut::restoreSettings();
}

bool dtkVisualizationDecoratorSurfaceColor::setCurrentFieldName(const QString& field_name)
{
    if (!dtkVisualizationDecoratorWithClut::setCurrentFieldName(field_name)) {
        return false;
    }

    using Support = dtkVisualizationDecoratorWithClut::Support;
    int support = d_func()->supports[field_name];
    if(support == Support::Point) {
        d->mapper->SetScalarModeToUsePointFieldData();
    } else if(support == Support::Cell) {
        d->mapper->SetScalarModeToUseCellFieldData();
    }
    d->mapper->SelectColorArray(qPrintable(field_name));
    d->mapper->Modified();

    return true;
}

void dtkVisualizationDecoratorSurfaceColor::setColorMap(const QMap<double, QColor>& new_colormap)
{
    dtkVisualizationDecoratorWithClut::setColorMap(new_colormap);

    d->mapper->SetLookupTable(d_func()->color_function);
    d->mapper->SelectColorArray(qPrintable(d_func()->current_field_name));
    auto&& range = d_func()->ranges[d_func()->current_field_name];
    d->mapper->SetScalarRange(range[0], range[1]);
    d->mapper->Modified();
}

void dtkVisualizationDecoratorSurfaceColor::setTransform(vtkTransform *transform)
{
    d_func()->transform = transform;
    d->actor->SetUserTransform(transform);
}

//
// dtkVisualizationDecoratorSurfaceColor.cpp ends here

#pragma once

#include <dtkVisualizationExport.h>

#include <dtkVisualization>

class DTKVISUALIZATION_EXPORT dtkVisualizationDecoratorClipper : public dtkVisualizationDecorator
{
    Q_OBJECT

public:
     dtkVisualizationDecoratorClipper(void);
    ~dtkVisualizationDecoratorClipper(void);

public:
    bool isDecorating(void) override;

protected:
    void restoreSettings(void) override;

public:
    void setData(const QVariant&)            override;
    void setCanvas(dtkVisualizationCanvas *) override;
    void setCollection(dtkVisualizationDecoratorCollection *);
    void unsetCanvas(void)                   override;

public:
    QVariant data(void) const override;
    dtkVisualizationCanvas *canvas(void) const override;

public:
    void setVisibility(bool) override;

public:
    QList<QWidget *> inspectors(void) const override;
    QWidget *getInspectorsWidget(void) const;

public:
    void setInputConnection(Connection *) override;
    Connection *outputPort(void) override;
    void setOutputConnection(dtkVisualizationDecorator *output_decorator);
    void setOutputConnections(QList<dtkVisualizationDecorator *> output_decorators);

public slots:
    void touch(void) override;

protected:
    class dtkVisualizationDecoratorClipperPrivate *d;
};

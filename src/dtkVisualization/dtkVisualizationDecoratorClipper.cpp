#include "dtkVisualizationDecoratorClipper.h"

#include <QtWidgets>

#include <vtkExtractGeometry.h>
#include <vtkClipDataSet.h>
#include <vtkCommand.h>
#include <vtkPlane.h>
#include <vtkImplicitPlaneWidget2.h>
#include <vtkImplicitPlaneRepresentation.h>
#include <vtkRenderWindowInteractor.h>

class dtkVisualizationDecoratorClipperPrivate
{
public:
    dtkVisualizationView3D *view;

public:
    QList<QWidget *> inspectors;

public:
    //collection containing this decorator
    dtkVisualizationDecoratorCollection *collection = nullptr;
    QList<dtkVisualizationDecorator *> hidden_decorators;

public:
    vtkSmartPointer<vtkPlane> plane = nullptr;
    vtkSmartPointer<vtkDataSet> data = nullptr;
    vtkSmartPointer<vtkExtractGeometry> extractor = nullptr;
    vtkSmartPointer<vtkClipDataSet> clipper = nullptr;
    vtkSmartPointer<vtkUnstructuredGridAlgorithm> current_clipper = nullptr;
    QList<dtkVisualizationDecorator *> output_decorators;

    QRadioButton *button_novisu = nullptr;
    QRadioButton *button_noclip = nullptr;
    QRadioButton *button_clip = nullptr;
    QRadioButton *button_extract = nullptr;

public:
    vtkSmartPointer<vtkImplicitPlaneRepresentation> representation;
    vtkSmartPointer<vtkImplicitPlaneWidget2> widget;
};

class dtkVisualizationDecoratorClipperCallBack : public vtkCommand
{
public:
    static dtkVisualizationDecoratorClipperCallBack *New(void)
    {
        return new dtkVisualizationDecoratorClipperCallBack;
    }

    virtual void Execute(vtkObject *caller, unsigned long, void*)
    {
        vtkImplicitPlaneWidget2 *planeWidget = reinterpret_cast<vtkImplicitPlaneWidget2*>(caller);
        vtkImplicitPlaneRepresentation *rep = reinterpret_cast<vtkImplicitPlaneRepresentation*>(planeWidget->GetRepresentation());
        rep->GetPlane(this->plane);
    }

    dtkVisualizationDecoratorClipperCallBack():
        plane(nullptr)
    {}

public:
    vtkSmartPointer<vtkPlane> plane;
    vtkSmartPointer<vtkRenderWindowInteractor> interactor;
};



dtkVisualizationDecoratorClipper::dtkVisualizationDecoratorClipper(void): d(new dtkVisualizationDecoratorClipperPrivate())
{
    this->setObjectName("Clipping");
    d->plane = vtkSmartPointer<vtkPlane>::New();

    d->extractor = vtkSmartPointer<vtkExtractGeometry>::New();
    d->extractor->SetImplicitFunction(d->plane);

    d->clipper = vtkSmartPointer<vtkClipDataSet>::New();
    d->clipper->SetClipFunction(d->plane);
    d->clipper->SetInsideOut(true);

    d->representation = vtkSmartPointer<vtkImplicitPlaneRepresentation>::New();
    d->representation->SetPlaceFactor(1.25);
    d->representation->SetNormal(d->plane->GetNormal());

    d->widget = vtkSmartPointer<vtkImplicitPlaneWidget2>::New();
    d->widget->SetRepresentation(d->representation);

// /////////////////////////////////////////////////////////////////////////////
// Inspectors
// /////////////////////////////////////////////////////////////////////////////

    QGroupBox *box = new QGroupBox;
    QBoxLayout *layout = new QVBoxLayout;
    box->setLayout(layout);
    d->inspectors << box;
    box->setStyleSheet("QGroupBox{background-color:transparent; border-color:transparent; margin: 0; padding: 0; spacing: 0}");

    d->button_novisu = new QRadioButton("None");
    d->button_noclip = new QRadioButton("No Clipping");
    d->button_clip = new QRadioButton("Clip");
    d->button_clip->setToolTip("Clipping can be very CPU-intensive. \"Extract cells\" should be preferred when performance is poor.");
    d->button_extract = new QRadioButton("Extract cells");
    d->button_extract->setToolTip("Provide a coarser-grain clipping algorithm than \"Clip\"");
    layout->addWidget(d->button_novisu);
    layout->addWidget(d->button_noclip);
    layout->addWidget(d->button_clip);
    layout->addWidget(d->button_extract);

    auto update_current_clipper = [=,this] (vtkSmartPointer<vtkUnstructuredGridAlgorithm> chosen_clipper) {
        if(!d->hidden_decorators.isEmpty()) {
            for(auto dec : d->hidden_decorators) {
                dec->setVisibility(true);
            }
        }
        d->hidden_decorators.clear();

        d->current_clipper = chosen_clipper;
        const bool clipping = chosen_clipper != nullptr;
        if (clipping) {
            for (auto *output_decorator: d->output_decorators) {
                output_decorator->setInputConnection(d->current_clipper->GetOutputPort());
                output_decorator->touch();
            }
        } else {
            for (auto *output_decorator: d->output_decorators) {
                output_decorator->setData(dtk::variantFromValue(d->data.Get()));
                output_decorator->touch();
            }
        }

        this->saveSettings("clipper",
                           chosen_clipper == d->clipper ? "Clip" :
                           chosen_clipper == d->extractor ? "Extract" :
                           "None");

        if (this->canvas()) {
            d->widget->SetEnabled(clipping);
            this->draw();
        }
    };

    d->button_noclip->setChecked(true);
    d->widget->SetEnabled(false);

    connect(d->button_novisu, &QRadioButton::released, [=,this] () {
       d->hidden_decorators.clear();
       if(d->collection) {
          for(auto *dec : *d->collection) {
              if(dec->isVisible()) {
                  d->hidden_decorators << dec;
                  dec->setVisibility(false);
              }
          }
      } else {
           d->hidden_decorators << this;
          this->setVisibility(false);
      }
      if(this->canvas())
          this->draw();

    });
    connect(d->button_noclip, &QRadioButton::released, [=] () {
        update_current_clipper(nullptr);
    });
    connect(d->button_clip, &QRadioButton::released, [=,this] () {
        update_current_clipper(d->clipper);
    });
    connect(d->button_extract, &QRadioButton::released, [=,this] () {
        update_current_clipper(d->extractor);
    });
}

dtkVisualizationDecoratorClipper::~dtkVisualizationDecoratorClipper(void)
{
    d->view = nullptr;

    delete d; d = nullptr;
}

void dtkVisualizationDecoratorClipper::setCollection(dtkVisualizationDecoratorCollection *coll)
{
    d->collection = coll;
}

void dtkVisualizationDecoratorClipper::restoreSettings(void)
{
    QSettings settings;
    settings.beginGroup("canvas");
    const QString saved_clipper = settings.value(this->objectName() + "_clipper", "Clip").toString();
    settings.endGroup();

    QRadioButton *saved_button =
        saved_clipper == "Clip" ? d->button_clip :
        saved_clipper == "Extract" ? d->button_extract :
        d->button_noclip;

    saved_button->setChecked(true);
    emit saved_button->released();
}

void dtkVisualizationDecoratorClipper::touch(void)
{
    if(!this->canvas()) {
        dtkWarn() << Q_FUNC_INFO << "No canvas was set, call setCanvas to call draw on a canvas.";
        return;
    }

    Q_ASSERT(this->canvas()->renderer());

    if (this->canvas()->interactor()) {
        this->canvas()->interactor()->Render();
    }
}

bool dtkVisualizationDecoratorClipper::isDecorating(void)
{
    return d->data != nullptr;
}

void dtkVisualizationDecoratorClipper::setData(const QVariant& data)
{
    if(dynamic_cast<vtkStructuredGrid *>(data.value<vtkStructuredGrid *>()))
        d->data = vtkSmartPointer<vtkDataSet>(data.value<vtkStructuredGrid *>());

    if(dynamic_cast<vtkUnstructuredGrid *>(data.value<vtkUnstructuredGrid *>()))
        d->data = vtkSmartPointer<vtkDataSet>(data.value<vtkUnstructuredGrid *>());

    if(dynamic_cast<vtkPolyData *>(data.value<vtkPolyData *>()))
        d->data = vtkSmartPointer<vtkDataSet>(data.value<vtkPolyData *>());

    if(dynamic_cast<vtkImageData *>(data.value<vtkImageData *>()))
        d->data = vtkSmartPointer<vtkDataSet>(data.value<vtkImageData *>());

    if (d->data == nullptr) {
        dtkWarn() << Q_FUNC_INFO << "No compatible data found.";
        return;
    }

    d->representation->PlaceWidget(d->data->GetBounds());

    d->extractor->SetInputData(d->data);
    d->extractor->Modified();
    d->extractor->Update();

    d->clipper->SetInputData(d->data);
    d->clipper->Modified();
    d->clipper->Update();

    this->draw();
}

QVariant dtkVisualizationDecoratorClipper::data(void) const
{
    if (d->data)
        return dtk::variantFromValue(d->data.Get());

    return QVariant();
}


dtkVisualizationCanvas *dtkVisualizationDecoratorClipper::canvas(void) const
{
    return d->view;
}

void dtkVisualizationDecoratorClipper::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d->view = dynamic_cast<dtkVisualizationView3D *>(canvas);

    if (!d->view) {
        qWarning() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    vtkSmartPointer<dtkVisualizationDecoratorClipperCallBack> callback = vtkSmartPointer<dtkVisualizationDecoratorClipperCallBack>::New();
    callback->plane = d->plane;
    callback->interactor = d->view->interactor();

    d->widget->SetInteractor(d->view->interactor());
    d->widget->AddObserver(vtkCommand::InteractionEvent, callback);
    d->widget->On();

    callback->Execute(d->widget, 0, 0);

    if (d->view->interactor())
        d->view->interactor()->Render();
}

void dtkVisualizationDecoratorClipper::unsetCanvas(void)
{
    d->view = nullptr;
}

void dtkVisualizationDecoratorClipper::setVisibility(bool visible)
{
    dtkVisualizationDecorator::setVisibility(visible);
    d->widget->SetEnabled(visible && (d->current_clipper != nullptr));
    for (auto *output_decorator: d->output_decorators) {
        output_decorator->setVisibility(visible);
    }
}

QList<QWidget *> dtkVisualizationDecoratorClipper::inspectors(void) const
{
    return d->inspectors;
}

QWidget *dtkVisualizationDecoratorClipper::getInspectorsWidget() const
{
    auto inspectors_layout = new QFormLayout;

    foreach(auto *inspector, d->inspectors){
        if(dynamic_cast<QPushButton*>(inspector))
            inspectors_layout->addRow(inspector);
        else
            inspectors_layout->addRow(inspector->objectName(), inspector);
    }
    auto inspectors_widget = new QWidget;
    inspectors_widget->setLayout(inspectors_layout);

    return inspectors_widget;
}

void dtkVisualizationDecoratorClipper::setInputConnection(Connection *connection)
{
    d->clipper->SetInputConnection(connection);
    d->extractor->SetInputConnection(connection);
}

dtkVisualizationDecoratorClipper::Connection *dtkVisualizationDecoratorClipper::outputPort(void)
{
    if (d->current_clipper == nullptr) {
        return nullptr;
    }
    return d->current_clipper->GetOutputPort();
}

void dtkVisualizationDecoratorClipper::setOutputConnection(dtkVisualizationDecorator *output_decorator)
{
    setOutputConnections({output_decorator});
}

void dtkVisualizationDecoratorClipper::setOutputConnections(QList<dtkVisualizationDecorator *> output_decorators)
{
    if (d->data == nullptr) {
        dtkWarn() << Q_FUNC_INFO << "setData() must be called before initialize()";
    }
    if (canvas() == nullptr) {
        dtkWarn() << Q_FUNC_INFO << "setCanvas() must be called before initialize()";
    }
    if (isDecorating() == false) {
        return;
    }

    QList<dtkVisualizationDecorator *> decorating_output_decorators;
    for (auto* output_decorator: output_decorators) {
        if (output_decorator->isDecorating()) {
            decorating_output_decorators << output_decorator;
        }
    }

    d->output_decorators = decorating_output_decorators;

    this->restoreSettings();
}

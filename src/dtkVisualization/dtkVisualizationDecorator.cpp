// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationDecorator.h"

#include "dtkVisualization.h"
#include "dtkVisualizationCanvas.h"

#include <vtkTransform.h>

// ///////////////////////////////////////////////////////////////////
// Register to dtkVisualization layer
// ///////////////////////////////////////////////////////////////////

namespace dtk { namespace visualization {
    DTK_DEFINE_CONCEPT(dtkVisualizationDecorator, decorator, dtk::visualization);
} }

class dtkVisualizationDecoratorPrivate
{
public:
    bool is_visible = true;
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecorator implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecorator::dtkVisualizationDecorator(): d(new dtkVisualizationDecoratorPrivate)
{
}

dtkVisualizationDecorator::~dtkVisualizationDecorator()
{
    delete d;
    d = nullptr;
}

void dtkVisualizationDecorator::draw(void)
{
    if (this->canvas()) {
        this->canvas()->draw();
    }
}

void dtkVisualizationDecorator::saveSettings(const QString & name, const QVariant& value)
{
    QSettings settings;
    settings.beginGroup("canvas");
    settings.setValue(this->objectName()+"_" + name, value);
    settings.endGroup();
}

bool dtkVisualizationDecorator::isVisible()
{
    return d->is_visible;
}

void dtkVisualizationDecorator::setVisibility(bool visible)
{
    d->is_visible = visible;
}

void dtkVisualizationDecorator::show(void)
{
    this->setVisibility(true);
}

void dtkVisualizationDecorator::hide(void)
{
    this->setVisibility(false);
}

void dtkVisualizationDecorator::setObjectName(const QString & name)
{
    QObject::setObjectName(name);
}

void dtkVisualizationDecorator::setTransform(vtkTransform *transform)
{
    qWarning() << Q_FUNC_INFO << "nothing is done";
}

void dtkVisualizationDecorator::setInputConnection(Connection *)
{
}

dtkVisualizationDecorator::Connection *dtkVisualizationDecorator::outputPort(void)
{
    return nullptr;
}

//
// dtkVisualizationDecorator.cpp ends here

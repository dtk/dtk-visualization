// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationDecoratorWithClut.h"

#include "dtkVisualizationDecoratorWithClut_p.h"
#include "dtkVisualizationMetaType.h"
#include "dtkVisualizationView2D.h"

#include <dtkVisualizationWidgets/dtkVisualizationWidgetsCategory>
#include <dtkVisualizationWidgets/dtkVisualizationWidgetsColorDialog>
#include <dtkVisualizationWidgets/dtkVisualizationWidgetsColorMapEditor>

#include <dtkFonts/dtkFontAwesome>
#include <dtkLog>

#include <QtGui>
#include <QtWidgets>

#include <vtkColorTransferFunction.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDataSet.h>
#include <vtkLookupTable.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPointData.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkSmartPointer.h>
#include <vtkScalarBarWidget.h>
#include <vtkScalarBarRepresentation.h>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorWithClutPrivate implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorWithClutPrivate::dtkVisualizationDecoratorWithClutPrivate(void)
{
    this->color_function = vtkSmartPointer<vtkColorTransferFunction>::New();


    this->scalar_bar = vtkSmartPointer<vtkScalarBarActor>::New();
    this->scalar_bar_label_includes_decorator_name = true;
    this->scalar_bar_is_seek_bar = false;

    this->scalar_bar_widget = vtkSmartPointer<vtkScalarBarWidget>::New();
    scalar_bar_widget->SetScalarBarActor(scalar_bar);

    // Fields

    this->fields_box = new QComboBox;
    this->fields_box->setObjectName("Field");

    this->field_components = new QComboBox;
    this->field_components->setObjectName("Component");
    this->field_components->addItem("Uniform");
    this->field_components->addItem("Magnitude");
    this->field_components->addItem("X");
    this->field_components->addItem("Y");
    this->field_components->addItem("Z");
    this->field_components->setCurrentIndex(1);

    // Color map editor
    this->colormap_editor = new dtkVisualizationWidgetsColorMapEditor;
    this->colormap_editor->setObjectName("Color Map");

    // Color dialog
    this->col_dialog = new dtkVisualizationWidgetsColorDialog;
    this->col_dialog->setObjectName("Color");

    // Display Data
    show_actor_cb = new QCheckBox;
    show_actor_cb->setObjectName("Data");

    // Range
    QHBoxLayout *h_layout_range = new QHBoxLayout;
    this->range_widget = new QWidget;
    this->range_widget->setObjectName("Color Range");

    this->min_range = new QLineEdit;
    this->max_range = new QLineEdit;

    QDoubleValidator *double_validator = new QDoubleValidator;
    double_validator->setDecimals(9);
    this->min_range->setValidator(double_validator);
    this->max_range->setValidator(double_validator);

    h_layout_range->setAlignment(Qt::AlignLeft);
    h_layout_range->setContentsMargins(0,0,0,0);
    h_layout_range->addWidget(this->min_range);
    h_layout_range->addWidget(this->max_range);

    this->range_widget->setLayout(h_layout_range);

    QHBoxLayout *h_layout_range2 = new QHBoxLayout;
    this->range_widget2 = new QWidget;
    this->range_widget2->setObjectName("Range");

    this->dynamic_range_button = new QCheckBox;
    this->dynamic_range_button->setToolTip("Automatically or manually change color range to fit the data range (min/max)");
    this->dynamic_range_button->setText("----");
    this->dynamic_range_button->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

    this->reset_range = new QPushButton;
    this->reset_range->setText("----");
    this->reset_range->setToolTip("----");
    this->reset_range->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

    h_layout_range2->setAlignment(Qt::AlignLeft);
    h_layout_range2->setContentsMargins(0,0,0,0);
    h_layout_range2->addWidget(this->dynamic_range_button);
    h_layout_range2->addWidget(this->reset_range);

    this->range_widget2->setLayout(h_layout_range2);


    // Opacity
    this->opacity_widget = new QWidget;
    this->opacity_widget->setObjectName("Opacity");

    this->opacity_slider = new QSlider;
    this->opacity_slider->setMinimum(0);
    this->opacity_slider->setMaximum(1000);
    this->opacity_slider->setTickInterval(1);
    this->opacity_slider->setValue(1000);
    this->opacity_slider->setOrientation(Qt::Orientation::Horizontal);

    this->opacity_val_sp = new QDoubleSpinBox;
    this->opacity_val_sp->setMinimum(0);
    this->opacity_val_sp->setMaximum(1);
    this->opacity_val_sp->setSingleStep(0.01);
    this->opacity_val_sp->setDecimals(3);
    this->opacity_val_sp->setKeyboardTracking(false);
    this->opacity_val_sp->setValue(1);

    QHBoxLayout *h_layout_opacity = new QHBoxLayout;
    h_layout_opacity->setContentsMargins(0,0,0,0);
    h_layout_opacity->addWidget(this->opacity_slider);
    h_layout_opacity->addWidget(this->opacity_val_sp);

    this->opacity_widget->setLayout(h_layout_opacity);

    // Scalar bar
    this->show_scalar_bar = new QCheckBox;
    this->show_scalar_bar->setObjectName("Scalar Bar");
    this->show_scalar_bar->setChecked(false);

    // Inspectors
    this->inspectors
        << this->fields_box
        << new dtkVisualizationWidgetsCategory("Display", {
                this->show_actor_cb,
                this->show_scalar_bar } )
        << new dtkVisualizationWidgetsCategory("Colors", {
                this->field_components,
                this->colormap_editor,
                this->range_widget,
                this->range_widget2,
                this->col_dialog
            });
}

void dtkVisualizationDecoratorWithClutPrivate::clear(void)
{
    this->current_field_name.clear();
    this->eligible_field_names.clear();
    this->color_transfer_functions.clear();
    this->opacity_functions.clear();
    this->ranges.clear();
    this->supports.clear();
}

void dtkVisualizationDecoratorWithClutPrivate::enableScalarBar(void)
{
    if (this->view && this->dataset) {
        this->view->addScalarBar(this->scalar_bar_widget, this->scalar_bar_is_seek_bar);
    }
}

void dtkVisualizationDecoratorWithClutPrivate::disableScalarBar(void)
{
    if (this->view) {
        this->view->removeScalarBar(this->scalar_bar_widget);
    }
}

void dtkVisualizationDecoratorWithClutPrivate::retrieveScalarData(vtkDataSetAttributes *field_data, Support support)
{
    vtkIdType nb_arrays = field_data->GetNumberOfArrays();

    for (vtkIdType i = 0; i < nb_arrays; ++i) {
        std::size_t nb_components = field_data->GetArray(i)->GetNumberOfComponents();
        if (nb_components == 1) {
            QString field_name = QString::fromUtf8(field_data->GetArrayName(i));
            this->eligible_field_names << field_name;
            this->supports[field_name] = support;
            this->kinds[field_name] = Kind::Scalar;

            auto&& range = this->ranges[field_name];
            field_data->GetArray(i)->GetRange(range.data());
            this->color_transfer_functions[field_name] = this->default_color_map;

            auto opacity_function = vtkSmartPointer<vtkPiecewiseFunction>::New();
            opacity_function->RemoveAllPoints();
            opacity_function->AddPoint(0,   0.);
            opacity_function->AddPoint(255, 1.);

            this->opacity_functions[field_name] = opacity_function;
        }
    }
}

void dtkVisualizationDecoratorWithClutPrivate::retrieveVectorData(vtkDataSetAttributes *field_data, Support support)
{
    vtkIdType nb_arrays = field_data->GetNumberOfArrays();
    for (vtkIdType i = 0; i < nb_arrays; ++i) {
        std::size_t nb_components = field_data->GetArray(i)->GetNumberOfComponents();
        if (nb_components == 3) {
            QString field_name = QString::fromUtf8(field_data->GetArrayName(i));
            this->eligible_field_names << field_name;
            this->supports[field_name] = support;
            this->kinds[field_name] = Kind::Vector;

            auto&& range = this->ranges[field_name];
            range[0] = range[1] = 0;
            this->color_transfer_functions[field_name] = this->default_color_map;

            auto opacity_function = vtkSmartPointer<vtkPiecewiseFunction>::New();
            opacity_function->RemoveAllPoints();
            opacity_function->AddPoint(0,   0.);
            opacity_function->AddPoint(255, 1.);

            this->opacity_functions[field_name] = opacity_function;
        }
    }
}

void dtkVisualizationDecoratorWithClutPrivate::retrieveScalarPoints(vtkDataSet *dataset)
{
    vtkPointData *point_data = dataset->GetPointData();
    if (!point_data)
        return;

    this->retrieveScalarData(point_data, Support::Point);
}

void dtkVisualizationDecoratorWithClutPrivate::retrieveScalarCells(vtkDataSet *dataset)
{
    vtkCellData *cell_data = dataset->GetCellData();
    if (!cell_data)
        return;

    this->retrieveScalarData(cell_data, Support::Cell);
}

void dtkVisualizationDecoratorWithClutPrivate::retrieveVectorPoints(vtkDataSet *dataset)
{
    vtkPointData *point_data = dataset->GetPointData();
    if (!point_data)
        return;

    this->retrieveVectorData(point_data, Support::Point);
}

void dtkVisualizationDecoratorWithClutPrivate::retrieveVectorCells(vtkDataSet *dataset)
{
    vtkCellData *cell_data = dataset->GetCellData();
    if (!cell_data)
        return;

    this->retrieveVectorData(cell_data, Support::Cell);
}

void dtkVisualizationDecoratorWithClutPrivate::sortEligibleFields(void)
{
    if (this->eligible_field_names.size() > 0) {
        this->eligible_field_names.sort();

        if (this->fields_box->count() > 0) {
            this->fields_box->clear();
        }
        this->fields_box->blockSignals(true);
        this->fields_box->addItems(this->eligible_field_names);
        this->fields_box->blockSignals(false);
        this->current_field_name = this->default_field_name;
        if (this->default_field_name.isEmpty() || !this->eligible_field_names.contains(this->default_field_name)) {
            this->current_field_name = this->eligible_field_names.first();
        }
        this->fields_box->setCurrentText(this->current_field_name);
    }
}

void dtkVisualizationDecoratorWithClutPrivate::updateRange(void)
{
    auto&& range = this->ranges[this->current_field_name];

    if (this->original_range) {
        // The following trick gives directly the right index value
        // to extract in the GetRange method for the vector case.
        int component_id = this->field_components->currentIndex() - 2;

        using Kind = dtkVisualizationDecoratorWithClutPrivate::Kind;
        using Support = dtkVisualizationDecoratorWithClutPrivate::Support;

        switch (this->kinds[this->current_field_name]) {
        case Kind::Scalar:
            switch (this->supports[this->current_field_name]) {
            case Support::Point:
                this->dataset->GetPointData()->GetArray(qPrintable(this->current_field_name))->Modified();
                this->dataset->GetPointData()->GetArray(qPrintable(this->current_field_name))->GetRange(range.data(), -1);
                break;
            case Support::Cell:
                this->dataset->GetCellData()->GetArray(qPrintable(this->current_field_name))->Modified();
                this->dataset->GetCellData()->GetArray(qPrintable(this->current_field_name))->GetRange(range.data(), -1);
                break;
            default:
                break;
            }
            break;

        case Kind::Vector:
            if (component_id > -2) {
                switch (this->supports[this->current_field_name]) {
                case Support::Point:
                    this->dataset->GetPointData()->GetArray(qPrintable(this->current_field_name))->Modified();
                    this->dataset->GetPointData()->GetArray(qPrintable(this->current_field_name))->GetRange(range.data(), component_id);
                    break;
                case Support::Cell:
                    this->dataset->GetCellData()->GetArray(qPrintable(this->current_field_name))->Modified();
                    this->dataset->GetCellData()->GetArray(qPrintable(this->current_field_name))->GetRange(range.data(), component_id);
                    break;
                default:
                    break;
                }
            } else {
                range[0] = range[1] = 0;
            }
            break;
        default:
            break;
        }
    }

    this->min_range->blockSignals(true);
    this->min_range->setText(QString::number(range[0]));
    this->min_range->blockSignals(false);

    this->max_range->blockSignals(true);
    this->max_range->setText(QString::number(range[1]));
    this->max_range->blockSignals(false);
}

void dtkVisualizationDecoratorWithClutPrivate::updateColorTransferFunction(void)
{
    using Kind = dtkVisualizationDecoratorWithClutPrivate::Kind;
    // The following trick gives directly the right index value
    // to extract in the GetRange method for the vector case.
    int component_id = this->field_components->currentIndex() - 2;

    dtkVisualizationWidgetsCategory *category = dynamic_cast<dtkVisualizationWidgetsCategory *>(this->inspectorItem("Colors"));
    Q_ASSERT_X(category, Q_FUNC_INFO, "Category Colors should exist.");

    category->setVisible(this->col_dialog, false);
    category->setVisible(this->colormap_editor, true);
    category->setVisible(this->range_widget, true);

    this->colormap_editor->blockSignals(true);
    this->colormap_editor->setValue(this->color_transfer_functions[this->current_field_name]);
    this->colormap_editor->blockSignals(false);
    QMap<double, QColor> color_map = this->colormap_editor->value();

    switch (this->kinds[this->current_field_name]) {
    case Kind::Vector:
        switch(component_id) {
        case -1:
            this->color_function->SetVectorModeToMagnitude();
            break;
        case 0:
            this->color_function->SetVectorModeToComponent();
            this->color_function->SetVectorComponent(0);
            break;
        case 1:
            this->color_function->SetVectorModeToComponent();
            this->color_function->SetVectorComponent(1);
            break;
        case 2:
            this->color_function->SetVectorModeToComponent();
            this->color_function->SetVectorComponent(2);
            break;
        default:
            category->setVisible(this->col_dialog, true);
            category->setVisible(this->colormap_editor, false);
            category->setVisible(this->range_widget, false);
            this->color_function->SetVectorModeToMagnitude();
            color_map.clear();
            auto&& range = this->ranges[this->current_field_name];
            color_map[range[0]] = this->col_dialog->currentColor();
            break;
        }
        break;
    default:
        break;
    }

    q->setColorMap(color_map);
}


QWidget *dtkVisualizationDecoratorWithClutPrivate::inspectorItem(const QString& name) const
{
    for (QWidget *w : this->inspectors) {
        if (w->objectName() == name) {
            return w;
        }
    }
    return nullptr;
}

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorWithClut implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorWithClut::dtkVisualizationDecoratorWithClut(void): dtkVisualizationDecorator(), d(new dtkVisualizationDecoratorWithClutPrivate)
{
    d->q = this;

    //////////
    // Inspectors connections

    connect(d->show_actor_cb, &QCheckBox::stateChanged, [=,this] (int state)
    {
        this->saveSettings("visibility", state == Qt::Checked);
        this->setVisibility(state == Qt::Checked);
        this->draw();
    });

    connect(d->fields_box, &QComboBox::currentTextChanged, [=,this] (const QString& field_name)
    {
        this->setCurrentFieldName(field_name);
        this->saveSettings("field_name", field_name);
        this->touch();
        this->draw();
    });

    connect(d->field_components, QOverload<int>::of(&QComboBox::currentIndexChanged), [=,this] (int component_id)
    {
        d->current_field_component = component_id;
        this->saveSettings("field_component", component_id);
        this->saveSettings("original_range", d->original_range);
        this->touch();
        this->draw();
    });

    connect(d->min_range, &QLineEdit::editingFinished, [=,this] ()
    {
        d->original_range = false;
        this->setCurrentRange(d->min_range->text().toDouble(), d->max_range->text().toDouble());
        this->saveSettings("range_min", d->min_range->text().toDouble());
        this->saveSettings("range_max", d->max_range->text().toDouble());
        this->saveSettings("original_range", false);
        this->touch();
        this->draw();
    });
    connect(d->max_range, &QLineEdit::editingFinished, [=,this] ()
    {
        d->original_range = false;
        this->setCurrentRange(d->min_range->text().toDouble(), d->max_range->text().toDouble());
        this->saveSettings("range_min", d->min_range->text().toDouble());
        this->saveSettings("range_max", d->max_range->text().toDouble());
        this->saveSettings("original_range", false);
        this->touch();
        this->draw();
    });
     connect(d->reset_range, QOverload<>::of(&QPushButton::released), [=,this] ()
     {
        d->original_range = true;
        this->saveSettings("original_range", true);
        this->touch();
        this->draw();
        if (d->dynamic_range_button->checkState() != Qt::Checked) {
            d->original_range = false;
        }
    });
    // Changes modes between automatic and dynamic
    connect(d->dynamic_range_button, &QCheckBox::stateChanged, [=,this] (int state)
    {
        d->original_range = state;

        d->updateDisabledWidgets();

        this->saveSettings("original_range", d->original_range);
        this->touch();
        this->draw();
    });

    connect(d->colormap_editor, &dtkVisualizationWidgetsColorMapEditor::valueChanged, [=,this] (const QMap<double, QColor>& val)
    {
        d->color_transfer_functions[d->current_field_name] = d->colormap_editor->name();
        this->setColorMap(val);
        this->saveSettings("colormap", d->colormap_editor->name());
        this->draw();
    });

    connect(d->col_dialog, &dtkVisualizationWidgetsColorDialog::colorChanged, [=,this] (QColor c)
    {
        if (c.isValid()) {
            this->saveSettings("color", c);
            this->touchColorMap();
            this->draw();
        }
    });

    connect(d->show_scalar_bar, &QCheckBox::stateChanged, [=,this] (int state)
    {
        d->scalar_bar->SetVisibility(state == Qt::Checked);//TODO: fix
        this->saveSettings("show_scalarbar", state == Qt::Checked);
        this->draw();
    });

    QObject::connect(d_func()->opacity_slider, QOverload<int>::of(&QSlider::valueChanged), [=,this] (int v)
    {
        double val = 0.001 * v;
        this->saveSettings("opacity", val);
        d->opacity_val_sp->blockSignals(true);
        d->opacity_val_sp->setValue(val);
        d->opacity_val_sp->blockSignals(false);

        this->setOpacity(val);
        this->draw();
    });

    connect(d_func()->opacity_val_sp, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this] (double val)
    {
        this->saveSettings("opacity", val);
        d->opacity_slider->blockSignals(true);
        d->opacity_slider->setValue(std::round(val * 1000));
        d->opacity_slider->blockSignals(false);

        this->setOpacity(val);
        this->draw();
    });
}

void dtkVisualizationDecoratorWithClut::restoreSettings(void)
{
    QString name = this->objectName();
    if (name.isEmpty()) {
        return;
    }

    QSettings settings;
    settings.beginGroup("canvas");

    d->default_field_name = settings.value(name+"_field_name").toString();
    if(!d->eligible_field_names.contains(d->default_field_name)) {
        d->default_field_name = QString();
    }

    d->default_visibility = settings.value(name+"_visibility", true).toBool();
    d->default_show_scalar_bar = settings.value(name+"_show_scalarbar", false).toBool();

    d->default_field_component = settings.value(name+"_field_component", 1).toInt();
    d->field_components->blockSignals(true);
    d->field_components->setCurrentIndex(d->default_field_component);
    d->field_components->blockSignals(false);

    d->default_color_map  = settings.value(name+"_colormap", "jet").toString();

    double range_min   = settings.value(name+"_range_min", -1.).toDouble();
    double range_max   = settings.value(name+"_range_max", -1.).toDouble();
    bool original_range = settings.value(name+"_original_range", true).toBool();

    QColor color = QColor(settings.value(name+"_color", "#000000").toString());

    double opacity = settings.value(name+"_opacity", 1.).toDouble();

    settings.endGroup();

    this->setVisibility(d->default_visibility);

    if (!original_range && !d->default_field_name.isEmpty() && range_min != -1.0 && range_max != -1.0) {
        this->setCurrentRange(range_min, range_max);
    }
    d->original_range = original_range;
    d->dynamic_range_button->blockSignals(true);
    d->dynamic_range_button->setChecked(original_range);
    d->dynamic_range_button->blockSignals(false);
    d->updateDisabledWidgets();

    d->colormap_editor->blockSignals(true);
    d->colormap_editor->setValue(d->default_color_map);
    d->colormap_editor->blockSignals(false);

    d->col_dialog->blockSignals(true);
    d->col_dialog->setColor(color);
    d->col_dialog->blockSignals(false);

    this->touchColorMap();


    d->show_scalar_bar->blockSignals(true);
    d->show_scalar_bar->setChecked(d->default_show_scalar_bar);
    d->show_scalar_bar->blockSignals(false);
    if (d->default_visibility) {
        d->scalar_bar->SetVisibility(d->default_show_scalar_bar); //TODO: fix
    }

    d->show_actor_cb->blockSignals(true);
    d->show_actor_cb->setChecked(d_func()->default_visibility);
    d->show_actor_cb->blockSignals(false);

    d->opacity_val_sp->blockSignals(true);
    d->opacity_val_sp->setValue(opacity);
    d->opacity_val_sp->blockSignals(false);

    d->opacity_slider->blockSignals(true);
    d->opacity_slider->setValue(std::round(opacity * 1000));
    d->opacity_slider->blockSignals(false);

    this->setOpacity(opacity);
}

QString dtkVisualizationDecoratorWithClut::fieldName(void) const
{
    return d->current_field_name;
}

void dtkVisualizationDecoratorWithClut::touch(void)
{
    if (d->current_field_name.isEmpty() || !d->dataset) {
        return;
    }


    // Use Template method design pattern to customize in inherited classes the following steps
    this->touchRange();
    this->touchColorMap();
}

void dtkVisualizationDecoratorWithClut::touchRange(void)
{
    d->updateRange();
}

void dtkVisualizationDecoratorWithClut::touchColorMap(void)
{
    d->updateColorTransferFunction();
}

void dtkVisualizationDecoratorWithClut::setVisibility(bool visible)
{
    dtkVisualizationDecorator::setVisibility(visible);
    d->show_scalar_bar->setEnabled(visible);
    d->scalar_bar->SetVisibility(visible &&
                                 d->show_scalar_bar->isChecked() &&
                                 d->show_scalar_bar->isEnabled()); //TODO: fix
}

dtkVisualizationDecoratorWithClut::~dtkVisualizationDecoratorWithClut(void)
{
    d->clear();
    delete d;
    d = nullptr;
}

bool dtkVisualizationDecoratorWithClut::setCurrentFieldName(const QString& field_name)
{
    if (field_name.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "Field name is empty, nothing is done.";
        return false;
    }

    if (!d->dataset) {
        dtkWarn() << Q_FUNC_INFO << "Before calling setCurrentFieldName, setDataSet must be called.";
        return false;
    }

    if(!d->eligible_field_names.contains(field_name)) {
        dtkWarn() << Q_FUNC_INFO << "The field name :" << field_name << "that was specified doesn't match any of the eligible scalar field names";

        return false;
    }

    d->current_field_name = field_name;

    using Kind = dtkVisualizationDecoratorWithClutPrivate::Kind;
    dtkVisualizationWidgetsCategory *category = dynamic_cast<dtkVisualizationWidgetsCategory *>(d->inspectorItem("Colors"));
    Q_ASSERT_X(category, Q_FUNC_INFO, "Category Colors should exist.");
    switch (d->kinds[field_name]) {
    case Kind::Scalar:
        category->setVisible(d->field_components, false);
        break;
    case Kind::Vector:
        category->setVisible(d->field_components, true);
        break;
    default:
        category->setVisible(d->field_components, false);
        break;
    }

    d->fields_box->blockSignals(true);
    d->fields_box->setCurrentText(d->current_field_name);
    d->fields_box->blockSignals(false);

    d->scalar_bar->UnconstrainedFontSizeOn();
    const QString title_prefix =
        d->scalar_bar_label_includes_decorator_name
        ? (this->objectName()+"/")
        : "";
    d->scalar_bar->SetTitle(qPrintable(title_prefix+d->current_field_name));
    d->scalar_bar->Modified();

    return true;
}


void dtkVisualizationDecoratorWithClut::setScalarBarLabelIncludesDecoratorName(bool value)
{
    d->scalar_bar_label_includes_decorator_name = value;
}

void dtkVisualizationDecoratorWithClut::setScalarBarIsSeekBar(bool value)
{
    d->scalar_bar_is_seek_bar = value;
}

void dtkVisualizationDecoratorWithClut::setCurrentRange(double min, double max)
{
    if (d->current_field_name.isEmpty() && d->default_field_name.isEmpty()) {
        qWarning() << Q_FUNC_INFO << "Field not selected";
        return;
    }
    if (min >= max) {
        qWarning() << Q_FUNC_INFO << " min >= max : " << min << max;
        return;
    }
    QString field_name = d->current_field_name.isEmpty() ? d->default_field_name : d->current_field_name;
    auto&& range = d->ranges[field_name];
    range[0] = min;
    range[1] = max;
}

void dtkVisualizationDecoratorWithClut::setColorMap(const QMap<double, QColor>& new_colormap)
{
    QMap<double, QColor> colormap_map = new_colormap;

    // If there is no new colormap, take the previous one.
    if (new_colormap.isEmpty()) {
        QString colormap_name = d->color_transfer_functions[d->current_field_name];
        d->colormap_editor->blockSignals(true);
        d->colormap_editor->setValue(colormap_name);
        d->colormap_editor->blockSignals(false);
        colormap_map = d->colormap_editor->value();
    }

    // Recomputes the vtkColorMap based on range.
    auto&& range = d->ranges[d->current_field_name];

    d->color_function->RemoveAllPoints();
    for (auto it = colormap_map.cbegin(); it != colormap_map.cend(); ++it) {
        auto&& val = it.key();
        auto&& color = it.value();
        double node = val * range[1] + (1 - val) * range[0];
        d->color_function->AddRGBPoint(node,
                                       color.red()   / 255.,
                                       color.green() / 255.,
                                       color.blue()  / 255.);
    }
    d->color_function->ClampingOn();
    d->color_function->Modified();

    d->scalar_bar->SetLookupTable(d->color_function);
    d->scalar_bar->Modified();
}

void dtkVisualizationDecoratorWithClut::setOpacity(double)
{
    // Not implemented here
}

bool dtkVisualizationDecoratorWithClut::isDecorating(void)
{
    return d->eligible_field_names.size();
}

QVariant dtkVisualizationDecoratorWithClut::data(void) const
{
    if (d->dataset) {
        return dtk::variantFromValue(d->dataset);

    } else {
        return QVariant();
    }
}

dtkVisualizationCanvas *dtkVisualizationDecoratorWithClut::canvas(void) const
{
    return d->view;
}

QList<QWidget *> dtkVisualizationDecoratorWithClut::inspectors(void) const
{
    return d->inspectors;
}

dtkVisualizationDecoratorWithClutPrivate *dtkVisualizationDecoratorWithClut::d_func(void)
{
    return d;
}

const dtkVisualizationDecoratorWithClutPrivate *dtkVisualizationDecoratorWithClut::d_func(void) const
{
    return d;
}

void dtkVisualizationDecoratorWithClutPrivate::updateDisabledWidgets()
{
    min_range->setEnabled(original_range == false);
    max_range->setEnabled(original_range == false);

    if (original_range) {
        this->dynamic_range_button->setText("Auto");
        this->reset_range->setEnabled(false);
        this->reset_range->setText("");
        this->reset_range->setToolTip("not available in automatic mode");
    } else {
        this->dynamic_range_button->setText("Manual");
        this->reset_range->setEnabled(true);
        this->reset_range->setText("Fit data");
        this->reset_range->setToolTip("Update color range to actual data range (min/max)");
    }
}

//
// dtkVisualizationDecoratorWithClut.cpp ends here

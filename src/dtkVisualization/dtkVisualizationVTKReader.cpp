// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationVTKReader.h"

#include <QtCore>

#include <vtkDataSet.h>
#include <vtkDataSetReader.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkRectilinearGrid.h>
#include <vtkStructuredGrid.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLImageDataReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationVTKReader implementation
// ///////////////////////////////////////////////////////////////////

vtkDataSet *dtkVisualizationVTKReader(const QString& path)
{
    if (path.endsWith(".vti"))
        return dtkVisualizationVTKReaderVTI(path);

    if (path.endsWith(".vtp"))
        return dtkVisualizationVTKReaderVTP(path);

    if (path.endsWith(".vtr"))
        return dtkVisualizationVTKReaderVTR(path);

    if (path.endsWith(".vts"))
        return dtkVisualizationVTKReaderVTS(path);

    if (path.endsWith(".vtu"))
        return dtkVisualizationVTKReaderVTU(path);

    if (path.endsWith(".vtk"))
        return dtkVisualizationVTKReaderVTK(path);

    qWarning() << "file" << path << "is not handled by vtk reader";
    return nullptr;
}

vtkDataSet *dtkVisualizationVTKReaderVTI(const QString& path)
{
    vtkSmartPointer<vtkXMLImageDataReader> reader = vtkSmartPointer<vtkXMLImageDataReader>::New();
    reader->SetFileName(qPrintable(path));
    reader->Update();

    if (reader->GetOutput() == nullptr) {
        return nullptr;
    }
    reader->GetOutput()->SetReferenceCount(2);
    return reader->GetOutput();
}

vtkDataSet *dtkVisualizationVTKReaderVTP(const QString& path)
{
    vtkSmartPointer<vtkXMLPolyDataReader> reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
    reader->SetFileName(qPrintable(path));
    reader->Update();

    if (reader->GetOutput() == nullptr) {
        return nullptr;
    }
    reader->GetOutput()->SetReferenceCount(2);
    return reader->GetOutput();
}

vtkDataSet *dtkVisualizationVTKReaderVTR(const QString& path)
{
    vtkSmartPointer<vtkXMLRectilinearGridReader> reader = vtkSmartPointer<vtkXMLRectilinearGridReader>::New();
    reader->SetFileName(qPrintable(path));
    reader->Update();

    if (reader->GetOutput() == nullptr) {
        return nullptr;
    }
    reader->GetOutput()->SetReferenceCount(2);
    return reader->GetOutput();
}

vtkDataSet *dtkVisualizationVTKReaderVTS(const QString& path)
{
    vtkSmartPointer<vtkXMLStructuredGridReader> reader = vtkSmartPointer<vtkXMLStructuredGridReader>::New();
    reader->SetFileName(qPrintable(path));
    reader->Update();

    if (reader->GetOutput() == nullptr) {
        return nullptr;
    }
    reader->GetOutput()->SetReferenceCount(2);
    return reader->GetOutput();
}

vtkDataSet *dtkVisualizationVTKReaderVTU(const QString& path)
{
    vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
    reader->SetFileName(qPrintable(path));
    reader->Update();

    if (reader->GetOutput() == nullptr) {
        return nullptr;
    }
    reader->GetOutput()->SetReferenceCount(2);
    return reader->GetOutput();
}

vtkDataSet *dtkVisualizationVTKReaderVTK(const QString& path)
{
    vtkSmartPointer<vtkDataSetReader> reader = vtkSmartPointer<vtkDataSetReader>::New();
    reader->SetFileName(qPrintable(path));
    reader->Update();

    if  (reader->GetOutput() == nullptr) {
        return(nullptr);
    }
    reader->GetOutput()->SetReferenceCount(2);
    return reader->GetOutput();
}

//
// dtkVisualizationView3D.cpp ends here

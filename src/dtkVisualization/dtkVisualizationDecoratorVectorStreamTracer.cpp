// dtkVisualizationDecoratorVectorStreamTracer.cpp
//

#include "dtkVisualizationDecoratorVectorStreamTracer.h"

#include "dtkVisualizationDecoratorWithClut_p.h"
#include "dtkVisualizationMetaType.h"
#include "dtkVisualizationView2D.h"

#include <dtkVisualizationWidgets/dtkVisualizationWidgetsCategory>

#include <dtkLog>

#include <QtGui>
#include <QtWidgets>

#include <vtkActor.h>
#include <vtkBoundingBox.h>
#include <vtkCellCenters.h>
#include <vtkCellData.h>
#include <vtkCenterOfMass.h>
#include <vtkColorTransferFunction.h>
#include <vtkLineRepresentation.h>
#include <vtkLineSource.h>
#include <vtkLineWidget2.h>
#include <vtkLookupTable.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPointData.h>
#include <vtkPointSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkSphereRepresentation.h>
#include <vtkSphereWidget2.h>
#include <vtkStreamTracer.h>
#include <vtkTubeFilter.h>

// /////////////////////////////////////////////////////////////////
// dtkStreamTracerObserver
// /////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorStreamTracerSphereObserver : public vtkCommand
{
private:
    dtkVisualizationDecoratorStreamTracerSphereObserver(void) {
    };

public:
    static dtkVisualizationDecoratorStreamTracerSphereObserver *New(void)
    {
        return new dtkVisualizationDecoratorStreamTracerSphereObserver;
    };

    void Execute(vtkObject *caller, unsigned long event, void *) override
    {
        if (event == vtkCommand::InteractionEvent) {
            source_sphere->GetPolyData(source_data);
            stream_tracer->SetSourceData(source_data);
            stream_tracer->Modified();

            vtkSphereWidget2 *source_sphere_widget = reinterpret_cast<vtkSphereWidget2*>(caller);
            vtkSphereRepresentation *sphere_representation = vtkSphereRepresentation::SafeDownCast(source_sphere_widget->GetRepresentation());
            radius->setValue(sphere_representation->GetRadius());
            double pos[3];
            sphere_representation->GetCenter(pos);
            pos_x->setValue(pos[0]);
            pos_y->setValue(pos[1]);
            pos_z->setValue(pos[2]);
        }
    };

public:
    vtkSmartPointer<vtkPolyData> source_data;
    vtkSmartPointer<vtkSphereRepresentation> source_sphere;
    vtkSmartPointer<vtkStreamTracer> stream_tracer;
    dtkVisualizationWidgetsScalarControl *pos_x = nullptr;
    dtkVisualizationWidgetsScalarControl *pos_y = nullptr;
    dtkVisualizationWidgetsScalarControl *pos_z = nullptr;
    dtkVisualizationWidgetsScalarControl *radius = nullptr;
};

class dtkVisualizationDecoratorStreamTracerLineObserver : public vtkCommand
{
private:
    dtkVisualizationDecoratorStreamTracerLineObserver(void) {
    };

public:
    static dtkVisualizationDecoratorStreamTracerLineObserver *New(void)
    {
        return new dtkVisualizationDecoratorStreamTracerLineObserver;
    };

    void Execute(vtkObject *caller, unsigned long event, void *) override
    {
        if (event == vtkCommand::InteractionEvent) {
            source_line->GetPolyData(source_data);
            stream_tracer->SetSourceData(source_data);
            stream_tracer->Modified();

            vtkLineWidget2 *source_line_widget = reinterpret_cast<vtkLineWidget2*>(caller);
            vtkLineRepresentation *line_representation = vtkLineRepresentation::SafeDownCast(source_line_widget->GetLineRepresentation());
            double pos1[3];
            double pos2[3];
            line_representation->GetPoint1WorldPosition(pos1);
            line_representation->GetPoint2WorldPosition(pos2);
            pos1_x->setValue(pos1[0]);
            pos1_y->setValue(pos1[1]);
            pos1_z->setValue(pos1[2]);
            pos2_x->setValue(pos2[0]);
            pos2_y->setValue(pos2[1]);
            pos2_z->setValue(pos2[2]);
        }
    };

public:
    vtkSmartPointer<vtkPolyData> source_data;
    vtkSmartPointer<vtkLineRepresentation>   source_line;
    vtkSmartPointer<vtkStreamTracer> stream_tracer;
    dtkVisualizationWidgetsScalarControl *pos1_x = nullptr;
    dtkVisualizationWidgetsScalarControl *pos1_y = nullptr;
    dtkVisualizationWidgetsScalarControl *pos1_z = nullptr;
    dtkVisualizationWidgetsScalarControl *pos2_x = nullptr;
    dtkVisualizationWidgetsScalarControl *pos2_y = nullptr;
    dtkVisualizationWidgetsScalarControl *pos2_z = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorStreamTracerPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationDecoratorVectorStreamTracerPrivate
{
public:
    vtkSmartPointer<dtkVisualizationDecoratorStreamTracerSphereObserver> source_sphere_observer;
    vtkSmartPointer<dtkVisualizationDecoratorStreamTracerLineObserver> source_line_observer;
    vtkSmartPointer<vtkLineWidget2> source_line_widget;
    vtkSmartPointer<vtkSphereWidget2> source_sphere_widget;
    vtkSmartPointer<vtkPolyData> source_data;
    vtkSmartPointer<vtkLineRepresentation> source_line;
    vtkSmartPointer<vtkSphereRepresentation> source_sphere;

public:
    vtkSmartPointer<vtkStreamTracer> streamtracer;
    vtkSmartPointer<vtkTubeFilter> tube_filter;
    vtkSmartPointer<vtkPolyDataMapper> mapper;
    vtkSmartPointer<vtkActor> actor;

public:
    QCheckBox *cb_show_seed = nullptr;
    QComboBox *cb_seed_type = nullptr;

public:
    QComboBox *cb_integrator_direction = nullptr;
    QComboBox *cb_integrator_type = nullptr;
    QSpinBox *sp_integrator_max_steps = nullptr;
    QDoubleSpinBox *sp_integrator_max_lengths = nullptr;

public:
    QDoubleSpinBox *sp_radius = nullptr;
    QSpinBox *sp_resolution = nullptr;

public:
    QVector<dtkVisualizationWidgetsScalarControl*> sphere_seed_controls;
    QVector<dtkVisualizationWidgetsScalarControl*> line_seed_controls;

public:
    bool     default_show_seed = true;
    QString  default_seed_type = "Sphere";

public:
    void updateSeedControls(void);
};

// ///////////////////////////////////////////////////////////////////

void dtkVisualizationDecoratorVectorStreamTracerPrivate::updateSeedControls(void)
{
    const bool is_sphere = (cb_seed_type->currentText() == "Sphere");
    for (auto* sphere_control : sphere_seed_controls) {
        sphere_control->setVisible(is_sphere);
    }
    for (auto* line_control : line_seed_controls) {
        line_control->setVisible(!is_sphere);
    }
    if (is_sphere) {
        source_sphere_observer->Execute(source_sphere_widget, vtkCommand::InteractionEvent, nullptr);
    } else {
        source_line_observer->Execute(source_line_widget, vtkCommand::InteractionEvent, nullptr);
    }
}

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecoratorVectorStreamTracer implementation
// ///////////////////////////////////////////////////////////////////

dtkVisualizationDecoratorVectorStreamTracer::dtkVisualizationDecoratorVectorStreamTracer(void): dtkVisualizationDecoratorWithClut(), d(new dtkVisualizationDecoratorVectorStreamTracerPrivate())
{
    d->source_data = vtkSmartPointer<vtkPolyData>::New();

    d->source_line = vtkSmartPointer<vtkLineRepresentation>::New();
    d->source_line->SetResolution(50);
    d->source_line->GetPolyData(d->source_data);

    d->source_sphere = vtkSmartPointer<vtkSphereRepresentation>::New();
    d->source_sphere->SetPhiResolution(7);
    d->source_sphere->SetThetaResolution(7);
    d->source_sphere->SetRadius(1.0);
    d->source_sphere->GetPolyData(d->source_data);
    d->source_sphere->SetRepresentationToWireframe();
    d->source_sphere->RadialLineOff();

    d->source_sphere_widget = vtkSmartPointer<vtkSphereWidget2>::New();
    d->source_sphere_widget->SetEnabled(false);
    d->source_sphere_widget->SetRepresentation(d->source_sphere);

    d->source_line_widget = vtkSmartPointer<vtkLineWidget2>::New();
    d->source_line_widget->SetEnabled(false);
    d->source_line_widget->SetRepresentation(d->source_line);

    d->streamtracer = vtkSmartPointer<vtkStreamTracer>::New();
    d->streamtracer->SetSourceData(d->source_data);

    d->streamtracer->SetIntegrationDirectionToBoth();
    d->streamtracer->SetIntegratorTypeToRungeKutta4();
    d->streamtracer->SetIntegrationStepUnit(vtkStreamTracer::Units::CELL_LENGTH_UNIT);
    d->streamtracer->SetInitialIntegrationStep(0.2);
    d->streamtracer->SetMinimumIntegrationStep(0.01);
    d->streamtracer->SetMaximumIntegrationStep(0.5);

    d->streamtracer->SetMaximumNumberOfSteps(2000);
    d->streamtracer->SetMaximumPropagation(20); // To be set as the maximum size of the dataset
    d->streamtracer->SetTerminalSpeed(1.e-12);
    d->streamtracer->SetMaximumError(1.e-6);
    d->streamtracer->SetComputeVorticity(true);

    d->tube_filter = vtkSmartPointer<vtkTubeFilter>::New();
    d->tube_filter->SetInputConnection(d->streamtracer->GetOutputPort());
    d->tube_filter->SetNumberOfSides(8);
    d->tube_filter->SetRadius(.02);
    d->tube_filter->SetVaryRadius(0);

    d->mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    d->mapper->SetInputConnection(d->tube_filter->GetOutputPort());
    d->mapper->SetScalarModeToUsePointFieldData();

    d->actor = vtkSmartPointer<vtkActor>::New();
    d->actor->SetMapper(d->mapper);

    d->source_sphere_observer = vtkSmartPointer<dtkVisualizationDecoratorStreamTracerSphereObserver>::New();
    d->source_sphere_observer->source_data = d->source_data;
    d->source_sphere_observer->source_sphere = d->source_sphere;
    d->source_sphere_observer->stream_tracer = d->streamtracer;

    d->source_line_observer = vtkSmartPointer<dtkVisualizationDecoratorStreamTracerLineObserver>::New();
    d->source_line_observer->source_data = d->source_data;
    d->source_line_observer->source_line = d->source_line;
    d->source_line_observer->stream_tracer = d->streamtracer;

    d->source_line_widget->AddObserver(vtkCommand::InteractionEvent, d->source_line_observer);
    d->source_sphere_widget->AddObserver(vtkCommand::InteractionEvent, d->source_sphere_observer);

    //////////
    // Inspectors creation and setup

    d_func()->show_actor_cb->setObjectName("Streamlines");

    d->cb_show_seed = new QCheckBox;
    d->cb_show_seed->setObjectName("Display");
    d->cb_show_seed->setChecked(d->default_show_seed);

    d->cb_seed_type = new QComboBox;
    d->cb_seed_type->addItem("Line");
    d->cb_seed_type->addItem("Sphere");
    d->cb_seed_type->setCurrentIndex(1);
    d->cb_seed_type->setObjectName("Type");

    d->cb_integrator_direction = new QComboBox;
    d->cb_integrator_direction->addItem("Forward");
    d->cb_integrator_direction->addItem("Backward");
    d->cb_integrator_direction->addItem("Both");
    d->cb_integrator_direction->setCurrentIndex(2);
    d->cb_integrator_direction->setObjectName("Direction");

    d->cb_integrator_type = new QComboBox;
    d->cb_integrator_type->addItem("RK2");
    d->cb_integrator_type->addItem("RK4");
    d->cb_integrator_type->addItem("RK45");
    d->cb_integrator_type->setCurrentIndex(1);
    d->cb_integrator_type->setObjectName("Type");

    d->sp_integrator_max_steps = new QSpinBox;
    d->sp_integrator_max_steps->setMaximum(99999999);
    d->sp_integrator_max_steps->setValue(2000);
    d->sp_integrator_max_steps->setKeyboardTracking(false);
    d->sp_integrator_max_steps->setObjectName("Maximum Steps");

    d->sp_integrator_max_lengths = new QDoubleSpinBox;
    d->sp_integrator_max_lengths->setMaximum(999999999);
    d->sp_integrator_max_lengths->setValue(20);
    d->sp_integrator_max_lengths->setKeyboardTracking(false);
    d->sp_integrator_max_lengths->setObjectName("Max Stream Length");

    d->sp_radius = new QDoubleSpinBox;
    d->sp_radius->setMaximum(99999999);
    d->sp_radius->setValue(1.0);
    d->sp_radius->setSingleStep(0.1);
    d->sp_radius->setDecimals(4);
    d->sp_radius->setKeyboardTracking(false);
    d->sp_radius->setObjectName("Tube Radius");

    d->sp_resolution = new QSpinBox;
    d->sp_resolution->setMaximum(10000);
    d->sp_resolution->setValue(50);
    d->sp_resolution->setKeyboardTracking(false);
    d->sp_resolution->setObjectName("Resolution");

    d->sphere_seed_controls = {
        new dtkVisualizationWidgetsScalarControl("Center X"),
        new dtkVisualizationWidgetsScalarControl("Center Y"),
        new dtkVisualizationWidgetsScalarControl("Center Z"),
        new dtkVisualizationWidgetsScalarPositiveControl("Radius", 1.3),
    };
    d->source_sphere_observer->pos_x = d->sphere_seed_controls[0];
    d->source_sphere_observer->pos_y = d->sphere_seed_controls[1];
    d->source_sphere_observer->pos_z = d->sphere_seed_controls[2];
    d->source_sphere_observer->radius = d->sphere_seed_controls[3];

    d->line_seed_controls = {
        new dtkVisualizationWidgetsScalarControl("Position 1 X"),
        new dtkVisualizationWidgetsScalarControl("Position 1 Y"),
        new dtkVisualizationWidgetsScalarControl("Position 1 Z"),
        new dtkVisualizationWidgetsScalarControl("Position 2 X"),
        new dtkVisualizationWidgetsScalarControl("Position 2 Y"),
        new dtkVisualizationWidgetsScalarControl("Position 2 Z"),
    };
    d->source_line_observer->pos1_x = d->line_seed_controls[0];
    d->source_line_observer->pos1_y = d->line_seed_controls[1];
    d->source_line_observer->pos1_z = d->line_seed_controls[2];
    d->source_line_observer->pos2_x = d->line_seed_controls[3];
    d->source_line_observer->pos2_y = d->line_seed_controls[4];
    d->source_line_observer->pos2_z = d->line_seed_controls[5];

    //////////
    // Inspectors connections

    connect(d->cb_show_seed, &QCheckBox::stateChanged, [=,this] (int state)
    {
        QString text= d->cb_seed_type->currentText();
        d->default_show_seed = state == Qt::Checked;
        this->saveSettings("show_seed", d->default_show_seed);

        if (d->cb_seed_type->currentText() == "Sphere") {
            d->source_sphere_widget->SetEnabled(d->default_show_seed);
        } else {
            d->source_line_widget->SetEnabled(d->default_show_seed);
        }
        d->updateSeedControls();
        this->draw();
    });

    connect(d->cb_integrator_direction, QOverload<int>::of(&QComboBox::currentIndexChanged), [=,this] (int id)
    {
        d->streamtracer->SetIntegrationDirection(id);
        this->draw();
    });

    connect(d->cb_integrator_type, QOverload<int>::of(&QComboBox::currentIndexChanged), [=,this] (int id)
    {
        d->streamtracer->SetIntegratorType(id);
        this->draw();
    });

    connect(d->cb_seed_type, &QComboBox::currentTextChanged, [=,this] (const QString& text)
    {
        this->saveSettings("seed_type", d->cb_seed_type->currentText());
        if (d->cb_show_seed->checkState() == Qt::Checked) {
            if (text == "Sphere") {
                d->source_line_widget->Off();
                d->source_sphere_widget->On();
            } else {
                d->source_sphere_widget->Off();
                d->source_line_widget->On();
            }
        }
        d->updateSeedControls();
        this->touch();
        this->draw();
    });

    connect(d->sp_resolution, QOverload<int>::of(&QSpinBox::valueChanged), [=,this] (int val)
    {
        int sqrt_val = std::round(std::sqrt(val));
        d->source_sphere->SetPhiResolution(sqrt_val);
        d->source_sphere->SetThetaResolution(sqrt_val);
        d->source_line->SetResolution(val);
        this->touch();
        this->draw();
    });

    connect(d->sp_integrator_max_steps, QOverload<int>::of(&QSpinBox::valueChanged), [=,this] (int val)
    {
        d->streamtracer->SetMaximumNumberOfSteps(val);
        this->draw();
    });

    connect(d->sp_integrator_max_lengths, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this] (double val)
    {
        d->streamtracer->SetMaximumPropagation(val);
        this->draw();
    });

    connect(d->sp_radius, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this] (double val)
    {
        d->tube_filter->SetRadius(val);
        this->draw();
    });

    for (int i=0; i<6; ++i) {
        for (auto* seed_controls : { &d->sphere_seed_controls, &d->line_seed_controls }) {
            if (i >= seed_controls->size()) {
                continue;
            }
            dtkVisualizationWidgetsScalarControl *control = (*seed_controls)[i];
            const bool is_sphere = (seed_controls == &d->sphere_seed_controls);

            connect(control, QOverload<double>::of(&dtkVisualizationWidgetsScalarControl::valueChanged), [=,this] (double val)
            {
                vtkSmartPointer<vtkPolyData> data = vtkSmartPointer<vtkPolyData>::New();

                if (is_sphere) {
                    if (i == 3) {// radius
                        d->source_sphere->SetRadius(val);
                    } else {
                        double pos[3];
                        d->source_sphere->GetCenter(pos);
                        pos[i] = val;
                        d->source_sphere->SetCenter(pos);
                    }
                    d->source_sphere->GetPolyData(data);
                } else {
                    double pos[3];
                    const int axis = (i % 3);
                    const bool is_point1 = (i < 3);
                    if (is_point1) {
                        d->source_line->GetPoint1WorldPosition(pos);
                    } else {
                        d->source_line->GetPoint2WorldPosition(pos);
                    }
                    pos[axis] = val;
                    if (is_point1) {
                        d->source_line->SetPoint1WorldPosition(pos);
                    } else {
                        d->source_line->SetPoint2WorldPosition(pos);
                    }
                    d->source_line->GetPolyData(data);
                }

                d->streamtracer->SetSourceData(data);
                d->streamtracer->Modified();

                this->draw();
            });
        }
    }

    this->setObjectName("Vector Stream Tracer");

    QWidget *seed_box = new dtkVisualizationWidgetsCategory("Seed", {
            d->cb_show_seed,
            d->cb_seed_type,
            d->sphere_seed_controls[0],
            d->sphere_seed_controls[1],
            d->sphere_seed_controls[2],
            d->sphere_seed_controls[3],
            d->line_seed_controls[0],
            d->line_seed_controls[1],
            d->line_seed_controls[2],
            d->line_seed_controls[3],
            d->line_seed_controls[4],
            d->line_seed_controls[5]});

    QWidget *integrator_box = new dtkVisualizationWidgetsCategory("Integrator", {
            d->cb_integrator_direction,
            d->cb_integrator_type,
            d->sp_integrator_max_steps,
            d->sp_integrator_max_lengths});

    QWidget *styling_box = new dtkVisualizationWidgetsCategory("Styling", {
            d->sp_radius,
            d->sp_resolution,
            d_func()->opacity_widget
        });

    d_func()->inspectors << seed_box
                         << integrator_box
                         << styling_box;
}

dtkVisualizationDecoratorVectorStreamTracer::~dtkVisualizationDecoratorVectorStreamTracer(void)
{
    dtkVisualizationDecoratorVectorStreamTracer::unsetCanvas();

    delete d;

    d = nullptr;
}

void dtkVisualizationDecoratorVectorStreamTracer::restoreSettings(void)
{
    QString name = this->objectName();
    if (name.isEmpty())
        return;

    dtkVisualizationDecoratorWithClut::restoreSettings();

    QSettings settings;
    settings.beginGroup("canvas");
    d->default_show_seed = settings.value(name+"_show_seed", true).toBool();
    d->default_seed_type = settings.value(name+"_seed_type", "Sphere").toString();
    settings.endGroup();
    d->cb_show_seed->blockSignals(true);
    d->cb_show_seed->setChecked(d->default_show_seed);
    d->cb_show_seed->blockSignals(false);
    d->cb_seed_type->blockSignals(true);
    d->cb_seed_type->setCurrentText(d->default_seed_type);
    d->cb_seed_type->blockSignals(false);

}

void dtkVisualizationDecoratorVectorStreamTracer::setData(const QVariant& data)
{
    vtkDataSet *dataset = data.value<vtkDataSet *>();

    if (!dataset) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet is expected. Input data is not stored.";
        return;
    }

    d_func()->clear();
    d_func()->retrieveVectorPoints(dataset);
    d_func()->retrieveVectorCells(dataset);

    if (!this->isDecorating()) {
        dtkWarn() << Q_FUNC_INFO << "vtkDataSet has no field to decorate. Nothing is done.";
        d_func()->clear();
        return;
    }

    d_func()->dataset = dataset;
    this->restoreSettings();

    double bounds[6];
    dataset->GetBounds(bounds);
    vtkBoundingBox box;
    box.SetBounds(bounds);
    double center[3]; box.GetCenter(center);
    double pos1[3]; pos1[0] = bounds[0]; pos1[1] = bounds[2]; pos1[2] = bounds[4];
    double pos2[3]; pos2[0] = bounds[1]; pos2[1] = bounds[3]; pos2[2] = bounds[5];

    d->sp_integrator_max_lengths->blockSignals(true);
    d->sp_integrator_max_lengths->setValue(box.GetDiagonalLength());
    d->streamtracer->SetMaximumPropagation(box.GetDiagonalLength());
    d->sp_integrator_max_lengths->blockSignals(false);

    d->sp_radius->blockSignals(true);
    d->sp_radius->setValue(box.GetDiagonalLength()/500.0);
    d->tube_filter->SetRadius(box.GetDiagonalLength()/500.0);
    d->sp_radius->blockSignals(false);

    d->source_sphere->SetCenter(center);
    d->source_sphere->SetRadius(box.GetDiagonalLength()/20.0);
    d->source_sphere->Modified();

    d->source_line->SetPoint1WorldPosition(pos1);
    d->source_line->SetPoint2WorldPosition(pos2);

    d->source_line->Modified();

    d->streamtracer->SetInputData(d->source_data);
    d->streamtracer->Modified();

    d_func()->sortEligibleFields();
    this->setCurrentFieldName(d_func()->current_field_name);

    if (this->canvas()) {
        this->canvas()->renderer()->AddActor(d->actor);
        d->source_sphere_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());
        d->source_line_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());

        if (d->default_show_seed) {
            if (d->cb_seed_type->currentText() == "Sphere") {
                d->source_line_widget->Off();
                d->source_sphere_widget->On();
            } else {
                d->source_sphere_widget->Off();
                d->source_line_widget->On();
            }
        } else {
            d->source_line_widget->Off();
            d->source_sphere_widget->Off();
        }
    }

    d_func()->enableScalarBar();

    d->updateSeedControls();

    d->sphere_seed_controls[0]->setResetValue(center[0]);
    d->sphere_seed_controls[1]->setResetValue(center[1]);
    d->sphere_seed_controls[2]->setResetValue(center[2]);
    d->sphere_seed_controls[3]->setResetValue(d->source_sphere->GetRadius());
    d->line_seed_controls[0]->setResetValue(pos1[0]);
    d->line_seed_controls[1]->setResetValue(pos1[1]);
    d->line_seed_controls[2]->setResetValue(pos1[2]);
    d->line_seed_controls[3]->setResetValue(pos2[0]);
    d->line_seed_controls[4]->setResetValue(pos2[1]);
    d->line_seed_controls[5]->setResetValue(pos2[2]);
}

void dtkVisualizationDecoratorVectorStreamTracer::setCanvas(dtkVisualizationCanvas *canvas)
{
    this->unsetCanvas();

    d_func()->view = dynamic_cast<dtkVisualizationView2D *>(canvas);
    if (!d_func()->view) {
        qWarning() << Q_FUNC_INFO << "View 2D or view 3D expected as canvas. Canvas is reset to nullptr.";
        return;
    }

    if (this->isDecorating()) {
        d_func()->view->renderer()->AddActor(d->actor);
        d->source_line_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());
        d->source_sphere_widget->SetInteractor(this->canvas()->renderer()->GetRenderWindow()->GetInteractor());
        if (!d_func()->default_visibility) {
            d_func()->default_visibility = true;
        } else {
            if (d->default_show_seed) {
                if (d->cb_seed_type->currentText() == "Sphere") {
                    d->source_line_widget->Off();
                    d->source_sphere_widget->On();
                } else {
                    d->source_line_widget->On();
                    d->source_sphere_widget->Off();
                }
            } else {
                d->source_line_widget->Off();
                d->source_sphere_widget->Off();
            }
        }
    }

    d_func()->enableScalarBar();
}

void dtkVisualizationDecoratorVectorStreamTracer::unsetCanvas(void)
{
    if (d_func()->view) {
        d->source_line_widget->Off();
        d->source_line_widget->SetInteractor(nullptr);
        d->source_sphere_widget->Off();
        d->source_sphere_widget->SetInteractor(nullptr);
        d_func()->view->renderer()->RemoveActor(d->actor);
    }

    d_func()->disableScalarBar();

    d_func()->view = nullptr;
}

void dtkVisualizationDecoratorVectorStreamTracer::touch(void)
{
    dtkVisualizationDecoratorWithClut::touch();

    if (d->cb_seed_type->currentText() == "Sphere") {
        d->source_sphere->GetPolyData(d->source_data);
    } else {
        d->source_line->GetPolyData(d->source_data);
    }
    d->streamtracer->SetSourceData(d->source_data);
    d->streamtracer->Modified();
}

void dtkVisualizationDecoratorVectorStreamTracer::setVisibility(bool visible)
{
    dtkVisualizationDecoratorWithClut::setVisibility(visible);
    d->actor->SetVisibility(visible);
    if (d->cb_show_seed->isChecked() && this->canvas()) {
        if (d->cb_seed_type->currentText() == "Sphere") {
            d->source_sphere_widget->SetEnabled(visible);
        } else {
            d->source_line_widget->SetEnabled(visible);
        }
    }
}

void dtkVisualizationDecoratorVectorStreamTracer::setOpacity(double opacity)
{
    d->actor->GetProperty()->SetOpacity(opacity);
    d->actor->Modified();
}

bool dtkVisualizationDecoratorVectorStreamTracer::setCurrentFieldName(const QString& field_name)
{
    if (field_name.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "Field name is empty, nothing is done.";
        return false;
    }

    if (!d_func()->dataset) {
        dtkWarn() << Q_FUNC_INFO << "Before calling setCurrentFieldName, setDataSet must be called.";
        return false;
    }

    if(!d_func()->eligible_field_names.contains(field_name)) {
        dtkWarn() << Q_FUNC_INFO << "The field name :" << field_name << "that was specified doesn't match any of the eligible scalar field names";
        return false;
    }

    d_func()->current_field_name = field_name;

    using Support = dtkVisualizationDecoratorWithClut::Support;
    int support = d_func()->supports[field_name];
    if(support == Support::Point) {
        d->streamtracer->SetInputData(d_func()->dataset);
        d->streamtracer->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, qPrintable(field_name));

    } else if(support == Support::Cell) {
        d->streamtracer->SetInputData(d_func()->dataset);
        d->streamtracer->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, qPrintable(field_name));
    }
    d->streamtracer->Modified();

    return dtkVisualizationDecoratorWithClut::setCurrentFieldName(field_name);
}

void dtkVisualizationDecoratorVectorStreamTracer::setColorMap(const QMap<double, QColor>& new_colormap)
{
    dtkVisualizationDecoratorWithClut::setColorMap(new_colormap);

    d->mapper->SetLookupTable(d_func()->color_function);
    d->mapper->SelectColorArray(qPrintable(d_func()->current_field_name));
    auto&& range = d_func()->ranges[d_func()->current_field_name];
    d->mapper->SetScalarRange(range[0], range[1]);
    d->mapper->Modified();
}

//
// dtkVisualizationDecoratorVectorStreamTracer.cpp ends here

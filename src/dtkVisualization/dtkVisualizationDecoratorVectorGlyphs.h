// dtkVisualizationDecoratorVectorGlyphs.h
//

#pragma once

#include <dtkVisualizationExport>

#include "dtkVisualizationDecoratorWithClut.h"

class DTKVISUALIZATION_EXPORT dtkVisualizationDecoratorVectorGlyphs : public dtkVisualizationDecoratorWithClut
{
    Q_OBJECT

public:
    dtkVisualizationDecoratorVectorGlyphs(void);
    ~dtkVisualizationDecoratorVectorGlyphs(void);

public:
    void setData(const QVariant&) override;
    void setCanvas(dtkVisualizationCanvas *) override;
    void unsetCanvas(void) override;
    void touch(void) override;
    void setAutoAdjustGlyphSize(bool value);
    void adjustGlyphSizeToDataSet();

protected:
    void restoreSettings(void) override;
    bool setCurrentFieldName(const QString&) override;
    void setColorMap(const QMap<double, QColor>&) override;
    void setOpacity(double) override;

public:
    void setVisibility(bool visible) override;

protected:
    bool isCurrentFieldUniform(void);

protected:
    class dtkVisualizationDecoratorVectorGlyphsPrivate *d = nullptr;
};

//
// dtkVisualizationDecoratorVectorGlyphs.h ends here

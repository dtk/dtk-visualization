// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVisualizationExport>

#include <dtkCore>

#include <QtCore/QObject>

class vtkAlgorithmOutput;
class dtkVisualizationCanvas;
class vtkTransform;
class QWidget;

class dtkVisualizationDecoratorPrivate;

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationDecorator declaration
// ///////////////////////////////////////////////////////////////////

/**
 *  # Inputs of the decorator
 *  ## Stand-alone decorator
 *  The instances use two inputs:
 *  - the data to decorate. \sa dtkVisualisationDecorator::setData ;
 *  - the internal parameters of how to decorate.
 *  ## Linked decorators
 *  Allow a decorator to get the data from the processing of another one.
 *  As an example: decorate the outputs of dtkVisualizationDecoratorClipper, so
 *  that the linked decorators are only decorating the non-clipped part of the scene.
 *  \sa dtkVisualisationDecorator::setInputConnection
 *  \sa dtkVisualisationDecorator::outputPort
 *
 */
class DTKVISUALIZATION_EXPORT dtkVisualizationDecorator : public QObject
{
    Q_OBJECT

public:
    using Connection = vtkAlgorithmOutput;

public:
     dtkVisualizationDecorator(void);
    ~dtkVisualizationDecorator(void);

public:
    virtual void touch(void) = 0;
    void setObjectName(const QString &);

public:
    void show(void);
    void hide(void);

protected:
    void draw(void);
    void saveSettings(const QString &, const QVariant&);

protected:
    virtual void restoreSettings(void) {};

public:
    virtual bool isVisible();
    /**
     * Makes the decorator visible or not.
     * @param visible Whether the decorator has to be visible.
     */
    virtual void setVisibility(bool visible);

    virtual bool isDecorating(void) = 0;

    virtual void setData(const QVariant&) = 0;

    virtual void setTransform(vtkTransform *);

    virtual void setCanvas(dtkVisualizationCanvas *) = 0;
    virtual void unsetCanvas(void) = 0;

    virtual QVariant data(void) const = 0;
    virtual dtkVisualizationCanvas *canvas(void) const = 0;

    virtual QList<QWidget *> inspectors(void) const = 0;

public:
    virtual void setInputConnection(Connection *);
    virtual Connection *outputPort(void);

private:
    dtkVisualizationDecoratorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// Give the concept the plugin machinery
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkVisualizationDecorator *)
DTK_DECLARE_PLUGIN        (dtkVisualizationDecorator, DTKVISUALIZATION_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkVisualizationDecorator, DTKVISUALIZATION_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkVisualizationDecorator, DTKVISUALIZATION_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkVisualization layer
// /////////////////////////////////////////////////////////////////

namespace dtk { namespace visualization {
    DTK_DECLARE_CONCEPT(dtkVisualizationDecorator, DTKVISUALIZATION_EXPORT, decorator);
} }

//
// dtkVisualizationDecorator.h ends here

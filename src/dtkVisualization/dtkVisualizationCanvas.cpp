// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationCanvas.h"

#include <dtkThemes>
#include <dtkWidgets>

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <QVTKOpenGLNativeWidget.h>

#include <vtkCamera.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarRepresentation.h>
#include <vtkScalarBarWidget.h>
#include <vtkSmartPointer.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTransform.h>
#include <vtkProperty2D.h>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationCanvasPrivate
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationCanvasPrivate : public QVTKOpenGLNativeWidget
{
    Q_OBJECT

public:
     dtkVisualizationCanvasPrivate(QWidget *parent = nullptr);
    ~dtkVisualizationCanvasPrivate(void);

public:
    QSize sizeHint(void) const;

protected:
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);

public slots:
    void enableInteractor(void);
    void disableInteractor(void);

public slots:
    void onFocus(void);

public:
    vtkSmartPointer<vtkGenericOpenGLRenderWindow> window;
    vtkSmartPointer<vtkRenderer> renderer;

public:
    dtkVisualizationCanvas *q = nullptr;

public:
    dtkWidgetsHUD *hud = nullptr;

public:
    QString title;
    QColor bg;
    vtkSmartPointer<vtkTextActor> textActor;

public:
    std::size_t nb_scalar_bars = 0;
};

///////////////////////////////////////////////////////////////////

dtkVisualizationCanvasPrivate::dtkVisualizationCanvasPrivate(QWidget *parent) : QVTKOpenGLNativeWidget(parent)
{
    this->setFormat(QVTKOpenGLNativeWidget::defaultFormat());

    this->renderer = vtkSmartPointer<vtkRenderer>::New();
    this->renderer->SetBackground(0.290, 0.295, 0.300);

    this->window = vtkGenericOpenGLRenderWindow::New();
    this->window->AddRenderer(this->renderer);

    this->setRenderWindow(this->window);
    this->setEnableHiDPI(true);

    this->hud = new dtkWidgetsHUD(parent);
    this->bg = QColor(0.3,0.3,0.3);
    this->setAcceptDrops(true);
}

dtkVisualizationCanvasPrivate::~dtkVisualizationCanvasPrivate()
{
}

void dtkVisualizationCanvasPrivate::enableInteractor(void)
{
    this->interactor()->Enable();
}

void dtkVisualizationCanvasPrivate::disableInteractor(void)
{
    this->interactor()->Disable();
}

QSize dtkVisualizationCanvasPrivate::sizeHint(void) const
{
    return QSize(800, 600);
}

void dtkVisualizationCanvasPrivate::resizeEvent(QResizeEvent *event)
{
    if (!this->title.isEmpty()) {
        QFontMetrics metrics(qApp->font());
        int pos_x = this->size().width() / 2 - metrics.horizontalAdvance(title)/2;
        this->textActor->SetPosition ( pos_x, 10 );
        this->textActor->Modified();
    }
    QVTKOpenGLNativeWidget::resizeEvent(event);
    this->hud->resize(event->size());
}

void dtkVisualizationCanvasPrivate::mousePressEvent(QMouseEvent *event)
{
    q->emit focused();

    QVTKOpenGLNativeWidget::mousePressEvent(event);
}

void dtkVisualizationCanvasPrivate::onFocus(void)
{

}

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationCanvas
// ///////////////////////////////////////////////////////////////////

dtkVisualizationCanvas::dtkVisualizationCanvas(QWidget *parent) : dtkWidgetsWidget(parent), d(new dtkVisualizationCanvasPrivate(this))
{
    d->q = this;

    static int count = 1;
    this->setObjectName(QString("Canvas - %1").arg(count++));

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(d);

    connect(this, &dtkVisualizationCanvas::focused, d, &dtkVisualizationCanvasPrivate::onFocus);
}

dtkVisualizationCanvas::~dtkVisualizationCanvas(void)
{
    delete d;
}

QWidget *dtkVisualizationCanvas::widget(void)
{
    return d;
}

void dtkVisualizationCanvas::setBackground(QColor color)
{
    d->bg = color;
    d->renderer->SetBackground( color.redF(), color.greenF(), color.blueF());
    if (!d->title.isEmpty()) {
        auto brightness = d->bg.redF() * 0.299 + d->bg.greenF() * 0.587 + d->bg.blueF() * 0.114;
        QColor fg;
        fg = (brightness > 0.6) ? Qt::black : Qt::white;
        d->textActor->GetTextProperty()->SetColor ( 1.0-fg.redF(), 1.0-fg.greenF(), 1.0-fg.blueF());
        d->textActor->GetTextProperty()->SetBackgroundColor ( 1.0-d->bg.redF(), 1.0-d->bg.greenF(), 1.0-d->bg.blueF());
    }
    this->draw();
}

void dtkVisualizationCanvas::setTitle(const QString & title)
{
    if (title.isEmpty()) {
        if (d->textActor)
            d->renderer->RemoveActor ( d->textActor );
        d->title = "";
        return;
    }
    auto brightness = d->bg.redF() * 0.299 + d->bg.greenF() * 0.587 + d->bg.blueF() * 0.114;
    QColor fg;
    fg = (brightness > 0.6) ? Qt::black : Qt::white;

    // Setup the text and add it to the renderer
    if (!d->textActor)
        d->textActor = vtkSmartPointer<vtkTextActor>::New();

    d->textActor->SetInput ( qPrintable(title) );
    d->textActor->GetTextProperty()->SetFontSize ( 12 );
    d->textActor->GetTextProperty()->SetColor ( 1.0-fg.redF(), 1.0-fg.greenF(), 1.0-fg.blueF());
    d->textActor->GetTextProperty()->SetBackgroundColor ( 1.0-d->bg.redF(), 1.0-d->bg.greenF(), 1.0-d->bg.blueF());
    d->textActor->GetTextProperty()->SetBackgroundOpacity ( 0.6 );
    d->renderer->AddActor2D ( d->textActor );

    d->title = title;
}

void dtkVisualizationCanvas::link(dtkVisualizationCanvas *other)
{
    d->renderer->SetActiveCamera(other->d->renderer->GetActiveCamera());

    other->d->window->AddObserver(vtkCommand::RenderEvent, this, &dtkVisualizationCanvas::update);

    d->hud->addInfo("Linked");

    this->update();
}

void dtkVisualizationCanvas::unlink(void)
{
    vtkSmartPointer<vtkCamera> camera = vtkCamera::New();
    camera->ShallowCopy(d->renderer->GetActiveCamera());

    d->renderer->SetActiveCamera(camera);

    d->hud->addInfo("Unlinked");

    this->update();
}

dtkWidgetsHUD *dtkVisualizationCanvas::hud(void)
{
    return d->hud;
}

void dtkVisualizationCanvas::reset(void)
{
    if (d->renderer) {
        d->renderer->ResetCamera();
    }
}

void dtkVisualizationCanvas::draw(void)
{
    if (d->interactor()) {
        d->interactor()->Render();
    }

    // if (d->renderer) {
    //     d->renderer->GetRenderWindow()->Render();
    // }
}

vtkRenderer *dtkVisualizationCanvas::renderer(void)
{
    return d->renderer;
}

vtkRenderWindowInteractor *dtkVisualizationCanvas::interactor(void)
{
    return d->interactor();
}

void dtkVisualizationCanvas::addScalarBar(vtkScalarBarWidget *scalar_bar_widget, bool scalar_bar_is_seek_bar)
{
    vtkScalarBarActor* scalar_bar = scalar_bar_widget->GetScalarBarActor();

    if (this->renderer() && this->renderer()->GetViewProps()->IsItemPresent(scalar_bar)) {
        if (!scalar_bar_widget->GetEnabled()) {
            scalar_bar_widget->On();
        }
        return;
    }

    if (scalar_bar_widget->GetInteractor() == this->interactor()) {
        // scalar bar already added to this interactor, skip
        if (!scalar_bar_widget->GetEnabled()) {
            scalar_bar_widget->On();
        }
        return;
    }

    if (this->renderer()) {
        //this->renderer()->AddActor2D(scalar_bar);
        this->renderer()->Render();
    }

    /********************/

        ++d->nb_scalar_bars;

        int display_level = (d->nb_scalar_bars - 1) / 4;
        int side_slot = (d->nb_scalar_bars - 1) % 4; // display slots are ordered as follows:
        /*
        _______________________________________
        |              111111                 |  scalar bar positioning-corner is marked with X:
        |              555555                 |  X_____________________
        |                                     |   |____horizontal______|       ___
        |2 6                             4  0 |                               | v |
        |2 6                             4  0 |                               | e |
        |2 6                             4  0 |                               | r |
        |2 6                             4  0 |                               | t |
        |                                     |                               | i |
        |                                     |                               | c |
        |                                     |                               | a |
        |______________333333_________________|                               | l |
                                                                              |___|
                                                                             X
        */
        int is_vertical = (side_slot == 0) || (side_slot == 2);
        int is_left = (side_slot == 1) || (side_slot == 2);

        scalar_bar->SetOrientation(is_vertical);
        scalar_bar->SetBarRatio(0.45);

        scalar_bar_widget->SetInteractor(this->interactor());
        vtkScalarBarRepresentation *r = scalar_bar_widget->GetScalarBarRepresentation();

        double length = 0.25;
        double thickness = 0.05;
        double xmin = 0.02;
        double ymin = (scalar_bar_is_seek_bar ? 0.10 : 0.02); //(xmin, ymin) is the bottom-left corner, because the origin is at the bottom-left corner.
        double xmax = 0.98;
        double ymax = 0.90; //(xmax, ymax) is the top-right corner, because the origin is at the bottom-left corner.

        double x = xmin;
        double y = ymin;

        switch(side_slot)
        {
            case 0:
                x = xmax - thickness;
                y = (ymax - ymin - length)/2;
            break;
            case 1:
                x = (xmax - xmin - length)/2;
                y = ymax;
            break;
            case 2:
                x = xmin;
                y = (ymax - ymin - length)/2;
            break;
            case 3:
                x = (xmax - xmin - length)/2;
                y = ymin;
            break;
            default:
            break;
        }

        // if more than 4 bars are displayed, they are shown by default in concentric circles, piling up.
        double space_x =  pow(-1, is_left) * display_level * thickness * double(is_vertical);
        double space_y =  pow(-1, is_left + 1) * display_level * thickness * double(!is_vertical);
        x -= space_x;
        y -= space_y;

        double start_pos[2] = {0, 0};
        start_pos[0] = x;
        start_pos[1] = y;
        double size[2] = {0, 0};
        size[0] = length * !is_vertical + thickness * is_vertical;
        size[1] = length * is_vertical + thickness * !is_vertical;
        // qDebug("pos: (%f %f), (%f %f) - display_level = %d", start_pos[0], start_pos[1], size[0], size[1], display_level);

        r->SetPosition(start_pos);
        r->SetPosition2(size); // this is SetSize. It's badly named. But it sets the representation size.
        r->SetProportionalResize(false);
        r->SetUseBounds(true);
        r->SetShowBorderToActive();
        r->SetOrientation(is_vertical);


        scalar_bar_widget->On();
}

void dtkVisualizationCanvas::scale(double x, double y, double z)
{
    if (x >= 0.01 && y >=0.01 && z >= 0.01)  {
        vtkCamera *camera = d->renderer->GetActiveCamera();
        vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
        transform->Scale(x, y, z);
        camera->SetModelTransformMatrix(transform->GetMatrix());
        d->renderer->ResetCamera();
        this->draw();
    }
}

void dtkVisualizationCanvas::removeScalarBar(vtkScalarBarWidget *scalar_bar_widget)
{
    if(this->renderer()) {
        //this->renderer()->RemoveActor2D(scalar_bar);
        scalar_bar_widget->Off();
        --d->nb_scalar_bars;
        this->renderer()->Render();
    }
}

// ///////////////////////////////////////////////////////////////////

#include "dtkVisualizationCanvas.moc"

//
// dtkVisualizationPlot2D.cpp ends here

// dtkVisualizationDecoratorVectorStreamTracer.h
//

#pragma once

#include <dtkVisualizationExport>

#include "dtkVisualizationDecoratorWithClut.h"

class DTKVISUALIZATION_EXPORT dtkVisualizationDecoratorVectorStreamTracer : public dtkVisualizationDecoratorWithClut
{
    Q_OBJECT

public:
     dtkVisualizationDecoratorVectorStreamTracer(void);
    ~dtkVisualizationDecoratorVectorStreamTracer(void);

public:
    void setData(const QVariant&) override;
    void setCanvas(dtkVisualizationCanvas *) override;
    void unsetCanvas(void) override;
    void touch(void) override;

protected:
    void restoreSettings(void) override;
    bool setCurrentFieldName(const QString&) override;
    void setColorMap(const QMap<double, QColor>&) override;
    void setOpacity(double) override;

public:
    void setVisibility(bool visible) override;

protected:
    bool isCurrentFieldUniform(void);

protected:
    class dtkVisualizationDecoratorVectorStreamTracerPrivate *d = nullptr;
};

//
// dtkVisualizationDecoratorVectorStreamTracer.h ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationTypeTraits.h"

#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkRectilinearGrid.h>
#include <vtkStructuredGrid.h>
#include <vtkUnstructuredGrid.h>

#include <QtCore>

#include <dtkCore/dtkCoreMetaType>

Q_DECLARE_METATYPE(vtkDataSet *);
Q_DECLARE_METATYPE(vtkImageData *);
Q_DECLARE_METATYPE(vtkPoints *);
Q_DECLARE_METATYPE(vtkPointSet *);
Q_DECLARE_METATYPE(vtkPolyData *);
Q_DECLARE_METATYPE(vtkRectilinearGrid *);
Q_DECLARE_METATYPE(vtkStructuredGrid *);
Q_DECLARE_METATYPE(vtkUnstructuredGrid *);

namespace dtk {

    namespace detail
    {
        // Custom fallback to create QVariant from vtkDataSet data
        template <typename T>
        struct variant_handler<T, std::enable_if_t<dtk::is_vtkdataset<T>::value>>
        {
            static QVariant fromValue(const T& t) {
                QString class_name(t->GetClassName());
                auto mt = QMetaType::fromName(qPrintable(class_name + "*"));

                if (!mt.isValid()) {
                    return QVariant::fromValue(t);
                }
                return QVariant(mt, &t);
            }
        };
    }
}

//
// dtkVisualizationMetaType.tpp ends here

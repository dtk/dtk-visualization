// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVisualizationFactoryExport>

#include <vtkActor.h>
#include <vtkObject.h>
#include <vtkObjectFactory.h>

class DTKVISUALIZATIONFACTORY_EXPORT dtkVisualizationActor : public vtkObject
{
public:
     dtkVisualizationActor(void);
    ~dtkVisualizationActor(void);

public:
    vtkTypeMacro(dtkVisualizationActor, vtkActor);

public:
    static dtkVisualizationActor* New(void);

public:
    void SetName(std::string name);

public:
    std::string Name(void);

private:
    dtkVisualizationActor(const dtkVisualizationActor&) = delete;
    dtkVisualizationActor& operator=(const dtkVisualizationActor&) = delete;

private:
    class dtkVisualizationActorPrivate *d;
};

VTK_CREATE_CREATE_FUNCTION(dtkVisualizationActor);

//
// dtkVisualizationActor.h ends here

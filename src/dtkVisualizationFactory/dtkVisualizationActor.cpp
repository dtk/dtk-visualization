// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationActor.h"

#include <string>

class dtkVisualizationActorPrivate
{
public:
    std::string name;
};

dtkVisualizationActor *dtkVisualizationActor::New(void)
{
    VTK_STANDARD_NEW_BODY(dtkVisualizationActor);
}

void dtkVisualizationActor::SetName(std::string name)
{
    d->name = name;
}

std::string dtkVisualizationActor::Name(void)
{
    return d->name;
}

dtkVisualizationActor::dtkVisualizationActor(void)
{
    d = new dtkVisualizationActorPrivate;
}

dtkVisualizationActor::~dtkVisualizationActor(void)
{
    delete d;
}

//
// dtkVisualizationActorBase.cpp ends here

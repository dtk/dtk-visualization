// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationFactory.h"

#include "dtkVisualizationActor.h"
#include <vtkVersion.h>

dtkVisualizationFactory *dtkVisualizationFactory::New(void)
{
    dtkVisualizationFactory *factory = new dtkVisualizationFactory;
    factory->InitializeObjectBase();

    return factory;
}

const char *dtkVisualizationFactory::GetDescription(void)
{
    return "dtk visualization factory";
}

const char *dtkVisualizationFactory::GetVTKSourceVersion(void)
{
    return VTK_VERSION;
}

dtkVisualizationFactory::dtkVisualizationFactory(void)
{
    this->RegisterOverride("vtkActor", "dtkVisualizationActor", "", 1, vtkObjectFactoryCreatedtkVisualizationActor);
}

//
// dtkVisualizationFactory.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVisualizationFactoryExport>

#include <vtkObjectFactory.h>

class DTKVISUALIZATIONFACTORY_EXPORT dtkVisualizationFactory : public vtkObjectFactory
{
public:
    static dtkVisualizationFactory *New(void);

public:
    const char *GetDescription(void) override;
    const char *GetVTKSourceVersion(void) override;

protected:
   dtkVisualizationFactory(const dtkVisualizationFactory&) = delete;

protected:
   dtkVisualizationFactory& operator=(const dtkVisualizationFactory&) = delete;

private:
    dtkVisualizationFactory(void);
};

//
// dtkVisualizationFactory.h ends here

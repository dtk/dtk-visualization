#include "dtkVisualizationWidgetsCategory.h"

#include <dtkFonts/dtkFontAwesome>

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
// dtkVisualizationWidgetsCategoryPrivate
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationWidgetsCategoryPrivate
{
public:
    QVector<QWidget *> items;
};

// ///////////////////////////////////////////////////////////////////

dtkVisualizationWidgetsCategory::dtkVisualizationWidgetsCategory(const QString& name, const QVector<QWidget *>& widgets, QWidget *parent) : QGroupBox(parent), d(new dtkVisualizationWidgetsCategoryPrivate)
{
    QLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(0,0,0,0);
    for (auto *widget: widgets) {
        d->items << widgets;
        const QString item_name = widget->objectName();
        Q_ASSERT_X(item_name.length(), Q_FUNC_INFO, "widget name must not be empty when it is a category item");
        QLayout *item_layout = new QHBoxLayout;
        item_layout->setContentsMargins(0,0,0,0);
        item_layout->addWidget(new QLabel(item_name));
        item_layout->addWidget(widget);
        QGroupBox *item_box = new QGroupBox;
        item_box->setLayout(item_layout);
        item_box->setStyleSheet("QGroupBox { margin: 0px; padding-left: 20px }");

        layout->addWidget(item_box);
        item_box->setParent(this);
    }
    this->setObjectName(name);
    this->setLayout(layout);
    this->setStyleSheet("QGroupBox { background-color: transparent; margin: 0px; border: transparent; } ");
}

dtkVisualizationWidgetsCategory::~dtkVisualizationWidgetsCategory(void)
{
    delete d;
}

void dtkVisualizationWidgetsCategory::setVisible(QWidget *w, bool visible)
{
    if (d->items.contains(w)) {
        QWidget *p = dynamic_cast<QWidget *>(w->parent());
        if (p) {
            p->setVisible(visible);
        }
    }
}

// ///////////////////////////////////////////////////////////////////

class dtkVisualizationWidgetsScalarControlPrivate
{
public:
    QDoubleSpinBox *spbox = nullptr;
    QPushButton *breset = nullptr;
    double reset_value = 0;
};

dtkVisualizationWidgetsScalarControl::dtkVisualizationWidgetsScalarControl(const QString& name, double reset, QWidget *parent) : QGroupBox(parent), d(new dtkVisualizationWidgetsScalarControlPrivate)
{
    setObjectName(name);
    d->spbox = new QDoubleSpinBox;
    d->breset = new QPushButton(dtkFontAwesome::instance()->icon(fa::undo), QString());
    d->reset_value = reset;

    QLayout *layout = new QHBoxLayout;
    d->spbox->setMinimum(-999999999);
    d->spbox->setMaximum( 999999999);
    d->spbox->setDecimals(5);
    d->spbox->setKeyboardTracking(false);
    d->breset->setToolTip("Reset");
    layout->addWidget(d->spbox);
    layout->addWidget(d->breset);
    layout->setContentsMargins(0,0,0,0);
    this->setLayout(layout);
    this->setValue(reset);
    connect(d->spbox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=,this] (double val)
    {
        emit valueChanged(val);
    });
    connect(d->breset, QOverload<>::of(&QPushButton::released), [=,this] ()
    {
        d->spbox->setValue(d->reset_value);
        emit valueChanged(d->reset_value);
    });
}

dtkVisualizationWidgetsScalarControl::~dtkVisualizationWidgetsScalarControl(void)
{
    delete d;
}

void dtkVisualizationWidgetsScalarControl::setValue(double value)
{
    d->spbox->setValue(value);
}
void dtkVisualizationWidgetsScalarControl::setResetValue(double value)
{
    d->reset_value = value;
}

void dtkVisualizationWidgetsScalarControl::setVisible(bool visible)
{
    QWidget *w = dynamic_cast<QWidget*>(this->parent());
    if (w) {
        // we apply setVisible() to parent() because
        // the parent includes the whole line, with QLabel (prefix, name)
        w->setVisible(visible);
    }
    QWidget::setVisible(visible);
}

// ///////////////////////////////////////////////////////////////////

dtkVisualizationWidgetsScalarPositiveControl::dtkVisualizationWidgetsScalarPositiveControl(const QString& name, double reset, QWidget *parent) : dtkVisualizationWidgetsScalarControl(name, reset, parent)
{
    d->spbox->setMinimum(0);
}

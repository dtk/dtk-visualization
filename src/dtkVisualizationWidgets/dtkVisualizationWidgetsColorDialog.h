// dtkVisualizationWidgetsColorDialog.h
//

#pragma once

#include <dtkVisualizationWidgetsExport>

#include <QtWidgets/QWidget>

// /////////////////////////////////////////////////////////////////
// dtkVisualizationWidgetsColorDialog
// /////////////////////////////////////////////////////////////////

class DTKVISUALIZATIONWIDGETS_EXPORT dtkVisualizationWidgetsColorDialog : public QWidget
{
    Q_OBJECT

public:
     dtkVisualizationWidgetsColorDialog(QWidget * = nullptr);
    ~dtkVisualizationWidgetsColorDialog(void);

public:
    void setColor(const QColor&);
    QColor currentColor(void) const;

signals:
    void colorChanged(QColor);

private:
    class dtkVisualizationWidgetsColorDialogPrivate *d;
};

//
// dtkVisualizationWidgetsColorDialogdtkVisualizationWidgetsColorDialog.h ends here

#pragma once

#include <dtkVisualizationWidgetsExport>

#include <QGroupBox>

class DTKVISUALIZATIONWIDGETS_EXPORT dtkVisualizationWidgetsCategory : public QGroupBox
{
public:
     dtkVisualizationWidgetsCategory(const QString& name, const QVector<QWidget *>& widgets, QWidget *parent = nullptr);
    ~dtkVisualizationWidgetsCategory(void);

public:
    void setVisible(QWidget *, bool);

protected:
    class dtkVisualizationWidgetsCategoryPrivate *d = nullptr;
};

class DTKVISUALIZATIONWIDGETS_EXPORT dtkVisualizationWidgetsScalarControl : public QGroupBox
{
    Q_OBJECT

public:
     dtkVisualizationWidgetsScalarControl(const QString& name = "", double reset = 0, QWidget *parent = nullptr);
    ~dtkVisualizationWidgetsScalarControl(void);

public:
    void setValue(double value);
    void setResetValue(double value);
    void setVisible(bool visible) override;

signals:
    void valueChanged(double);

protected:
    class dtkVisualizationWidgetsScalarControlPrivate *d;
};

class DTKVISUALIZATIONWIDGETS_EXPORT dtkVisualizationWidgetsScalarPositiveControl : public dtkVisualizationWidgetsScalarControl
{
    Q_OBJECT

public:
    dtkVisualizationWidgetsScalarPositiveControl(const QString& name = "", double reset = 0, QWidget *parent = nullptr);
};

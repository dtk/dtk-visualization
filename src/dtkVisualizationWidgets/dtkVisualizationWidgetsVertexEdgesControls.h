#pragma once

#include <QtWidgets>
#include <vtkSmartPointer.h>
#include <vtkDataSet.h>

#include <dtkVisualizationWidgetsExport.h>

class  dtkVisualizationWidgetsVertexEdgesControlsPrivate;

class DTKVISUALIZATIONWIDGETS_EXPORT dtkVisualizationWidgetsVertexEdgesControls : public QWidget
{
Q_OBJECT
public:
    dtkVisualizationWidgetsVertexEdgesControls(QWidget *parent = nullptr);

    /** Whether the decoration is active */
    bool isActive(void);
    void setActive(bool checked);

    void setColor(const QColor& color);

    void setOpacity(double value);

    void setRadius(double value);
    double radius(void) const;

signals:

    void toggled(bool);

    void color_changed(QColor);

    void opacity_changed(double);

    void radius_changed(double);

private:
    dtkVisualizationWidgetsVertexEdgesControlsPrivate *d;
};

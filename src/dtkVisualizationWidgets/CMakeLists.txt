### CMakeLists.txt ---
##

project(dtkVisualizationWidgets
VERSION
  ${dtkVisualization_VERSION}
LANGUAGES
  CXX)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkVisualizationWidgets
  dtkVisualizationWidgetsActorList
  dtkVisualizationWidgetsActorList.h
  dtkVisualizationWidgetsClutEditor
  dtkVisualizationWidgetsClutEditor.h
  dtkVisualizationWidgetsColorDialog
  dtkVisualizationWidgetsColorDialog.h
  dtkVisualizationWidgetsColorMapEditor
  dtkVisualizationWidgetsColorMapEditor.h
  dtkVisualizationWidgetsFieldScalar
  dtkVisualizationWidgetsFieldScalar.h
  dtkVisualizationWidgetsIndicator
  dtkVisualizationWidgetsVertexEdgesControls.h
  dtkVisualizationWidgetsSliceControls
  dtkVisualizationWidgetsSliceControls.h
  dtkVisualizationWidgetsCategory
  dtkVisualizationWidgetsCategory.h
  dtkVisualizationWidgetsVideoControls
  dtkVisualizationWidgetsVideoControls.h
  )

set(${PROJECT_NAME}_SOURCES
  dtkVisualizationWidgetsActorList.cpp
  dtkVisualizationWidgetsClutEditor.cpp
  dtkVisualizationWidgetsColorDialog.cpp
  dtkVisualizationWidgetsColorMapEditor.cpp
  dtkVisualizationWidgetsFieldScalar.cpp
  dtkVisualizationWidgetsVertexEdgesControls.cpp
  dtkVisualizationWidgetsSliceControls.cpp
  dtkVisualizationWidgetsCategory.cpp
  dtkVisualizationWidgetsVideoControls.cpp
)

set(${PROJECT_NAME}_SOURCES_QRC
  dtkVisualizationWidgets.qrc)

## #################################################################
## Build rules
## #################################################################

qt_add_resources(${PROJECT_NAME}_SOURCES_RCC ${${PROJECT_NAME}_SOURCES_QRC})

qt_add_library(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_SOURCES_RCC}
  ${${PROJECT_NAME}_HEADERS})

target_include_directories(${PROJECT_NAME} PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/..
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>/..
  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
  $<INSTALL_INTERFACE:include>)

target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Gui)
target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Xml)

target_link_libraries(${PROJECT_NAME} PRIVATE dtkFonts)

target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Core)
target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Widgets)

target_link_libraries(${PROJECT_NAME} PRIVATE ${VTK_LIBRARIES})

add_library(dtk::VisualizationWidgets ALIAS dtkVisualizationWidgets)

## #################################################################
## Target properties
## #################################################################

set_target_properties(${PROJECT_NAME} PROPERTIES VERSION   ${${PROJECT_NAME}_VERSION}
                                                 SOVERSION ${${PROJECT_NAME}_VERSION_MAJOR})

## #################################################################
## Export header file
## #################################################################

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export")
generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export"
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

## ###################################################################
## Install rules - files
## ###################################################################

install(
FILES
  ${${PROJECT_NAME}_HEADERS}
DESTINATION
  include/${PROJECT_NAME})

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

install(EXPORT ${PROJECT_NAME}-targets
FILE
  ${PROJECT_NAME}Targets.cmake
DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/dtkVisualization)

export(EXPORT ${PROJECT_NAME}-targets
FILE
  ${CMAKE_BINARY_DIR}/${PROJECT_NAME}Targets.cmake)

######################################################################
### CMakeLists.txt ends here

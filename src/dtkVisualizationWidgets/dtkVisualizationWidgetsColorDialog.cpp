// dtkVisualizationWidgetsColorDialog.cpp
//

#include "dtkVisualizationWidgetsColorDialog.h"

#include <dtkFonts/dtkFontAwesome>

#include <QtGui>
#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkVisualizationWidgetsColorDialogPrivate
{
public:
    QLineEdit *line_edit = nullptr;
    QPushButton *color_button = nullptr;
    QPushButton *dialog_button = nullptr;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVisualizationWidgetsColorDialog::dtkVisualizationWidgetsColorDialog(QWidget *parent) : QWidget(parent), d(new dtkVisualizationWidgetsColorDialogPrivate)
{
    d->line_edit = new QLineEdit;
    d->color_button = new QPushButton;
    d->dialog_button = new QPushButton;

    QHBoxLayout *h_layout = new QHBoxLayout;
    h_layout->setAlignment(Qt::AlignLeft);
    h_layout->setContentsMargins(0,0,0,0);
    h_layout->addWidget(d->line_edit);
    h_layout->addWidget(d->color_button);
    h_layout->addWidget(d->dialog_button);
    this->setLayout(h_layout);

    d->line_edit->setMaxLength(7);
    d->line_edit->setAlignment(Qt::AlignRight);
    d->line_edit->setReadOnly(true);

    d->color_button->setEnabled(false);

    d->dialog_button->setIcon(dtkFontAwesome::instance()->icon(fa::adjust));
    d->dialog_button->setToolTip("Select Color");

    connect(d->dialog_button, &QPushButton::released, [=, this] (void)
    {
        this->setColor(QColorDialog::getColor(this->currentColor(), d->dialog_button));
    });
}

dtkVisualizationWidgetsColorDialog::~dtkVisualizationWidgetsColorDialog(void)
{
    delete d;
}

void dtkVisualizationWidgetsColorDialog::setColor(const QColor& c)
{
    if (c.isValid()) {
        d->line_edit->blockSignals(true);
        d->line_edit->setText(c.name());
        d->line_edit->blockSignals(false);

        d->color_button->setStyleSheet(QString("QPushButton { background-color: %1; }").arg(c.name()));
    }

    emit this->colorChanged(c);
}

QColor dtkVisualizationWidgetsColorDialog::currentColor(void) const
{
    return QColor(d->line_edit->text());
}

//
// dtkVisualizationWidgetsColorDialog.cpp ends here

#include <vtkProperty.h>
#include <dtkFonts>
#include "dtkVisualizationWidgetsVertexEdgesControls.h"

class dtkVisualizationWidgetsVertexEdgesControlsPrivate
{
public:
    QGridLayout *layout{nullptr};
    QLabel *enabled_label{nullptr};
    QAbstractButton *enabled_button{nullptr};
    QDoubleSpinBox *radius_sp{nullptr};
    QVector<QWidget *> hideable_widgets;
    QPushButton *color_button{nullptr};
    QDoubleSpinBox *opacity_sp{nullptr};
    QColor color;

    void insertWidget(const QString& name, QWidget *widget, const bool hideable = true)
    {
        insertWidget(new QLabel(name), widget, hideable);
    }

    void insertWidget(QLabel *name, QWidget *widget, const bool hideable = true)
    {
        const int lines = layout->count() / 2;
        layout->addWidget(name, lines, 0);
        layout->addWidget(widget, lines, 1);

        if (hideable) {
            hideable_widgets.append(name);
            hideable_widgets.append(widget);
        }

    }
};


dtkVisualizationWidgetsVertexEdgesControls::dtkVisualizationWidgetsVertexEdgesControls(QWidget *parent) :
        QWidget(parent),
        d(new dtkVisualizationWidgetsVertexEdgesControlsPrivate)
{
    d->layout = new QGridLayout;

    d->enabled_label = new QLabel;
    d->enabled_button = new QToolButton;
    d->enabled_button->setCheckable(true);
    d->enabled_button->setChecked(false);
    d->enabled_button->setIconSize(QSize(80, 15));

    d->color_button = new QPushButton;
    setColor(Qt::white);

    d->opacity_sp = new QDoubleSpinBox;
    d->opacity_sp->setRange(0.0, 1.0);
    d->opacity_sp->setValue(1);
    d->opacity_sp->setSingleStep(0.05);
    d->opacity_sp->setDecimals(2);

    d->radius_sp = new QDoubleSpinBox;
    d->radius_sp->setMinimum(0.0);
    d->radius_sp->setDecimals(3);
    d->radius_sp->setSingleStep(0.001);

    d->insertWidget(d->enabled_label, d->enabled_button, false);
    d->insertWidget("Color", d->color_button);
    d->insertWidget("Opacity", d->opacity_sp);
    d->insertWidget("Radius", d->radius_sp);

    setLayout(d->layout);

    connect(d->enabled_button, &QAbstractButton::toggled,
            this, &dtkVisualizationWidgetsVertexEdgesControls::setActive);

    connect(d->color_button, &QPushButton::released, [=, this](void) {
        const QColor color_value = QColorDialog::getColor(d->color, parent);
        if (color_value.isValid()) {
            setColor(color_value);
        }
    });

    connect(d->opacity_sp, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, &dtkVisualizationWidgetsVertexEdgesControls::opacity_changed);

    connect(d->radius_sp, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this, &dtkVisualizationWidgetsVertexEdgesControls::radius_changed);

    setActive(d->enabled_button->isChecked());
}

bool dtkVisualizationWidgetsVertexEdgesControls::isActive(void)
{
    return d->enabled_button->isChecked();
}

void dtkVisualizationWidgetsVertexEdgesControls::setActive(bool checked)
{
    d->enabled_button->blockSignals(true);
    d->enabled_button->setChecked(checked);
    d->enabled_button->blockSignals(false);
    d->enabled_label->setText(checked ? "Hide" : "Show");
    d->enabled_button->setIcon(dtkFontAwesome::instance()->icon(checked ? fa::arrowcircleup : fa::arrowcircledown));
    for (QWidget *w: this->d->hideable_widgets) {
        w->setVisible(checked);
    }
    emit toggled(checked);
}

void dtkVisualizationWidgetsVertexEdgesControls::setColor(const QColor& color)
{
    const auto color_string = QString(" rgba(%1, %2, %3, %4)").arg(color.red()).arg(color.green()).arg(
            color.blue()).arg(color.alpha());
    d->color_button->setStyleSheet("background-color: " + color_string);
    d->color = color;
    emit color_changed(d->color);
}

void dtkVisualizationWidgetsVertexEdgesControls::setOpacity(double value)
{
    d->opacity_sp->setValue(value);
}

void dtkVisualizationWidgetsVertexEdgesControls::setRadius(double value)
{
    d->radius_sp->setValue(value);
}

double dtkVisualizationWidgetsVertexEdgesControls::radius(void) const
{
    return d->radius_sp->value();
}

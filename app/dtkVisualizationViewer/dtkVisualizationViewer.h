// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgets/dtkWidgetsMainWindow>

class dtkVisualizationViewer : public dtkWidgetsMainWindow
{
    Q_OBJECT

public:
     dtkVisualizationViewer(QWidget *parent = nullptr);
    ~dtkVisualizationViewer(void);

public slots:
    void importDataSet(const QString& path);
    void applyScale(double x, double y, double z);

signals:
    void updated(void);

protected:
    void setup(void);
    void setdw(void);

protected:
    void keyPressEvent(QKeyEvent *) override;

protected:
    void resizeEvent(QResizeEvent *) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    class dtkVisualizationViewerPrivate *d;
};

//
// dtkVisualizationViewer.h ends here

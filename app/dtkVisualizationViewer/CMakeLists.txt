### CMakeLists.txt ---
##

project(dtkVisualizationViewer)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkVisualizationViewer.h
  dtkVisualizationViewerStack.h)

set(${PROJECT_NAME}_SOURCES
  dtkVisualizationViewer.cpp
  dtkVisualizationViewerStack.cpp
  main.cpp)

set(${PROJECT_NAME}_SOURCES_QRC dtkVisualizationViewer.qrc)

set_property(SOURCE qrc_dtkVisualizationViewer.cpp PROPERTY SKIP_AUTOMOC ON)

## #################################################################
## Build rules
## #################################################################

qt_add_resources(${PROJECT_NAME}_SOURCES_RCC ${${PROJECT_NAME}_SOURCES_QRC})

qt_add_executable(${PROJECT_NAME} MACOSX_BUNDLE
  ${${PROJECT_NAME}_SOURCES_RCC}
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

#target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Core)
target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Gui)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Core)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::CoreRuntime)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Themes)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Visualization)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::VisualizationWidgets)
target_link_libraries(${PROJECT_NAME} PRIVATE dtk::Widgets)

## ###################################################################
## Bundle setup
## ###################################################################

if(APPLE)
  set(${PROJECT_NAME}_RESOURCE_DIR ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${PROJECT_NAME}.app/Contents/Resources)
  add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${${PROJECT_NAME}_RESOURCE_DIR}
    COMMAND ${CMAKE_COMMAND} ARGS -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.icns ${${PROJECT_NAME}_RESOURCE_DIR})
endif(APPLE)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.plist.in ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.plist)

if(APPLE)
  set_target_properties(${PROJECT_NAME} PROPERTIES MACOSX_BUNDLE_INFO_PLIST ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.plist)
endif(APPLE)

## #################################################################
## Install rules
## #################################################################

if (UNIX AND NOT APPLE)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.desktop.in ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.desktop)

  install(PROGRAMS ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.desktop DESTINATION ~/.local/share/applications)
endif(UNIX AND NOT APPLE)

install(TARGETS ${PROJECT_NAME}
   BUNDLE DESTINATION ${CMAKE_INSTALL_BINDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

######################################################################
### CMakeLists.txt ends here

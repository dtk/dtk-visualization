// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtGui>

#include <QVTKOpenGLNativeWidget.h>
#include <vtkGenericOpenGLRenderWindow.h>

#include <dtkCore>
#include <dtkCoreRuntime>

#include <dtkThemes/dtkThemesEngine>

#include <dtkVisualization/dtkVisualization.h>

#include <dtkWidgets>

#include "dtkVisualizationViewer.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtk::widgets::initialize();
    dtk::visualization::initialize();
    dtkThemesEngine::instance()->apply();

    QSurfaceFormat::setDefaultFormat(QVTKOpenGLNativeWidget::defaultFormat());
    vtkOpenGLRenderWindow::SetGlobalMaximumNumberOfMultiSamples(0);

    dtkApplication *application = dtkApplication::create(argc, argv);
    application->setApplicationName("dtkVisualizationViewer");
    application->setOrganizationName("inria");
    application->setOrganizationDomain("fr");

    application->initialize();

    dtkWidgetsLayoutItem::Actions actions;
    actions.insert("Plot2D", "Plot 2D");
    actions.insert("Plot3D", "Plot 3D");
    actions.insert("View2D", "View 2D");
    actions.insert("View3D", "View 3D");
    actions.insert("ViewVideoPlayer", "Video player");

    dtkWidgetsLayoutItem::setActions(actions);

    dtkVisualizationViewer *viewer = new dtkVisualizationViewer;
    viewer->setWindowTitle("dtkVisualizationViewer");
    viewer->show();
    viewer->raise();

    int status = application->exec();

    dtkWidgetsController::instance()->clear();

    return status;
}

// ///////////////////////////////////////////////////////////////////

//
// main.cpp ends here

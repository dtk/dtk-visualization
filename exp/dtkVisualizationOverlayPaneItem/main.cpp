// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkWidgets/dtkWidgetsColorButton>
#include <dtkWidgets/dtkWidgetsOverlayPaneItem>

#include <QtWidgets>

int main(int argc, char **argv)
{
    QApplication application(argc, argv);
    application.setApplicationName("dtkVisualizationOverlayPaneItem");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

    QFormLayout *layout_1 = new QFormLayout;
    layout_1->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout_1->addRow("a", new QLineEdit);
    layout_1->addRow("b", new QLineEdit);
    layout_1->addRow("c", new QLineEdit);

    QFormLayout *layout_2 = new QFormLayout;
    layout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout_2->addRow("A", new QComboBox);
    layout_2->addRow("B", new QComboBox);
    layout_2->addRow("C", new QComboBox);

    QFormLayout *layout_3 = new QFormLayout;
    layout_3->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout_3->addRow("R", new dtkWidgetsColorButton(Qt::red));
    layout_3->addRow("G", new dtkWidgetsColorButton(Qt::green));
    layout_3->addRow("B", new dtkWidgetsColorButton(Qt::blue));

    dtkWidgetsOverlayPaneItem *item = new dtkWidgetsOverlayPaneItem;
    item->setTitle("Title");
    item->addLayout(layout_1);
    item->addLayout(layout_2);
    item->addLayout(layout_3);

    QScrollArea *scroll = new QScrollArea;
    scroll->setWidget(item);
    scroll->setWidgetResizable(true);
    scroll->setWindowTitle("dtkVisualizationOverlayPaneItem");
    scroll->resize(300, 100);
    scroll->show();
    scroll->raise();

    return application.exec();
}

//
// main.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkVisualization/dtkVisualizationPlot2D>

#include <QtWidgets>

#include <vtkChartXY.h>
#include <vtkAbstractArray.h>
#include <vtkDoubleArray.h>
#include <QVTKOpenGLWidget.h>

#include <math.h>

#define MAX 5

int main(int argc, char **argv)
{
    QSurfaceFormat::setDefaultFormat(QVTKOpenGLWidget::defaultFormat());

    QApplication application(argc, argv);
    application.setApplicationName("dtkVisualizationPlot2D");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

// ///////////////////////////////////////////////////////////////////
// Field data
// ///////////////////////////////////////////////////////////////////

    vtkSmartPointer<vtkDoubleArray> field_1 = vtkSmartPointer<vtkDoubleArray>::New();
    field_1->SetName("Linear");
    field_1->SetNumberOfComponents(1);
    field_1->SetNumberOfTuples(MAX * 1e2);

    vtkSmartPointer<vtkDoubleArray> field_2 = vtkSmartPointer<vtkDoubleArray>::New();
    field_2->SetName("Fake EEG");
    field_2->SetNumberOfComponents(1);
    field_2->SetNumberOfTuples(MAX * 1e2);

    vtkSmartPointer<vtkDoubleArray> field_3 = vtkSmartPointer<vtkDoubleArray>::New();
    field_3->SetName("third_field");
    field_3->SetNumberOfComponents(1);
    field_3->SetNumberOfTuples(MAX * 1e2);

    for(double i = 0; i < MAX; i += 1e-2) {
        field_1->SetValue(i * 1e2, i);
        field_2->SetValue(i * 1e2, cos(M_PI*6*i) + 2*sin(M_PI*2*i));
        field_3->SetValue(i * 1e2, rand() % 10);
    }

// ///////////////////////////////////////////////////////////////////
// Plot 2D example
// ///////////////////////////////////////////////////////////////////
    dtkVisualizationPlot2D *plot = new dtkVisualizationPlot2D;
    plot->setWindowTitle("dtkVisualizationPlot2D");
    plot->addFieldX(field_1);
    plot->addFieldY(field_2, true, vtkChart::LINE, vtkPlotPoints::NONE);
    plot->addFieldY(field_3);
    plot->show();
    plot->raise();
    plot->render();

    qDebug() << plot->fields();

    plot->removeField("third_field");

    qDebug() << plot->fields();

    plot->addFieldX(field_3);
    plot->removeField("third_field");
// ///////////////////////////////////////////////////////////////////

    return application.exec();
}
//
// main.cpp ends here

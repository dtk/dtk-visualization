// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualizationViewHybrid.h"

#include <dtkVisualizationWidgets/dtkVisualizationWidgetsActorList>
#include <dtkVisualizationWidgets/dtkVisualizationWidgetsClutEditor>

#include <QtWidgets>

#include <vtkGenericOpenGLRenderWindow.h>
#include <QVTKOpenGLStereoWidget.h>

int main(int argc, char **argv)
{
    vtkOpenGLRenderWindow::SetGlobalMaximumNumberOfMultiSamples(0);

    QSurfaceFormat::setDefaultFormat(QVTKOpenGLStereoWidget::defaultFormat());

    QApplication application(argc, argv);
    application.setApplicationName("dtkVisualizationViewHybrid");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

// ///////////////////////////////////////////////////////////////////
// Hybrid View example
// ///////////////////////////////////////////////////////////////////
    dtkVisualizationViewHybrid *view = new dtkVisualizationViewHybrid();

    dtkVisualizationWidgetsActorList *actors_list = new dtkVisualizationWidgetsActorList();
    actors_list->setRenderer(view->renderer2D());
    actors_list->setRenderer(view->renderer3D());

    view->setBottomLeftWidget(fa::alignleft, actors_list);

    dtkVisualizationWidgetsClutEditor *clut_editor = new dtkVisualizationWidgetsClutEditor();
    view->setBottomRightWidget(fa::paintbrush, clut_editor);
    view->setClutEditor(clut_editor);

    view->setWindowTitle("dtkVisualizationViewHybrid");
    view->show();
    view->raise();
    view->render();

// ///////////////////////////////////////////////////////////////////

    return application.exec();
}
//
// main.cpp ends here

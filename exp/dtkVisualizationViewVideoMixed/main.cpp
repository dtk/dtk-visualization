// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtMultimedia>
#include <QtMultimediaWidgets>

#include <dtkVisualization>

#include <QVTKOpenGLStereoWidget.h>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummyWindow : public QSplitter
{
    Q_OBJECT

public:
     dummyWindow(QWidget *parent = nullptr);
    ~dummyWindow(void);

public slots:
    void update(int requestId, const QImage& img);

private:
    QCamera             *camera;
    QCameraImageCapture *camera_capture;

private:
    dtkVisualizationView3D          *view_3;
    dtkVisualizationViewVideoRaster *view_v;
};

dummyWindow::dummyWindow(QWidget *parent) : QSplitter(parent)
{
    this->camera = new QCamera(QCameraInfo::defaultCamera());

    this->camera_capture = new QCameraImageCapture(this->camera);

    this->view_v = new dtkVisualizationViewVideoRaster(this);
    this->view_v->setTitle("Live stream Raster Mixed with vtk");
    this->view_v->resize(800, 400);

    this->view_3 = new dtkVisualizationView3D(this);

    QCameraViewfinder *viewfinder = new QCameraViewfinder;

    this->setOrientation(Qt::Horizontal);
    this->addWidget(this->view_v);
    this->addWidget(this->view_3);

    connect(this->camera_capture, &QCameraImageCapture::imageCaptured, this, &dummyWindow::update);

    this->camera->setViewfinder(viewfinder);
    this->camera->setCaptureMode(QCamera::CaptureStillImage);
    this->camera->start();

    QTimer *timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this->camera_capture, [=] (void) {
        camera_capture->capture();
    });

    timer->start(1.0/15.0 * 1000);
}

dummyWindow::~dummyWindow(void)
{

}

void dummyWindow::update(int id, const QImage& image)
{
    Q_UNUSED(id);

    this->view_v->setImage(image);

    QFrame::update();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QSurfaceFormat::setDefaultFormat(QVTKOpenGLStereoWidget::defaultFormat());

    QApplication application(argc, argv);

    dummyWindow *window = new dummyWindow;
    window->resize(1000, 500);
    window->show();
    window->raise();

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////

#include <main.moc>

//
// main.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtMultimedia>
#include <QtMultimediaWidgets>

#include <dtkVisualization>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummyWindow : public QFrame
{
    Q_OBJECT

public:
     dummyWindow(QWidget *parent = nullptr);
    ~dummyWindow(void);

public slots:
    void update(int requestId, const QImage& img);

private:
    QCamera             *camera;
    QImageCapture       *camera_capture;

private:
    dtkVisualizationViewVideo       *viev;
    dtkVisualizationViewVideoGL     *view;
    dtkVisualizationViewVideoRaster *vieu;
};

dummyWindow::dummyWindow(QWidget *parent) : QFrame(parent)
{
    this->camera = new QCamera();

    this->camera_capture = new QImageCapture(this->camera);

    this->vieu = new dtkVisualizationViewVideoRaster(this);
    this->vieu->setTitle("Live stream Raster");

    this->viev = new dtkVisualizationViewVideo(this);
    this->viev->setTitle("Live stream GL < 2.1");

    this->view = new dtkVisualizationViewVideoGL(this);
    this->view->setTitle("Live stream GL = 2.1");

    QCameraViewfinder *viewfinder = new QCameraViewfinder;

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(this->vieu);
    layout->addWidget(this->viev);
    layout->addWidget(this->view);

    connect(this->camera_capture, &QImageCapture::imageCaptured, this, &dummyWindow::update);

    this->camera->setViewfinder(viewfinder);
    this->camera->setCaptureMode(QCamera::CaptureStillImage);
    this->camera->start();

    QTimer *timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this->camera_capture, [=] (void) {
        camera_capture->capture();
    });

    timer->start(1.0/15.0 * 1000);
}

dummyWindow::~dummyWindow(void)
{

}

void dummyWindow::update(int id, const QImage& image)
{
    Q_UNUSED(id);

    this->vieu->setImage(image);
    this->viev->setImage(image);
    this->view->setImage(image);

    QFrame::update();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setVersion(2, 1);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
    format.setSamples(32);

    QSurfaceFormat::setDefaultFormat(format);

    QApplication application(argc, argv);

    dummyWindow *window = new dummyWindow;
    window->resize(1000, 500);
    window->show();
    window->raise();

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////

#include <main.moc>

//
// main.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkWidgets/dtkWidgetsOverlayRope>

#include <QtWidgets>

int main(int argc, char **argv)
{
    QApplication application(argc, argv);
    application.setApplicationName("dtkVisualizationOverlayRope");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

    dtkWidgetsOverlayRope *window = new dtkWidgetsOverlayRope;
    window->setWindowTitle("dtkVisualizationOverlayRope");
    window->resize(800, 600);
    window->show();
    window->raise();

    return application.exec();
}

//
// main.cpp ends here
